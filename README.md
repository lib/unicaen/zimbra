# UnicaenZimbra

 * [Introduction](#introduction)
 * [Installation](#installation)
 * [Configuration](#configuration)
 
## Introduction
 
Ce module permet d'utiliser l'[API SOAP Zimbra](https://wiki.zimbra.com/wiki/SOAP_API_Reference_Material_Beginning_with_ZCS_8) et de piloter le serveur Zimbra de l'université. 

## Pré-requis

L'utilisation de ce module nécessite l'installation de l'extension `ext-curl` de PHP.

## Installation

```bash 
$ composer require unicaen/zimbra
```

## Configuration

> Récupérer les fichiers de config du module 
 
```bash
$ cp -n vendor/unicaen/zimbra/config/unicaen-zimbra.global.php.dist config/autoload/unicaen-zimbra.global.php
$ cp -n vendor/unicaen/zimbra/config/unicaen-zimbra.local.php.dist config/autoload/unicaen-zimbra.local.php
```

> Adapter le contenu à vos besoins en configurant notamment les paramètres de connexion au serveur Zimbra.

```php
'unicaen-zimbra' => [
    'server' => 'test-zimbra.unicaen.fr',
    'port' => 7071,
    'email' => 'test@unicaen.fr',
    'password' => 'xxxx',
    'domain' => 'unicaen.fr',
    'boite_generique_cos_id' => 'e00428a1-0c00-11d9-836a-000d93afea2a',
    'debug_mode' => false,
]
```