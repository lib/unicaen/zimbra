<?php

namespace UnicaenZimbra\Processus;

use UnicaenZimbra\Entity\Account;
use UnicaenZimbra\Entity\Filter\Action\FileInto as FilterActionFileInto;
use UnicaenZimbra\Entity\Filter\Action\Keep as FilterActionKeep;
use UnicaenZimbra\Entity\Filter\Filter;
use UnicaenZimbra\Entity\Filter\Test\Address as FilterTestAddress;
use UnicaenZimbra\Entity\Filter\Test\Header as FilterTestHeader;
use UnicaenZimbra\Entity\Grant;
use UnicaenZimbra\Entity\Identity;
use UnicaenZimbra\Entity\Right;
use UnicaenZimbra\Options\ModuleOptions;
use UnicaenZimbra\Service\ZimbraFolderService;

class ZimbraBoiteGeneriqueProcessus extends AbstractProcessus
{
    const ZIMBRA_TEMPORISATION = 5000000; // microsecondes

    /**
     * @var ModuleOptions
     */
    protected $options;


    /**
     * @param ModuleOptions $moduleOptions
     */
    public function setOptions(ModuleOptions $moduleOptions)
    {
        $this->options = $moduleOptions;
    }

    /**
     * @return ModuleOptions
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Créer une boite générique
     *
     * @param string $mail Adresse mail de la boite générique
     * @param string $password Mot de passe de la boite
     * @param string $name Nom de la boite
     * @param string $description Description
     * @return self
     */
    public function create($mail, $password, $name, $description)
    {
        $account = new Account;
        $account->name = $mail;
        $account->password = $password;
        $account->description = $description;
        $account->zimbraCOSId = $this->getOptions()->getBoiteGeneriqueCosId();
        $this->accountService->create($account);
        $this->accountService->addAlias($account, $name . substr($mail, strpos($mail, '@')));

        return $this;
    }

    /**
     * Renommer une boîte générique
     *
     * @param string $mail adresse mail de la boîte générique
     * @param string $newName nouveau nom
     * @return self
     */
    public function rename($mail, $newName)
    {
        $this->accountService->rename($this->accountService->get($mail), $newName);

        return $this;
    }

    /**
     * Supprimer une boite générique
     *
     * @param string $mail Adresse mail de la boite générique
     * @return self
     */
    public function delete($mail)
    {
        $this->accountService->delete($this->accountService->get($mail));

        return $this;
    }

    /**
     * Créer une adresse fonctionnelle par rapport à la boite générique (paramètre $mail)
     *
     * @param string $mail Adresse mail de la boite générique
     * @param string $adresseFonctionnelle Adresse mail de l'adresse fonctionnelle
     * @return self
     */
    public function createAdresseFonctionnelle($mail, $adresseFonctionnelle)
    {
        $afName = $this->getMailName($adresseFonctionnelle);

        // Travail sur la boite générique
        $bg = $this->accountService->get($mail);
        $this->folderService->setAccount($bg);
        $this->filterService->setAccount($bg);

        // Liste de distribution
        $distribList = $this->distributionListService->create($adresseFonctionnelle);
        usleep(self::ZIMBRA_TEMPORISATION); // temporisation nécessaire pour Zimbra

        // Ajout des membres à la liste
        $this->distributionListService->addMembers($distribList, $mail);

        // Répertoires
        $inputFolder = $this->folderService->create($afName . ' | Réception');
        $sentFolder = $this->folderService->create($afName . ' | Envoi');

        // Filtre de réception
        $filter = new Filter;
        $filter->setName($afName . ' | Réception')
            ->setCondition($filter::CONDITION_ANYOF);
        $test1 = new FilterTestAddress;
        $test1->setHeader('to,cc')->setStringComparison('contains')->setPart('all')
            ->setValue($adresseFonctionnelle);
        $test2 = new FilterTestHeader;
        $test2->setHeader('X-Zimbra-DL')->setStringComparison('contains')
            ->setValue($adresseFonctionnelle);
        $filter->setTests([$test1, $test2]);
        $test3 = new FilterTestHeader;
        $test3->setHeader('Resent-To')->setStringComparison('contains')
            ->setValue($adresseFonctionnelle);
        $test4 = new FilterTestHeader;
        $test4->setHeader('Received')->setStringComparison('contains')
            ->setValue("for <$adresseFonctionnelle>");
        $filter->setTests([$test1, $test2, $test3, $test4]);

        $action = new FilterActionFileInto;
        $action->setFolderPath($inputFolder->absFolderPath);
        $filter->addAction($action);
        $filter->addAction(new FilterActionKeep);
        $this->filterService->createInput($filter);

        return $this;
    }

    /**
     * Renommer une adresse fonctionnelle
     *
     * @param string $mail Adresse mail de la boite générique
     * @param string $adresseFonctionnelle adresse mail de l'adresse fonctionnelle
     * @param string $newName nouveau nom
     * @return self
     */
    public function renameAdresseFonctionnelle($mail, $adresseFonctionnelle, $newName)
    {
        $afName = $this->getMailName($adresseFonctionnelle);
        $afNewName = $this->getMailName($newName);

        // Travail sur la boite générique
        $bg = $this->accountService->get($mail);
        $this->folderService->setAccount($bg);
        $this->filterService->setAccount($bg);

        // Liste de distribution
        // la suppression de la liste de distribution permet de supprimer les droits (ACE) sur celle-ci
        $this->distributionListService->delete($this->distributionListService->get($adresseFonctionnelle));
        $distribList = $this->distributionListService->create($newName);
        usleep(self::ZIMBRA_TEMPORISATION); // temporisation nécessaire pour Zimbra

        // Ajout des membres à la liste
        $this->distributionListService->addMembers($distribList, $mail);

        // Répertoires
        $inputFolder = $this->folderService->rename($this->folderService->get($afName . ' | Réception'), $afNewName . ' | Réception');
        $sentFolder = $this->folderService->rename($this->folderService->get($afName . ' | Envoi'), $afNewName . ' | Envoi');

        // Filtre entrant
        $filter = new Filter;
//	    $filter = $this->filterService->getInput()[$afName.' | Réception'];
        $filter->setName($afNewName . ' | Réception')
            ->setCondition($filter::CONDITION_ANYOF);
        $test1 = new FilterTestAddress();
        $test1->setHeader('to,cc')->setStringComparison('contains')->setPart('all')
            ->setValue($newName);
        $test2 = new FilterTestHeader();
        $test2->setHeader('X-Zimbra-DL')->setStringComparison('contains')
            ->setValue($newName);
        $test3 = new FilterTestHeader;
        $test3->setHeader('Resent-To')->setStringComparison('contains')
            ->setValue($newName);
        $test4 = new FilterTestHeader;
        $test4->setHeader('Received')->setStringComparison('contains')
            ->setValue("for <$newName>");
        $filter->setTests([$test1, $test2, $test3, $test4]);

        $action = new FilterActionFileInto;
        $action->setFolderPath($afNewName . ' | Réception');
        $filter->addAction($action);
        $filter->addAction(new FilterActionKeep);

        $this->filterService->deleteInput($afName . ' | Réception');
        $this->filterService->createInput($filter);

        return $this;
    }

    /**
     * Supprimer une adresse fonctionnelle par rapport à la boite générique (paramètre $mail)
     *
     * @param string $mail Adresse mail de la boite générique
     * @param string $adresseFonctionnelle Adresse mail de l'adresse fonctionnelle
     * @return self
     */
    public function deleteAdresseFonctionnelle($mail, $adresseFonctionnelle)
    {
        $afName = $this->getMailName($adresseFonctionnelle);

        // Travail sur la boite générique
        $bg = $this->accountService->get($mail);
        $this->folderService->setAccount($bg);
        $this->filterService->setAccount($bg);

        // Liste de distribution
        $this->distributionListService->delete($this->distributionListService->get($adresseFonctionnelle));

        // Répertoires
        $this->folderService->delete($this->folderService->get($afName . ' | Réception'));
        $this->folderService->delete($this->folderService->get($afName . ' | Envoi'));

        // Filtres
        $this->filterService->deleteInput($afName . ' | Réception');

        return $this;
    }

    /**
     * Retourner les droits sur une adresse fonctionnelle
     * Permet notamment de récupérer les utilisateurs qui ont un accès à l'adresse fonctionnelle
     *
     * @param type $adresseFonctionnelle Adresse mail de l'adresse fonctionnelle
     * @return Right[]
     */
    public function getGrantsOfAdresseFonctionnelle($adresseFonctionnelle)
    {
        $ace = new Right;
        $ace->setTarget($this->distributionListService->get($adresseFonctionnelle));

        return $this->rightService->getGrants($ace);
    }

    /**
     * Donner accès à une adresse fonctionnelle pour un utilisateur
     *
     * @param string $adresseFonctionnelle Adresse mail de l'adresse fonctionnelle
     * @param string $userMail Adresse mail de l'utilisateur
     * @return self
     */
    public function grantUserToAdresseFonctionnelle($adresseFonctionnelle, $userMail)
    {
        $afName = $this->getMailName($adresseFonctionnelle);

        // Travail sur la boite générique
        $dlAccount = $this->distributionListService->get($adresseFonctionnelle);
        foreach($dlAccount->members as $member) {
            if(preg_match('/bg-.*/', $member)) {
                $bgAccount = $member;
                break;
            }
        }
        $bgAccount = $bgAccount ?: $dlAccount->members[0];

        $userAccount = $this->accountService->get($userMail);
        $this->folderService->setAccount($bgAccount);

        // Partages
        $inputFolder = $this->folderService->get($afName . ' | Réception');
        $sentFolder = $this->folderService->get($afName . ' | Envoi');

        $grant = new Grant;
        $grant->setGrantee($userAccount);
        $grant->perm = Grant::PERM_MANAGE;

        $this->folderService->addGrant($inputFolder, $grant, ZimbraFolderService::GRANT_ACTION_NONE)
            ->createMountPoint($inputFolder, $userAccount, $inputFolder->absFolderPath)
            ->addGrant($sentFolder, $grant, ZimbraFolderService::GRANT_ACTION_NONE)
            ->createMountPoint($sentFolder, $userAccount, $sentFolder->absFolderPath);

        // Travail sur la boite utilisateur
        $this->filterService->setAccount($userAccount);
        $this->identityService->setAccount($userAccount);
        $this->folderService->setAccount($userAccount);

        // Filtre d'envoi
        $filter = new Filter;
        $filter->setName($afName . ' | Envoi')
            ->setCondition($filter::CONDITION_ANYOF);
        $test1 = new FilterTestAddress;
        $test1->setHeader('from')->setStringComparison('contains')->setPart('all')
            ->setValue($adresseFonctionnelle);
        $test2 = new FilterTestHeader();
        $test2->setHeader('Reply-To')->setStringComparison('contains')
            ->setValue($adresseFonctionnelle);
        $filter->setTests([$test1, $test2]);

        $action = new FilterActionFileInto;
        $action->setFolderPath($sentFolder->absFolderPath);
        $filter->addAction($action);
        $this->filterService->createOutput($filter);

        // ACE
        $ace = new Right;
        $ace->setTarget($dlAccount)
            ->setGrantee($userAccount)
            ->setRightByRightName('sendAsDistList');
        $this->rightService->grantRight($ace);

        // Avatar
        $identity = new Identity;
        $identity->setName($afName);
        $identity->zimbraPrefFromAddress = $adresseFonctionnelle;
        $identity->zimbraPrefFromAddressType = 'sendAs';
        $identity->zimbraPrefFromDisplay = $afName;
        $identity->zimbraPrefWhenInFolderIds = $this->folderService->get($afName . ' | Réception')->getId();
        $identity->zimbraPrefWhenInFoldersEnabled = true;
        $identity->zimbraPrefReplyToAddress = $adresseFonctionnelle;
        $identity->zimbraPrefReplyToEnabled = true;
        $identity->zimbraPrefWhenSentToAddresses = $adresseFonctionnelle;
        $identity->zimbraPrefWhenSentToEnabled = true;
        $this->identityService->create($identity);

        return $this;
    }

    /**
     * Retirer l'accès à une adresse fonctionnelle pour un utilisateur
     *
     * @param string $adresseFonctionnelle Adresse mail de l'adresse fonctionnelle
     * @param string $userMail Adresse mail de l'utilisateur
     * @return self
     */
    public function revokeUserFromAdresseFonctionnelle($adresseFonctionnelle, $userMail)
    {
        $afName = $this->getMailName($adresseFonctionnelle);

        // Travail sur la boite générique
        $dlAccount = $this->distributionListService->get($adresseFonctionnelle);
        foreach($dlAccount->members as $member) {
            if(preg_match('/bg-.*/', $member)) {
                $bgAccount = $member;
                break;
            }
        }
        $userAccount = $this->accountService->get($userMail);
        $this->folderService->setAccount($bgAccount);

        // Suppression des partages
        $inputFolder = $this->folderService->get($afName . ' | Réception');
        $sentFolder = $this->folderService->get($afName . ' | Envoi');

        $this->folderService
            ->revokeGrant($inputFolder, $userAccount)
            ->revokeGrant($sentFolder, $userAccount);

        // Travail sur la boite utilisateur
        $this->filterService->setAccount($userAccount);
        $this->identityService->setAccount($userAccount);

        // Filtres
        $this->filterService->deleteOutput($afName . ' | Envoi');

        // ACE
        $ace = new Right;
        $ace->setTarget($dlAccount)
            ->setGrantee($userAccount)
            ->setRightByRightName('sendAsDistList');
        $this->rightService->revokeRight($ace);

        // Avatar
        $this->identityService->delete($this->identityService->get($afName));

        return $this;
    }

    /**
     * Renommer une adresse fonctionnelle pour un utilisateur
     *
     * @param string $adresseFonctionnelle adresse mail de l'adresse fonctionnelle
     * @param type $userMail adresse mail de l'utilisateur
     * @param type $newName nouveau nom
     * @return self
     */
    public function renameAdresseFonctionnelleToUser($adresseFonctionnelle, $userMail, $newName)
    {
        $afName = $this->getMailName($adresseFonctionnelle);
        $afNewName = $this->getMailName($newName);

        // Travail sur la boite utilisateur
        $userAccount = $this->accountService->get($userMail);
        $this->folderService->setAccount($userAccount);
        $this->filterService->setAccount($userAccount);
        $this->identityService->setAccount($userAccount);

        // Répertoires
        $inputFolder = $this->folderService->rename($inputFolder = $this->folderService->get($afName . ' | Réception'), $afNewName . ' | Réception');
        $sentFolder = $this->folderService->rename($sentFolder = $this->folderService->get($afName . ' | Envoi'), $afNewName . ' | Envoi');

        // Filtre sortant
        $filter = new Filter;
//	    $filter = $this->filterService->getOutput()[$afName.' | Envoi'];
        $filter->setName($afNewName . ' | Envoi');
        $test = new FilterTestAddress();
        $test->setHeader('from')->setStringComparison('contains')->setPart('all')
            ->setValue($newName);
        $filter->setTests([$test]);

        $action = new FilterActionFileInto;
        $action->setFolderPath($afNewName . ' | Envoi');
        $filter->addAction($action);

        $this->filterService->deleteOutput($afName . ' | Envoi');
        $this->filterService->createOutput($filter);

        // ACE
        $newAce = new Right();
        $newAce->setTarget($this->distributionListService->get($newName))
            ->setGrantee($userAccount)
            ->setRightByRightName('sendAsDistList');
        $this->rightService->grantRight($newAce);

        // Avatar
        $this->identityService->delete($this->identityService->get($afName)); // suppression ancienne identité
        $identity = new Identity();
        $identity->setName($afNewName);
        $identity->zimbraPrefFromAddress = $newName;
        $identity->zimbraPrefFromAddressType = 'sendAs';
        $identity->zimbraPrefFromDisplay = $afNewName;
        $identity->zimbraPrefWhenInFolderIds = $this->folderService->get($afNewName . ' | Réception')->getId();
        $identity->zimbraPrefWhenInFoldersEnabled = true;
        $identity->zimbraPrefReplyToAddress = $newName;
        $identity->zimbraPrefReplyToEnabled = true;
        $identity->zimbraPrefWhenSentToAddresses = $newName;
        $identity->zimbraPrefWhenSentToEnabled = true;
        $this->identityService->create($identity);

        return $this;
    }

    /**
     * Retourne la partie locale d'une adresse mail (tout ce qu'il y a avant le dernier @)
     *
     * @param string $mail
     * @return string
     */
    private function getMailName($mail)
    {
        return substr($mail, 0, strrpos($mail, '@'));
    }
}