<?php

namespace UnicaenZimbra\Processus;

trait ZimbraBoiteGeneriqueProcessusAwareTrait
{
    /**
     * @var ZimbraBoiteGeneriqueProcessus
     */
    protected $boiteGeneriqueProcessus;


    /**
     * @param ZimbraBoiteGeneriqueProcessus $boiteGeneriqueProcessus
     */
    public function setZimbraBoiteGeneriqueProcessus(ZimbraBoiteGeneriqueProcessus $boiteGeneriqueProcessus)
    {
        $this->boiteGeneriqueProcessus = $boiteGeneriqueProcessus;
    }

    /**
     * @return ZimbraBoiteGeneriqueProcessus
     */
    public function getZimbraBoiteGeneriqueProcessus()
    {
        return $this->boiteGeneriqueProcessus;
    }
}