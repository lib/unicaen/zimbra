<?php

namespace UnicaenZimbra\Processus;

use Interop\Container\ContainerInterface;
use UnicaenZimbra\Options\ModuleOptions;
use UnicaenZimbra\Service\ZimbraAccountService;
use UnicaenZimbra\Service\ZimbraCosService;
use UnicaenZimbra\Service\ZimbraDistributionListService;
use UnicaenZimbra\Service\ZimbraFilterService;
use UnicaenZimbra\Service\ZimbraFolderService;
use UnicaenZimbra\Service\ZimbraIdentityService;
use UnicaenZimbra\Service\ZimbraRightService;
use UnicaenZimbra\Zimbra;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ZimbraBoiteGeneriqueProcessusFactory implements FactoryInterface
{
    /**
     * Create processus
     *
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return ZimbraBoiteGeneriqueProcessus
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) : ZimbraBoiteGeneriqueProcessus
    {
        /**
         * @var Zimbra $zimbra
         * @var ModuleOptions $moduleOptions
         * @var ZimbraAccountService $accountService
         * @var ZimbraCosService $cosService
         * @var ZimbraDistributionListService $distributionListService
         * @var ZimbraFilterService $filterService
         * @var ZimbraFolderService $folderService
         * @var ZimbraIdentityService $identityService
         * @var ZimbraRightService $rightService
         */
        $zimbra = $container->get('zimbra');
        $moduleOptions = $container->get('zimbraOptions');
        $accountService = $container->get(ZimbraAccountService::class);
        $cosService = $container->get(ZimbraCosService::class);
        $distributionListService = $container->get(ZimbraDistributionListService::class);
        $filterService = $container->get(ZimbraFilterService::class);
        $folderService = $container->get(ZimbraFolderService::class);
        $identityService = $container->get(ZimbraIdentityService::class);
        $rightService = $container->get(ZimbraRightService::class);

        $processus = new ZimbraBoiteGeneriqueProcessus();
        $processus->setZimbra($zimbra);
        $processus->setOptions($moduleOptions);
        $processus->setZimbraAccountService($accountService);
        $processus->setZimbraCosService($cosService);
        $processus->setZimbraDistributionListService($distributionListService);
        $processus->setZimbraFilterService($filterService);
        $processus->setZimbraFolderService($folderService);
        $processus->setZimbraIdentityService($identityService);
        $processus->setZimbraRightService($rightService);

        return $processus;
    }
}