<?php

namespace UnicaenZimbra\Processus;

use UnicaenZimbra\Service\ZimbraAccountServiceAwareTrait;
use UnicaenZimbra\Service\ZimbraCosServiceAwareTrait;
use UnicaenZimbra\Service\ZimbraDistributionListServiceAwareTrait;
use UnicaenZimbra\Service\ZimbraFilterServiceAwareTrait;
use UnicaenZimbra\Service\ZimbraFolderServiceAwareTrait;
use UnicaenZimbra\Service\ZimbraIdentityServiceAwareTrait;
use UnicaenZimbra\Service\ZimbraRightServiceAwareTrait;
use UnicaenZimbra\Zimbra;

abstract class AbstractProcessus
{
    use ZimbraAccountServiceAwareTrait;
    use ZimbraCosServiceAwareTrait;
    use ZimbraDistributionListServiceAwareTrait;
    use ZimbraFilterServiceAwareTrait;
    use ZimbraFolderServiceAwareTrait;
    use ZimbraIdentityServiceAwareTrait;
    use ZimbraRightServiceAwareTrait;
    
    /**
     * @var Zimbra
     */
    protected $zimbra;


    /**
     * @param Zimbra $zimbra
     * @return $this
     */
    public function setZimbra(Zimbra $zimbra) : self
    {
        $this->zimbra = $zimbra;

        return $this;
    }

    /**
     * @return Zimbra
     */
    public function getZimbra() : Zimbra
    {
        return $this->zimbra;
    }
}