<?php

namespace UnicaenZimbra\Options;

use Laminas\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
{
    /**
     * @var string
     */
    protected $server;

    /**
     * @var integer
     */
    protected $port;

    /**
     * @var string
     */
    protected $email;
    
    /**
     *
     * @var string
     */
    protected $password;
    
    /**
     *
     * @var string
     */
    protected $domain;

    /**
     *
     * @var string
     */
    protected $boiteGeneriqueCosId;
    
    /**
     *
     * @var boolean
     */
    protected $debugMode = false;
    
    
    
    
    /**
     *
     * @param string $server
     * @return ModuleOptions
     */
    public function setServer($server)
    {
        $this->server = (string) $server;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }
    
    /**
     *
     * @param string $port
     * @return ModuleOptions
     */
    public function setPort($port)
    {
        $this->port = (string) $port;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }
    
    /**
     *
     * @param string $email
     * @return ModuleOptions
     */
    public function setEmail($email)
    {
        $this->email = (string) $email;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     *
     * @param string $password
     * @return ModuleOptions
     */
    public function setPassword($password)
    {
        $this->password = (string) $password;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     *
     * @param string $domain
     * @return ModuleOptions
     */
    public function setDomain($domain)
    {
        $this->domain = (string) $domain;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     *
     * @param string $boiteGeneriqueCosId
     * @return ModuleOptions
     */
    public function setBoiteGeneriqueCosId($boiteGeneriqueCosId)
    {
        $this->boiteGeneriqueCosId = (string) $boiteGeneriqueCosId;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getBoiteGeneriqueCosId()
    {
        return $this->boiteGeneriqueCosId;
    }

    /**
     *
     * @param boolean $debugMode
     * @return ModuleOptions
     */
    public function setDebugMode($debugMode)
    {
        $this->debugMode = $debugMode == true;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getDebugMode()
    {
        return $this->debugMode;
    }
}