<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Account as AccountEntity;
use UnicaenZimbra\Entity\MailBox as MailBoxEntity;
use UnicaenZimbra\Zimbra;

class ZimbraMailBoxService extends AbstractService
{

    /**
     * Retourne la liste des boites mail
     * 
     * Les clés sont les ID des boites mail
     * 
     * @param integer $limit
     * @param integer $offset
     * @return MailBoxEntity[]
     */
    public function getList( $limit=Zimbra::DEFAULT_LIMIT, $offset=Zimbra::DEFAULT_OFFSET )
    {
        $params = array(
            'limit'  => $limit,
            'offset' => $offset
        );
        
        $response = $this->getZimbra()->request('GetAllMailboxesRequest', array(), $params);

        $results = array();
        $this->count = (int)$response->children()->GetAllMailboxesResponse['searchTotal'];
        foreach ($response->children()->GetAllMailboxesResponse->children() as $mailBoxXml) {
            $mailBox = new MailBoxEntity;
            $mailBox->populate($mailBoxXml);
            $results[$mailBox->getId()] = $mailBox;
        }
        return $results;
    }

    /**
     * Retourne une boite mail en fonction de l'identifiant de compte ou du compte donné
     * @param string|AccountEntity $accountId
     * @return MailBoxEntity
     */
    public function get( $accountId )
    {
        if ($accountId instanceof AccountEntity) $accountId = $accountId->getId();
        
        $params = array(
            'mbox' => array( '@attributes' => array(
                'id' => $accountId
            ) )
        );

        $response = $this->getZimbra()->request('GetMailboxRequest', array(), $params);
        $mailBoxs = $response->children()->GetMailboxResponse->children();
        $mailBox = new MailBoxEntity;
        $mailBoxs[0]->addAttribute('accountId', $accountId);
        $mailBox->populate($mailBoxs[0]);
        $this->count = 1;
        return $mailBox;       
    }

}