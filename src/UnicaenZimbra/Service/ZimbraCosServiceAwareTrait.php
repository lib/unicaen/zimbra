<?php

namespace UnicaenZimbra\Service;

trait ZimbraCosServiceAwareTrait
{
    /**
     * @var ZimbraCosService
     */
    protected $cosService;


    /**
     * @param ZimbraCosService $cosService
     */
    public function setZimbraCosService(ZimbraCosService $cosService)
    {
        $this->cosService = $cosService;
    }

    /**
     * @return ZimbraCosService
     */
    public function getZimbraCosService()
    {
        return $this->cosService;
    }
}