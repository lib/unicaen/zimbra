<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\DistributionList as DistributionListEntity;
use UnicaenZimbra\Entity\Account as AccountEntity;
use UnicaenZimbra\Zimbra;

class ZimbraDistributionListService extends AbstractService
{
    
    /**
     * Retourne la liste des listes de distribution
     * 
     * Les clés sont les ID des comptes
     * 
     * @param string $domain
     * @param integer $limit
     * @param integer $offset
     * @return DistributionListEntity[]
     */
    public function getList( $domain, $limit=Zimbra::DEFAULT_LIMIT, $offset=Zimbra::DEFAULT_OFFSET )
    {
        $distributionLists = $this->getZimbra()->searchDirectory($domain, $limit, $offset, 'distributionlists');

        $results = array();
        $this->count = (int)$distributionLists->children()->SearchDirectoryResponse['searchTotal'];
        foreach ($distributionLists->children()->SearchDirectoryResponse->children() as $distributionListXml) {
            $distributionList = new DistributionListEntity;
            $distributionList->populate($distributionListXml);
            $results[$distributionList->getId()] = $distributionList;
        }
        return $results;
    }

    /**
     * Détermine si une liste de distribution existe ou non
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return boolean
     */
    public function exists($value, $by='name')
    {
        return $this->getZimbra()->exists($value, $by, 'distributionlists');
    }

    /**
     * Retourne une liste de distribution en fonction de la valeur donnée et de son type (son nom par défaut)
     * @param string $value
     * @param string $by
     * @return DistributionListEntity
     */
    public function get( $value, $by='name' )
    {
        $params = array(
            'dl' => array(
                '@content'  => $value,
                '@attributes' => array( 'by' => $by ),
            )
        );

        $response = $this->getZimbra()->request('GetDistributionListRequest', array(), $params);
        $distributionLists = $response->children()->GetDistributionListResponse->children();
        $distributionList = new DistributionListEntity;
        $distributionList->populate($distributionLists[0]);
        $this->count = 1;
        return $distributionList;       
    }

    /**
     * 
     * @param string|DistributionListEntity $id
     * @param string $newName
     * @return DistributionListEntity
     */
    public function rename($id, $newName)
    {
        if ($id instanceof DistributionListEntity){
            $dlid = $id->getId();
            $distributionList = $id;
        }else{
            $dlid = $id;
            $distributionList = new DistributionListEntity;
        }
        
        $attributes = array(
            'id'      => $dlid,
            'newName' => $newName
        );

        $response = $this->getZimbra()->request('RenameDistributionListRequest', $attributes, array());
        $distributionLists = $response->children()->RenameDistributionListResponse->children();
        $distributionList->populate($distributionLists[0]);
        $this->count = 1;
        return $distributionList;
    }

    /**
     * Prend soit le nom de la nouvelle liste de distribution, soit un nouvel objet DistributionListEntity
     * 
     * @param string|DistributionListEntity $value
     * @return DistributionListEntity
     */
    public function create( $value )
    {
        $attrs = array();

        if ($value instanceof DistributionListEntity){
            $attrs['name'] = $value->getName();
            $params = $value->getConvertedChanges();
            if (isset($params['name'])) unset($params['name']);
            $params = $this->getZimbra()->makeZimbraAttributes( $params );
            $distributionList = $value;
        }elseif(is_string($value)){
            $attrs['name'] = $value;
            $params = array();
            $distributionList = new DistributionListEntity;
        }

        $response = $this->getZimbra()->request('CreateDistributionListRequest', $attrs, $params);
        $distributionLists = $response->children()->CreateDistributionListResponse->children();
        $distributionList->populate($distributionLists[0]);
        $this->count = 1;
        return $distributionList;
    }

    /**
     * 
     * @param DistributionListEntity $distributionList
     * @return DistributionListEntity
     */
    public function modify(DistributionListEntity $distributionList)
    {
        $attrs = array(
           'id' => $distributionList->getId(),
        );
        $params = $this->getZimbra()->makeZimbraAttributes( $distributionList->getConvertedChanges() );

        $response = $this->getZimbra()->request('ModifyDistributionListRequest', $attrs, $params);
        $distributionLists = $response->children()->ModifyDistributionListResponse->children();
        $distributionList->populate($distributionLists[0]);
        $this->count = 1;
        return $distributionList;
    }

    /**
     * 
     * @param string|DistributionListEntity $id
     * @return self
     */
    public function delete( $id )
    {
        if ($id instanceof DistributionListEntity) $id = $id->getId();
        $this->getZimbra()->request('DeleteDistributionListRequest', array(), array('id' => $id));
        $this->count = 1;
        return $this;
    }

    /**
     * Retourne la liste des membres de la liste de distribution
     * 
     * @param string|DistributionListEntity $name
     * @return string[]
     */
    public function getMembers( $name, $limit=Zimbra::DEFAULT_LIMIT, $offset=Zimbra::DEFAULT_OFFSET )
    {
        if ($name instanceof DistributionListEntity) $name = $name->getName();

        $attrs = array(
            'limit' => $limit,
            'offset' => $offset,
        );
        $params = array(
            'dl' => $name,
        );
        $membersXml = $this->getZimbra()->request('GetDistributionListMembersRequest', $attrs, $params, 'urn:zimbraAccount');
        $members = array();
        $this->count = $membersXml->children()->GetDistributionListMembersResponse['total'];
        foreach ($membersXml->children()->GetDistributionListMembersResponse->children() as $memberXml) {
            $members[] = (string)$memberXml;
        }
        $this->count = count($members);
        return $members;
    }

    /**
     * Ajoute des membres à une liste de distribution
     * 
     * @param string|DistributionListEntity $id
     * @param string|string[]|AccountEntity|AccountEntity[] $members
     * @return self
     */
    public function addMembers( $id, $members )
    {
        if ($id instanceof DistributionListEntity) $id = $id->getId();
        if (is_string($members)) $members = array($members);
        elseif($members instanceof AccountEntity) $members = array( $members->getName() );
        elseif(is_array($members)){
            foreach( $members as $index => $member ){
                if ($member instanceof AccountEntity) $members[$index] = $member->getName();
            }
        }
        
        $attrs = array(
            'id' => $id,
        );
        $params = array(
            '*dlm' => $members,
        );    
        $this->getZimbra()->request('AddDistributionListMemberRequest', $attrs, $params);

        return $this;
    }
    
    /**
     * Retire un ou plusieurs membres à une liste de distribution
     * 
     * @param string|DistributionListEntity $id
     * @param string|string[]|AccountEntity|AccountEntity[] $members
     * @return self
     */
    public function removeMembers( $id, $members )
    {
        if ($id instanceof DistributionListEntity) $id = $id->getId();
        if (is_string($members)) $members = array($members);
        elseif($members instanceof AccountEntity) $members = array( $members->getName() );
        elseif(is_array($members)){
            foreach( $members as $index => $member ){
                if ($member instanceof AccountEntity) $members[$index] = $member->getName();
            }
        }

        $attrs = array(
           'id' => $id,
        );
        $params = array(
            '*dlm' => $members
        );
        $this->getZimbra()->request('RemoveDistributionListMemberRequest', $attrs, $params);

        return $this;
    }
}