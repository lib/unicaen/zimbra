<?php

namespace UnicaenZimbra\Service;

use Interop\Container\ContainerInterface;
use UnicaenZimbra\Zimbra;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ZimbraDistributionListServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|DistributionListService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Zimbra $zimbra
         */
        $zimbra = $container->get('zimbra');

        $service = new ZimbraDistributionListService();
        $service->setZimbra($zimbra);

        return $service;
    }
}