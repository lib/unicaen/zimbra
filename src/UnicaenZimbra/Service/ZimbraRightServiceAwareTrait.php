<?php

namespace UnicaenZimbra\Service;

trait ZimbraRightServiceAwareTrait
{
    /**
     * @var ZimbraRightService
     */
    protected $rightService;


    /**
     * @param ZimbraRightService $rightService
     */
    public function setZimbraRightService(ZimbraRightService $rightService)
    {
        $this->rightService = $rightService;
    }

    /**
     * @return ZimbraRightService
     */
    public function getZimbraRightService()
    {
        return $this->rightService;
    }
}