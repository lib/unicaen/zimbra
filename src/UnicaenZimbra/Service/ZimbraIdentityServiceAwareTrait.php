<?php

namespace UnicaenZimbra\Service;

trait ZimbraIdentityServiceAwareTrait
{
    /**
     * @var ZimbraIdentityService
     */
    protected $identityService;


    /**
     * @param ZimbraIdentityService $identityService
     */
    public function setZimbraIdentityService(ZimbraIdentityService $identityService)
    {
        $this->identityService = $identityService;
    }

    /**
     * @return ZimbraIdentityService
     */
    public function getZimbraIdentityService()
    {
        return $this->identityService;
    }
}