<?php

namespace UnicaenZimbra\Service;

trait ZimbraFolderServiceAwareTrait
{
    /**
     * @var ZimbraFolderService
     */
    protected $folderService;


    /**
     * @param ZimbraFolderService $folderService
     */
    public function setZimbraFolderService(ZimbraFolderService $folderService)
    {
        $this->folderService = $folderService;
    }

    /**
     * @return ZimbraFolderService
     */
    public function getZimbraFolderService()
    {
        return $this->folderService;
    }
}