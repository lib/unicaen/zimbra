<?php

namespace UnicaenZimbra\Service;

trait ZimbraAliasServiceAwareTrait
{
    /**
     * @var ZimbraAliasService
     */
    protected $aliasService;


    /**
     * @param ZimbraAliasService $aliasService
     */
    public function setZimbraAliasService(ZimbraAliasService $aliasService)
    {
        $this->aliasService = $aliasService;
    }

    /**
     * @return ZimbraAliasService
     */
    public function getZimbraAliasService()
    {
        return $this->aliasService;
    }
}