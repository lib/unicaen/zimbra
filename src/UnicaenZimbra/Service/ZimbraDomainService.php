<?php


namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Domain as DomainEntity;

class ZimbraDomainService extends AbstractService
{

    /**
     * Retourne la liste des domaines
     * 
     * Les clés sont les ID des domaines
     * 
     * @return DomainEntity[]
     */
    public function getList()
    {
        $response = $this->getZimbra()->request('GetAllDomainsRequest');

        $results = array();
        foreach ($response->children()->GetAllDomainsResponse->children() as $domainXml) {
            $domain = new DomainEntity;
            $domain->populate($domainXml);
            $results[$domain->getId()] = $domain;
        }
        $this->count = count($results);
        return $results;
    }

    /**
     * Détermine si un domaine existe ou non
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return boolean
     */
    public function exists($value, $by='name')
    {
        return $this->getZimbra()->exists($value, $by, 'domains');
    }

    /**
     * Retourne un domaine en fonction de la valeur donnée et de son type (son nom par défaut)
     * @param string $value
     * @param string $by
     * @return DomainEntity
     */
    public function get( $value, $by='name' )
    {
        $params = array(
            'domain' => array(
                '@content'  => $value,
                '@attributes' => array( 'by' => $by )
            )
        );

        $response = $this->getZimbra()->request('GetDomainRequest', array(), $params);
        $domains = $response->children()->GetDomainResponse->children();
        $domain = new DomainEntity;
        $domain->populate($domains[0]);
        $this->count = 1;
        return $domain;       
    }

    /**
     * 
     * @param DomainEntity $domain
     * @return DomainEntity
     */
    public function modify(DomainEntity $domain)
    {
        $attrs = array(
           'id' => $domain->getId(),
        );
        $params = $this->getZimbra()->makeZimbraAttributes( $domain->getConvertedChanges() );

        $response = $this->getZimbra()->request('ModifyDomainRequest', $attrs, $params);
        $domains = $response->children()->ModifyDomainResponse->children();

        $domain->populate($domains[0]);
        $this->count = 1;
        return $domain;
    }

}