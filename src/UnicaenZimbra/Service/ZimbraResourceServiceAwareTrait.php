<?php

namespace UnicaenZimbra\Service;

trait ZimbraResourceServiceAwareTrait
{
    /**
     * @var ZimbraResourceService
     */
    protected $resourceService;


    /**
     * @param ZimbraResourceService $resourceService
     */
    public function setZimbraResourceService(ZimbraResourceService $resourceService)
    {
        $this->resourceService = $resourceService;
    }

    /**
     * @return ZimbraResourceService
     */
    public function getZimbraResourceService()
    {
        return $this->resourceService;
    }
}