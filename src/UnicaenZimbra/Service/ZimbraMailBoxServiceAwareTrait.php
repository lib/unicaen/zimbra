<?php

namespace UnicaenZimbra\Service;

trait ZimbraMailBoxServiceAwareTrait
{
    /**
     * @var ZimbraMailBoxService
     */
    protected $mailBoxService;


    /**
     * @param ZimbraMailBoxService $mailBoxService
     */
    public function setZimbraMailBoxService(ZimbraMailBoxService $mailBoxService)
    {
        $this->mailBoxService = $mailBoxService;
    }

    /**
     * @return ZimbraMailBoxService
     */
    public function getZimbraMailBoxService()
    {
        return $this->mailBoxService;
    }
}