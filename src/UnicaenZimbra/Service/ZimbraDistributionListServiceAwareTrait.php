<?php

namespace UnicaenZimbra\Service;

trait ZimbraDistributionListServiceAwareTrait
{
    /**
     * @var ZimbraDistributionListService
     */
    protected $distributionListService;


    /**
     * @param ZimbraDistributionListService $distributionListService
     */
    public function setZimbraDistributionListService(ZimbraDistributionListService $distributionListService)
    {
        $this->distributionListService = $distributionListService;
    }

    /**
     * @return ZimbraDistributionListService
     */
    public function getZimbraDistributionListService()
    {
        return $this->distributionListService;
    }
}