<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Gal as GalEntity;
use UnicaenZimbra\Zimbra;

class ZimbraGalService extends AbstractService
{

    /**
     * Retourne la liste des boites mail
     *
     * @param string $domain
     * @param string $name
     * @param string $type
     * @param integer $limit
     * @return GalEntity[]
     */
    public function autoComplete( $domain, $name, $type='account', $limit=Zimbra::DEFAULT_LIMIT )
    {
        $attributes = array(
            'domain' => $domain,
            'name' => $name,
            'type' => $type,
            'limit'  => $limit,
        );
        $params = array();

        $response = $this->getZimbra()->request('AutoCompleteGalRequest', $attributes, $params);

        $results = array();
        $this->count = (int)$response->children()->GetAllMailboxesResponse['searchTotal'];
        foreach ($response->children()->AutoCompleteGalResponse->children() as $cn) {
            $gal = new GalEntity;
            $gal->populate($cn);
            $results[$gal->getId()] = $gal;
        }
        return $results;
    }

}