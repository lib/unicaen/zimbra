<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Folder as FolderEntity;
use UnicaenZimbra\Entity\Account as AccountEntity;
use UnicaenZimbra\Entity\Grant as GrantEntity;
use UnicaenZimbra\Entity\Link as LinkEntity;
use UnicaenZimbra\Exception;

class ZimbraFolderService extends AbstractService
{
    use ZimbraAccountServiceAwareTrait;

    /**
     * Anvoie un mail lors d'un partage sur un destinataire
     */
    const GRANT_ACTION_SENDMAIL = 'sendMail';

    /**
     * Ajoute automatiquement le lien sur le destinataire lors d'une partage
     */
    const GRANT_ACTION_LINK = 'link';

    /**
     * Ne fait rien à la suite d'un partage
     */
    const GRANT_ACTION_NONE = 'none';

    /**
     * URN local
     */
    const URN = 'urn:zimbraMail';

    /**
     * Nom du compte référent
     * 
     * @var string|AccountEntity
     */
    protected $account;





    /**
     * 
     * @param string|AccountEntity $name
     * @return self
     */
    public function setAccount( $name ){
        $this->account = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountName(){
        if ($this->account instanceof AccountEntity){
            return $this->account->getName();
        }else{
            return $this->account;
        }
    }

    /**
     * @return AccountEntity
     */
    public function getAccount(){
        if (is_string($this->account)){
            return $this->getZimbraAccountService()->get($this->account);
        }else{
            return $this->account;
        }
    }

    /**
     * Ajout un noeud de contexte
     */
    private function applyAccountContext()
    {
        if (null == $this->account) throw new Exception('Un compte doit obligatoirement être affecté au service.');
        $this->getZimbra()->addContextChild('account', $this->getAccountName(), array('by' => 'name'));
    }

    /**
     * Retourne la liste des dossiers du compte courant
     * 
     * $keyField peur prendre la valeur "id" (par défaut) ou bien "absFolderPath" si on souhaite que les dossiers soient accessibles par leur
     * chemin d'accès absolu
     * 
     * @param string $keyField
     * @return FolderEntity[]
     */
    public function getList( $keyField = 'id' )
    {
        $this->applyAccountContext();
        $foldersXml = $this->getZimbra()->request('GetFolderRequest', array(), array(), self::URN);        

        $results = array();
        foreach ($foldersXml->children()->GetFolderResponse->children() as $folderXml) {
            $folder = new FolderEntity;
            $folder->populate($folderXml);
            $results[$folder->$keyField] = $folder;
            $this->getFlatChilds( $folder, $results, $keyField );
        }
        $this->count = count($results);
        return $results;
    }

    /**
     * Retourne la liste des liens du compte courant
     *
     * $keyField peur prendre la valeur "id" (par défaut) ou bien "absFolderPath" si on souhaite que les dossiers soient accessibles par leur
     * chemin d'accès absolu
     *
     * @param string $keyField
     * @return LinkEntity[]
     */
    public function getLinkList( $keyField = 'id' )
    {
        $folders = $this->getList();
        $results = array();
        foreach( $folders as $folder ){
            $links = $folder->getLinks();
            foreach( $links as $link ){
                $results[$link->$keyField] = $link;
            }
        }
        return $results;
    }

    /**
     * Ajoute à $list l'ensemble des sous-dossiers de $folder de manière récursive
     * 
     * @param FolderEntity $folder
     * @param array $list
     * @param string $keyField
     * @param array $results
     */
    protected function getFlatChilds( FolderEntity $folder, array &$list, $keyField='id' ){
        $children = $folder->getChildren();
        if (is_array($children)) foreach( $children as $child ){
            $list[$child->$keyField] = $child;
            $this->getFlatChilds( $child, $list, $keyField );
        }
        $this->count = count($list);
    }

    /**
     * Retourne un dossier en fonction de la valeur donnée et de son type (son chemin absolu par défaut)
     * 
     * By doit faire partie de {"uuid","id","absFolderPath","name"}, default = "absFolderPath"
     * 
     * @param string $value
     * @param string $by
     * @return FolderEntity
     */
    public function get( $value, $by='absFolderPath' )
    {
        $convert = array(
            'absFolderPath' => 'path',
            'id'            => 'l',
            'uuid'          => 'uuid',
            'name'          => 'name',
        );
        if (! isset($convert[$by]))
                throw new Exception('Le paramètre "'.$by.'" n\'est pas valide');

        $params = array(
            'folder' => array(
                $convert[$by] => $value,
            )
        );

        $this->applyAccountContext();
        $response = $this->getZimbra()->request('GetFolderRequest', array(), $params, self::URN);
        $foldersXml = $response->children()->GetFolderResponse->children();
        $folder = new FolderEntity;
        $folder->populate($foldersXml[0]);
        $this->count = 1;
        return $folder;       
    }

    /**
     * Retourne le possier parent
     *
     * $value = Chemin absolu du dossier ou bien un objet Folder dont la propriété absFolderPath a été renseignée
     *
     * @param string|FolderEntity $value
     */
    public function getParent( $value )
    {
        if ($value instanceof FolderEntity){
            $parent = $value->getParent();
            if (null !== $parent) return $parent;
            if (null == $value->absFolderPath)
                throw new Exception('Le répertoire n\'est pas positionné dans une arborescence : il n\'a pas de chemin absolu');
            $value = $value->absFolderPath;
        }

        if ('/' == $value) return null; // Pas de parent pour le dossier racine
        return $this->get( dirname( $value ) );
    }

    /**
     * Renommage / déplacement d'un dossier
     * 
     * Si le nouveau nom débute par /, alors le dossier sera déplace selon la hiérarchie correspondante.
     * La méthode crée automatiquement tous les dossiers parents nécessaires.
     * 
     * @param string|FolderEntity $id
     * @param string $newName
     * @return FolderEntity
     */
    public function rename($id, $newName)
    {
        if ($id instanceof FolderEntity){
            $fid = $id->getId();
            $folder = $id;
        }else{
            $fid = $id;
            $folder = null;
        }

        $newId = $this->action($fid,'rename',array('name' => $newName));
        $this->count = 1;
        if ($newId != $fid){
            return $this->get($newId,'id');
        }else{
            if(empty($folder)){
                $folder = $this->get($newId,'id');
            }else{
                $folder->__hasBeenRenamed($newName);
            }
            return $folder;
        }
    }

    /**
     * Prend soit le nom du nouveau dossier, soit un nouvel objet FolderEntity
     * 
     * Si une chaîne de caractères est transmise, alors le nom du dossier doit être sous forme de chemin absolu débutant par /
     * 
     * @param string|FolderEntity $value
     * @return FolderEntity
     */
    public function create( $value )
    {
        $attrs = array();
        $params = array(
            'folder' => array(),
        );

        if (is_string($value)){
            $params['folder']['name'] = $value;
            $folder = new FolderEntity;
        }  elseif ($value instanceof FolderEntity) {
            $params['folder'] = $value->getArrayCopy();
            $folder = $value;
        }

        $this->applyAccountContext();
        $response = $this->getZimbra()->request('CreateFolderRequest', $attrs, $params,self::URN);
        $foldersXml = $response->children()->CreateFolderResponse->children();
        $folder->populate($foldersXml[0]);
        $this->count = 1;
        return $folder;
    }

    /**
     * Enregistrement des changements
     * 
     * Les sous-dossiers ne sont pas concernés par les changements
     * 
     * @param FolderEntity $account
     * @return FolderEntity
     */
    public function modify(FolderEntity $folder)
    {
        $id = $folder->getId();
        $changes = $folder->getConvertedChanges();
        
        if (array() != $changes){
            $this->action( $id, 'update', $changes );
        }
        $this->count = 1;
        return $folder;
    }

    /**
     * Ajoute un droit de partage à un dossier
     *
     * @param string|FolderEntity $id
     * @param GrantEntity $grant
     * @param string $action
     * @retun self
     */
    public function addGrant( $id, GrantEntity $grant, $action=self::GRANT_ACTION_LINK ){
        if ($id instanceof FolderEntity){
            $folder = $id;
            $id = $id->getId();
        }else{
            $folder = null;
        }

        /* Ajout de la règle de partage */
        $this->action( $id, 'grant', array(
            'grant' => $grant->getConvertedToXml()
        ) );
        if (! empty($folder)) $folder->__aclGrantAdded ($grant);

        switch( $action ){
        case self::GRANT_ACTION_SENDMAIL:
            /* Envoi d'un mail au destinataire pour le partage */
            if (!in_array($grant->granteeType, array(GrantEntity::TYPE_USR, GrantEntity::TYPE_GRP))) {
                throw new Exception('L\'envoi de mail de notification ne fonctionne que si le partage se fait avec un utilisateur (GrantEntity::TYPE_USR) ou une liste de distribution (GrantEntity::TYPE_GRP)');
            }
            $this->sendGrantNotification($id, $grant->granteeName);
        break;
        case self::GRANT_ACTION_LINK:
	    if (empty($folder)) $folder = $this->get($id,'id');
            $this->createMountPoint($id, $grant->granteeName, '/'.$folder->getName().' de '.$this->getAccount()->displayName);
        break;
        }
        return $this;
    }

    /**
     *
     * @param string|FolderEntity $id
     * @param string|AccountEntity $accountId
     * @param string $action
     * @retun self
     */
    public function revokeGrant( $id, $accountId, $action=self::GRANT_ACTION_LINK ){
        if ($id instanceof FolderEntity){
            $folder = $id;
            $id = $id->getId();
        }else{
            $folder = null;
        }
        if ($accountId instanceof AccountEntity){
            $account = $accountId;
            $accountId = $accountId->getId();
        }else{
            $account = null;
        }

        $this->action( $id, '!grant', array(
          'zid' => $accountId
        ) );
        if (! empty($folder)) $folder->__aclGrantRemoved($accountId);

        switch( $action ){
        case self::GRANT_ACTION_SENDMAIL:
            /* Envoi d'un mail au destinataire pour le partage */
            if (empty($account)) $account = $this->getZimbraAccountService()->get($accountId,'id');
            $this->sendRevokeNotification($id, $account);
        break;
        case self::GRANT_ACTION_LINK:
            /* Suppression automatique du lien vers le dossier partagé sur le destinataire en cas de partage de usr à usr uniquement */
            if (empty($account)) $account = $this->getZimbraAccountService()->get($accountId,'id');
            $this->deleteMountPoint($id, $account);
        break;
        }
        return $this;
    }

    /**
     * Envoie un mail de notification de partage au destinataire
     *
     * $id = <b>Identifiant</b> de répertoire ou répertoire
     * $account = <b>adresse mail</b> ou compte
     *
     * @param string|FolderEntity $id
     * @param string|AccountEntity $account
     * @return self
     */
    public function sendGrantNotification($id, $account)
    {
        if ($id instanceof FolderEntity)        $id = $id->getId();
        if ($account instanceof AccountEntity ) $account = $account->getName();

        $params = array(
           'item' => array( '@attributes' => array( 'id' => $id ) ),
           'e' => array( '@attributes' => array( 'a' => $account ) ),
        );
        $attributes = array(
            'action' => 'edit'
        );
        $this->applyAccountContext();
        $this->getZimbra()->request('SendShareNotificationRequest', $attributes, $params, self::URN);
        return $this;
    }

    /**
     * Envoie un mail de notification de fin de partage au destinataire
     *
     * $id = <b>Identifiant</b> de répertoire ou répertoire
     * $account = <b>adresse mail</b> ou compte
     *
     * @param string|FolderEntity $id
     * @param string|AccountEntity $account
     * @return self
     */
    public function sendRevokeNotification($id, $account)
    {
        if ($id instanceof FolderEntity)        $id = $id->getId();
        if ($account instanceof AccountEntity ) $account = $account->getName();

        $params = array(
           'item' => array( '@attributes' => array( 'id' => $id ) ),
           'e' => array( '@attributes' => array( 'a' => $account ) ),
        );
        $attributes = array(
            'action' => 'revoke'
        );
        $this->applyAccountContext();
        $this->getZimbra()->request('SendShareNotificationRequest', $attributes, $params, self::URN);
        return $this;
    }

    /**
     * Monte le partage sur le compte du destinataire
     *
     * $account = <b>adresse mail</b> ou compte
     *
     * @param string|FolderEntity $id
     * @param string|AccountEntity $account
     * @param string $absPath
     * @return self
     */
    public function createMountPoint($id, $account, $absPath)
    {
        if ($id instanceof FolderEntity) $id = $id->getId();

        $currentAccount = $this->account;
        $this->account = $account;
	
        $destFolderParent = dirname( $absPath);
        if ('.' == $destFolderParent) $destFolderParent = '/';
        $destFolderName = basename( $absPath );
        $destFolderParent = $this->get($destFolderParent);

        $attributes = array();
        $params = array(
            'link' => array( '@attributes' => array(
                'name'  => $destFolderName,
                'rid'   => explode(':',$id)[1],
                'zid'   => explode(':',$id)[0],
                'owner' => $this->getAccountName(),
                'l'     => $destFolderParent->getId()
            ) )
        );
        $this->applyAccountContext();
        $this->getZimbra()->request('CreateMountpointRequest', $attributes, $params, self::URN);
	
	$this->account = $currentAccount;
        return $this;
    }

    /**
     * Démonte le partage sur le compte du destinataire
     *
     * $account = <b>adresse mail</b> ou compte
     *
     * @param string|FolderEntity $id
     * @param string|AccountEntity $account
     * @return self
     */
    public function deleteMountPoint($id, $account)
    {
        if ($id instanceof FolderEntity) $id = $id->getId();

        $currentAccount = $this->account;
        $this->account = $account;

        $granteeLinks = $this->getLinkList();
        foreach( $granteeLinks as $link ){
            $fid = $link->getRemoteFolderId();
            if ($fid == $id) $this->action($link->getId(), 'delete', array());
        }

        $this->account = $currentAccount;
        return $this;
    }

    /**
     * Exécute une action sur un dossier
     * 
     * retourne l'ID de retour du dossier
     * 
     * @param string $id
     * @param string $op
     * @param array $params
     * @return string
     */
    protected function action($id, $op, array $params )
    {
        $params = array(
            'action' => $params
        );
        $params['action']['op'] = $op;
        $params['action']['id'] = $id;

        $this->applyAccountContext();
        $response = $this->getZimbra()->request('FolderActionRequest', array(), $params, self::URN);
        $result = $response->children()->FolderActionResponse->children();
        $newId = (string)$result[0]->attributes()->id;
        return $newId;
    }

    /**
     * Suppression d'un répertoire et de tous son contenu
     * 
     * @param string|FolderEntity $id
     * @return self
     */
    public function delete( $id )
    {
        if ($id instanceof FolderEntity) $id = $id->getId();
        $this->action( $id, 'delete', array() );
        $this->count = 1;
        return $this;
    }
}