<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Right as RightEntity;

class ZimbraRightService extends AbstractService
{
    /**
     * Returns all grants on the specified target entry, or all grants granted 
     * to the specified grantee entry.
     * 
     * @param \UnicaenZimbra\Entity\Right $right
     * @return RightEntity[]
     */
    public function getGrants(RightEntity $right)
    {
	$params = $right->getConvertedToXml();
	$response = $this->getZimbra()->request('GetGrantsRequest', array(), $params);
	
	$results = array();
        foreach ($response->children()->GetGrantsResponse->children() as $rightXml) {
            $right = new RightEntity;
            $right->populate($rightXml);
            $results[] = $right;
        }
        $this->count = count($results);
        return $results;
    }
    
    /**
     * Grant a right on a target to an individual or group grantee.
     * 
     * @param \UnicaenZimbra\Entity\Right $right
     * @return boolean
     */
    public function grantRight(RightEntity $right)
    {
	$params = $right->getConvertedToXml();
	
	$this->getZimbra()->request('GrantRightRequest', array(), $params);
	$this->count = 1;
        return true;
    }
	
    /**
     * Revoke a right on a target to an individual or group grantee.
     * 
     * @param \UnicaenZimbra\Entity\Right $right
     * @return boolean
     */
    public function revokeRight(RightEntity $right)
    {
	$params = $right->getConvertedToXml();
	
	$this->getZimbra()->request('RevokeRightRequest', array(), $params);
	$this->count = 1;
        return true;
    }
}
