<?php

namespace UnicaenZimbra\Service;

use Interop\Container\ContainerInterface;
use UnicaenZimbra\Zimbra;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ZimbraAccountServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|AccountService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Zimbra $zimbra
         */
        $zimbra = $container->get('zimbra');
        $cosService = $container->get(ZimbraCosService::class);

        $service = new ZimbraAccountService();
        $service->setZimbra($zimbra);
        $service->setZimbraCosService($cosService);

        return $service;
    }
}