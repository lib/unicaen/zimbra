<?php

namespace UnicaenZimbra\Service;

trait ZimbraServerServiceAwareTrait
{
    /**
     * @var ZimbraServerService
     */
    protected $serverService;


    /**
     * @param ZimbraServerService $serverService
     */
    public function setZimbraServerService(ZimbraServerService $serverService)
    {
        $this->serverService = $serverService;
    }

    /**
     * @return ZimbraServerService
     */
    public function getZimbraServerService()
    {
        return $this->serverService;
    }
}