<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Alias as AliasEntity;
use UnicaenZimbra\Exception;
use UnicaenZimbra\Zimbra;

class ZimbraAliasService extends AbstractService
{

    /**
     * Retourne la liste des alias
     * 
     * @param string $domain
     * @param integer $limit
     * @param integer $offset
     * @param string $sort
     * @param string $query
     * @return AliasEntity[]
     */
    public function getList( $domain, $limit=Zimbra::DEFAULT_LIMIT, $offset=Zimbra::DEFAULT_OFFSET, $sort=null, $query=null )
    {
        $aliases = $this->getZimbra()->searchDirectory($domain, $limit, $offset, 'aliases', $sort, $query);

        $results = array();
        $this->count = (int)$aliases->children()->SearchDirectoryResponse['searchTotal'];
        foreach ($aliases->children()->SearchDirectoryResponse->children() as $aliasXml) {
            $alias = new AliasEntity;
            $alias->populate($aliasXml);
            $results[$alias->getId()] = $alias;
        }
        return $results;
    }

    /**
     * Retourne un objet alias en fonction de son adresse mail
     * 
     * @param string $name
     * @return AliasEntity
     */
    public function get( $name )
    {
        list( $uid, $domain ) = explode( '@', $name );
        $query = 'uid='.$uid;

        $result = $this->getZimbra()->searchDirectory($domain, 1, 0, 'aliases', null, $query);
        $count = (int)$result->children()->SearchDirectoryResponse['searchTotal'];
        if (0 == $count){
            throw new Exception('L\'alias "'.$name.'" n\a pas pu être trouvé');
        }else{
            $alias = new AliasEntity;
            $alias->populate( $result->children()->SearchDirectoryResponse->children()[0] );
            $this->count = $count;
            return $alias;
        }
    }

    /**
     * Détermine si un alias existe ou non
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return boolean
     */
    public function exists($value, $by='name')
    {
        return $this->getZimbra()->exists($value, $by, 'aliases');
    }

}