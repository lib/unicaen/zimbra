<?php

namespace UnicaenZimbra\Service;

trait ZimbraGalServiceAwareTrait
{
    /**
     * @var ZimbraGalService
     */
    protected $galService;


    /**
     * @param ZimbraGalService $galService
     */
    public function setZimbraGalService(ZimbraGalService $galService)
    {
        $this->galService = $galService;
    }

    /**
     * @return ZimbraGalService
     */
    public function getZimbraGalService()
    {
        return $this->galService;
    }
}