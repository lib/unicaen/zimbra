<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Resource as ResourceEntity;
use UnicaenZimbra\Zimbra;

class ZimbraResourceService extends AbstractService
{

    /**
     * Retourne la liste des ressources
     * 
     * Les clés sont les ID des comptes
     * 
     * @param string $domain
     * @param integer $limit
     * @param integer $offset
     * @param string $sort
     * @param string $query
     * @return ResourceEntity[]
     */
    public function getList( $domain, $limit=Zimbra::DEFAULT_LIMIT, $offset=Zimbra::DEFAULT_OFFSET, $sort=null, $query=null )
    {
        $resources = $this->getZimbra()->searchDirectory($domain, $limit, $offset, 'resources', $sort, $query);

        $results = array();
        $this->count = (int)$resources->children()->SearchDirectoryResponse['searchTotal'];
        foreach ($resources->children()->SearchDirectoryResponse->children() as $resourceXml) {
            $resource = new ResourceEntity;
            $resource->populate($resourceXml);
            $results[$resource->getId()] = $resource;
        }
        return $results;
    }

   /**
     * Détermine si une ressource existe ou non
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return boolean
     */
    public function exists($value, $by='name')
    {
        return $this->getZimbra()->exists($value, $by, 'resources');
    }

    /**
     * Retourne une ressource en fonction de la valeur donnée et de son type (son nom par défaut)
     * @param string $value
     * @param string $by
     * @return ResourceEntity
     */
    public function get( $value, $by='name' )
    {
        $params = array(
            'calresource' => array(
                '@content'  => $value,
                '@attributes' => array( 'by' => $by )
            )
        );

        $response = $this->getZimbra()->request('GetCalendarResourceRequest', array(), $params);
        $calresources = $response->children()->GetCalendarResourceResponse->children();
        $resource = new ResourceEntity;
        $resource->populate($calresources[0]);
        $this->count = 1;
        return $resource;       
    }
 
    /**
     * 
     * @param string|ResourceEntity $id
     * @param string $newName
     * @return ResourceEntity
     */
    public function rename($id, $newName)
    {
        if ($id instanceof ResourceEntity){
            $rid = $id->getId();
            $resource = $id;
        }else{
            $rid = $id;
            $resource = new ResourceEntity;
        }
        
        $attributes = array(
            'id'    => $rid,
            'newName' => $newName
        );

        $response = $this->getZimbra()->request('RenameCalendarResourceRequest', $attributes, array());
        $resources = $response->children()->RenameCalendarResourceResponse->children();
        $resource->populate($resources[0]);
        $this->count = 1;
        return $resource;
    }

    /**
     * Prend soit le nom de la nouvelle ressource, soit un nouvel objet ResourceEntity
     * 
     * Le paramètre displayName n'est utile que si $value est une chaine de caractères
     * 
     * @param ResourceEntity $value
     * @return ResourceEntity
     */
    public function create( ResourceEntity $resource )
    {
        $attrs = array();

        $attrs['name'] = $resource->getName();
        $params = $resource->getConvertedChanges();
        if (isset($params['name'])) unset($params['name']);
        $params = $this->getZimbra()->makeZimbraAttributes( $params );

        $response = $this->getZimbra()->request('CreateCalendarResourceRequest', $attrs, $params);
        $resources = $response->children()->CreateCalendarResourceResponse->children();
        $resource->populate($resources[0]);
        $this->count = 1;
        return $resource;
    }

    /**
     * 
     * @param ResourceEntity $account
     * @return ResourceEntity
     */
    public function modify(ResourceEntity $resource)
    {
        $attrs = array(
           'id' => $resource->getId(),
        );
        $params = $this->getZimbra()->makeZimbraAttributes( $resource->getConvertedChanges() );

        $response = $this->getZimbra()->request('ModifyCalendarResourceRequest', $attrs, $params);
        $resources = $response->children()->ModifyCalendarResourceResponse->children();

        $resource->populate($resources[0]);
        $this->count = 1;
        return $resource;
    }

    /**
     * 
     * @param string|ResourceEntity $id
     * @return boolean
     */
    public function delete( $id )
    {
        if ($id instanceof ResourceEntity) $id = $id->getId();
        $this->getZimbra()->request('DeleteCalendarResourceRequest', array(), array('id' => $id));
        $this->count = 1;
        return true;
    }

}