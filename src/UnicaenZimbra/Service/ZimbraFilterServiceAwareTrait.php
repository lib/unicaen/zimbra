<?php

namespace UnicaenZimbra\Service;

trait ZimbraFilterServiceAwareTrait
{
    /**
     * @var ZimbraFilterService
     */
    protected $filterService;


    /**
     * @param ZimbraFilterService $filterService
     */
    public function setZimbraFilterService(ZimbraFilterService $filterService)
    {
        $this->filterService = $filterService;
    }

    /**
     * @return ZimbraFilterService
     */
    public function getZimbraFilterService()
    {
        return $this->filterService;
    }
}