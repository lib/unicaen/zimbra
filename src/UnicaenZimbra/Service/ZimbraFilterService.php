<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Filter\Filter as FilterEntity;
use UnicaenZimbra\Entity\Account as AccountEntity;
use UnicaenZimbra\Exception;

class ZimbraFilterService extends AbstractService
{
    use ZimbraAccountServiceAwareTrait;

    /**
     * URN local
     */
    const URN = 'urn:zimbraMail';

    /**
     * Nom du compte référent
     *
     * @var string
     */
    protected $account;





    /**
     * 
     * @param string|AccountEntity $name
     * @return self
     */
    public function setAccount( $name ){
        $this->account = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountName(){
        if ($this->account instanceof AccountEntity){
            return $this->account->getName();
        }else{
            return $this->account;
        }
    }

    /**
     * @return AccountEntity
     */
    public function getAccount(){
        if (is_string($this->account)){
            return $this->getZimbraAccountService()->get($this->account);
        }else{
            return $this->account;
        }
    }

    /**
     * Ajout un noeud de contexte
     */
    private function applyAccountContext()
    {
        if (null == $this->account) throw new Exception('Un compte doit obligatoirement être affecté au service.');
        $this->getZimbra()->addContextChild('account', $this->getAccountName(), array('by' => 'name'));
    }

    /**
     * Retourne les filtres en entrée
     *
     * Les clés sont les noms des filtres
     *
     * @return FilterEntity[]
     */
    public function getInput()
    {
        $this->applyAccountContext();
        $results = array();

        $response = $this->getZimbra()->request('GetFilterRulesRequest', array(), array(), self::URN);
        foreach ($response->children()->GetFilterRulesResponse->filterRules->children() as $filterXml) {
            $filter = new FilterEntity;
            $filter->populate($filterXml);
            $results[$filter->getName()] = $filter;
        }

        $this->count = count($results);
        return $results;
    }

    /**
     * Ajoute un filtre en entrée
     *
     * @param FilterEntity $filter
     */
    public function createInput( FilterEntity $filter )
    {
        $filters = $this->getInput();
        $filters[$filter->getName()] = $filter;
        $this->setInput($filters);
    }

    /**
     * Supprime un filtre en entrée à partir de son nom ou d'un objet
     *
     * @param FilterEntity|string $filter
     */
    public function deleteInput( $filter )
    {
        if ($filter instanceof FilterEntity) $filter = $filter->getName();
        $filters = $this->getInput();
        unset($filters[$filter]);
        $this->setInput($filters);
    }

   /**
     * Retourne les filtres en sortie
     *
     * Les clés sont les noms des filtres
     *
     * @return FilterEntity[]
     */
    public function getOutput()
    {
        $this->applyAccountContext();
        $results = array();

        $response = $this->getZimbra()->request('GetOutgoingFilterRulesRequest', array(), array(), self::URN);
        foreach ($response->children()->GetOutgoingFilterRulesResponse->filterRules->children() as $filterXml) {
            $filter = new FilterEntity;
            $filter->populate($filterXml);
            $results[$filter->getName()] = $filter;
        }

        $this->count = count($results);
        return $results;
    }

    /**
     * Ajoute un filtre en sortie
     *
     * @param FilterEntity $filter
     */
    public function createOutput( FilterEntity $filter )
    {
        $filters = $this->getOutput();
        $filters[$filter->getName()] = $filter;
        $this->setOutput($filters);
    }

    /**
     * Supprime un filtre en sortie à partir de son nom ou d'un objet
     *
     * @param FilterEntity|string $filter
     */
    public function deleteOutput( $filter )
    {
        if ($filter instanceof FilterEntity) $filter = $filter->getName();
        $filters = $this->getOutput();
        unset($filters[$filter]);
        $this->setOutput($filters);
    }

    /**
     * Applique des filtres en entrée
     * 
     * @param FilterEntity[] $filters
     * @return self
     */
    public function setInput( array $filters )
    {
        $params = $this->makeParams($filters);
        $this->applyAccountContext();
        $this->getZimbra()->request('ModifyFilterRulesRequest', array(), $params, self::URN);
        return $this;
    }

    /**
     * Applique des filtres en sortie
     *
     * @param FilterEntity[] $filters
     * @return self
     */
    public function setOutput( array $filters )
    {
        $params = $this->makeParams($filters);
        $this->applyAccountContext();
        $this->getZimbra()->request('ModifyOutgoingFilterRulesRequest', array(), $params, self::URN);
        return $this;
    }

    /**
     * Construit un tableau de paramètres prêt à être envoyé sous forme de requête à Zimbra
     * @param FilterEntity[] $filters
     * @return array
     */
    protected function makeParams( array $filters )
    {
        $rules = array();
        foreach( $filters as $filter ){
            $rule = array(
               '@attributes' => $filter->getConvertedChanges()
            );
            $rule['filterTests'] = array(
                '@attributes' => array(
                    'condition' => $rule['@attributes']['condition'],
                ),
            );

            foreach( $filter->getTests() as $index => $test ){
                $testName = $test->getType();
                if (! isset($rule['filterTests']['*'.$testName])) $rule['filterTests']['*'.$testName] = array();

                $attrs = $test->getConvertedChanges();
                $attrs['index'] = $index;

                $rule['filterTests']['*'.$testName][] = array( '@attributes' => $attrs );
            }
            unset($rule['@attributes']['condition']);

            $rule['filterActions'] = array();

            foreach( $filter->getActions() as $index => $action ){
                $actionName = $action->getType();
                if (! isset($rule['filterActions']['*'.$actionName])) $rule['filterActions']['*'.$actionName] = array();

                $attrs = $action->getConvertedChanges();
                $attrs['index'] = $index;

                $rule['filterActions']['*'.$actionName][] = array( '@attributes' => $attrs );
            }

            $rules[] = $rule;
        }

        $params = array(
            'filterRules' => array(
                '*filterRule' => $rules
            )
        );

        //echo '<code><pre>'.str_replace( "  ", '&nbsp;&nbsp;&nbsp;&nbsp;', var_export($params, true) ).'</pre></code>';
        return $params;
    }
}