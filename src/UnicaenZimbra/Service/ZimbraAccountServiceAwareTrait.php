<?php

namespace UnicaenZimbra\Service;

trait ZimbraAccountServiceAwareTrait
{
    /**
     * @var ZimbraAccountService
     */
    protected $accountService;


    /**
     * @param ZimbraAccountService $accountService
     */
    public function setZimbraAccountService(ZimbraAccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @return ZimbraAccountService
     */
    public function getZimbraAccountService()
    {
        return $this->accountService;
    }
}