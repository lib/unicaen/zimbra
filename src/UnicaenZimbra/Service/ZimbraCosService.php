<?php

/**
 * Service de classes de services (COS) Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Cos as CosEntity;

class ZimbraCosService extends AbstractService
{

    /**
     * Retourne la liste des COS
     * 
     * Les clés sont les ID des COS
     * 
     * @return CosEntity[]
     */
    public function getList()
    {
        $response = $this->getZimbra()->request('GetAllCosRequest');

        $results = array();
        foreach ($response->children()->GetAllCosResponse->children() as $cosXml) {
            $cos = new CosEntity;
            $cos->populate($cosXml);
            $results[$cos->getId()] = $cos;
        }
        $this->count = count($results);
        return $results;
    }

    /**
     * Détermine si une COS existe ou non
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return boolean
     */
    public function exists($value, $by='name')
    {
        return $this->getZimbra()->exists($value, $by, 'coses');
    }

    /**
     * Retourne un COS en fonction de la valeur donnée et de son type (son nom par défaut)
     * 
     * @param string $value
     * @param string $by
     * @return CosEntity
     */
    public function get( $value, $by='name' )
    {
        $params = array(
            'cos' => array(
                '@content' => $value,
                '@attributes' => array( 'by' => $by ),
            )
        );

        $response = $this->getZimbra()->request('GetCosRequest', array(), $params);
        $coses = $response->children()->GetCosResponse->children();
        $cos = new CosEntity;
        $cos->populate($coses[0]);
        $this->count = 1;
        return $cos;       
    }

}