<?php

namespace UnicaenZimbra\Service;

trait ZimbraDomainServiceAwareTrait
{
    /**
     * @var ZimbraDomainService
     */
    protected $domainService;


    /**
     * @param ZimbraDomainService $domainService
     */
    public function setZimbraDomainService(ZimbraDomainService $domainService)
    {
        $this->domainService = $domainService;
    }

    /**
     * @return ZimbraDomainService
     */
    public function getZimbraDomainService()
    {
        return $this->domainService;
    }
}