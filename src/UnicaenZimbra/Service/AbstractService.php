<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Zimbra;

abstract class AbstractService
{
    /**
     * @var Zimbra
     */
    protected $zimbra;

    /**
     * Nombre d'occurences renvoyées par la dernière requête en date
     *
     * @var integer
     */
    protected $count;

    /**
     * @param Zimbra $zimbra
     * @return $this
     */
    public function setZimbra(Zimbra $zimbra) : self
    {
        $this->zimbra = $zimbra;

        return $this;
    }

    /**
     * @return Zimbra
     */
    public function getZimbra() : Zimbra
    {
        return $this->zimbra;
    }

    /**
     * Retourne le nombre d'occurences renvoyées par la dernière requête renvoyant des données
     *
     * @return integer
     */
    public function getLastCount()
    {
        return $this->count;
    }
}