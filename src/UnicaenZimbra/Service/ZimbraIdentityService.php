<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Identity as IdentityEntity;
use UnicaenZimbra\Entity\Account as AccountEntity;
use UnicaenZimbra\Exception;

class ZimbraIdentityService extends AbstractService
{
    use ZimbraAccountServiceAwareTrait;

    /**
     * URN local
     */
    const URN = 'urn:zimbraAccount';

    /**
     * Nom du compte référent
     *
     * @var string|AccountEntity
     */
    protected $account;


    


    /**
     *
     * @param string|AccountEntity $name
     * @return self
     */
    public function setAccount( $name ){
        $this->account = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountName(){
        if ($this->account instanceof AccountEntity){
            return $this->account->getName();
        }else{
            return $this->account;
        }
    }

    /**
     * @return AccountEntity
     */
    public function getAccount(){
        if (is_string($this->account)){
            return $this->getZimbraAccountService()->get($this->account);
        }else{
            return $this->account;
        }
    }

    /**
     * Ajout un noeud de contexte
     */
    private function applyAccountContext()
    {
        if (null == $this->account) throw new Exception('Un compte doit obligatoirement être affecté au service.');
        $this->getZimbra()->addContextChild('account', $this->getAccountName(), array('by' => 'name'));
    }

    /**
     * Retourne la liste des identités
     * 
     * $keyField = {name|id}
     *
     * @param string $keyField
     * @return IdentityEntity[]
     */
    public function getList( $keyField = 'name' )
    {
        $this->applyAccountContext();
        $response = $this->getZimbra()->request('GetIdentitiesRequest', array(), array(), self::URN);

        $results = array();
        foreach ($response->children()->GetIdentitiesResponse->children() as $identityXml) {
            $identity = new IdentityEntity;
            $identity->populate($identityXml);
            $results[$identity->$keyField] = $identity;
        }
        $this->count = count($results);
        return $results;
    }

    /**
     * Détermine si une identité existe ou non
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return boolean
     */
    public function exists($value, $by='name')
    {
        $list = $this->getList( $by );
        return isset($list[$value]);
    }

    /**
     * Retourne une identité en fonction de la valeur donnée et de son type (son nom par défaut)
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return IdentityEntity
     */
    public function get( $value, $by='name' )
    {
        $list = $this->getList( $by );
        if (! isset($list[$value])){
            throw new Exception('L\'identité "'.$value.'" est introuvable');
        }
        $this->count = 1;
        return $list[$value];
    }

    /**
     * Création d'une nouvelle identité
     * 
     * @param IdentityEntity $value
     * @return IdentityEntity
     */
    public function create( $identity )
    {
        $attrs = array();

        $attrs['name'] = $identity->getName();
        $params = $identity->getConvertedChanges();
        if (isset($params['name'])) unset($params['name']);

        $params = $this->getZimbra()->makeZimbraAttributes( $params, 'name' );
        $params['@attributes'] = array(
            'name' => $identity->getName(),
        );
        $params = array(
            'identity' => $params
        );

        $this->applyAccountContext();
        $response = $this->getZimbra()->request('CreateIdentityRequest', $attrs, $params, self::URN);
        $identitiesXml = $response->children()->CreateIdentityResponse->children();
        $identity->populate($identitiesXml[0]);
        $this->count = 1;
        return $identity;
    }

    /**
     * 
     * @param IdentityEntity $identity
     * @return IdentityEntity
     */
    public function modify(IdentityEntity $identity)
    {
        $attrs = array(
           'id' => $identity->getId(),
        );
        $params = $identity->getConvertedChanges();
        $params = $this->getZimbra()->makeZimbraAttributes( $params, 'name' );
        $params['@attributes'] = array(
            'name' => $identity->getName(),
        );
        $params = array(
            'identity' => $params
        );

        $this->applyAccountContext();
        $response = $this->getZimbra()->request('ModifyIdentityRequest', $attrs, $params,self::URN);
        $this->count = 1;
        return $identity;
    }

    /**
     * 
     * @param IdentityEntity|string $id
     * @return boolean
     */
    public function delete( $id )
    {
        if ($id instanceof IdentityEntity) $id = $id->getId();
        $this->applyAccountContext();

        $params = array(
            'identity' => array(
                '@attributes' => array(
                    'id' => $id,
                ),
            ),
        );

        $this->getZimbra()->request('DeleteIdentityRequest', array(), $params,self::URN);
        $this->count = 1;
        return true;
    }
}