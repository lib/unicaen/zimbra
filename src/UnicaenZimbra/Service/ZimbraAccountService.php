<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Account as AccountEntity;
use UnicaenZimbra\Entity\Cos;
use UnicaenZimbra\Zimbra;

class ZimbraAccountService extends AbstractService
{
    use ZimbraCosServiceAwareTrait;

    /**
     * Retourne la liste des comptes
     * 
     * Les clés sont les ID des comptes
     * 
     * @param string $domain
     * @param integer $limit
     * @param integer $offset
     * @param string $sort
     * @param string $query
     * @return AccountEntity[]
     * @throws \UnicaenZimbra\Exception
     */
    public function getList( $domain, $limit = Zimbra::DEFAULT_LIMIT, $offset = Zimbra::DEFAULT_OFFSET, $sort = null, $query = null ): array
    {
        $accounts = $this->getZimbra()->searchDirectory($domain, $limit, $offset, 'accounts', $sort, $query);

        $results = array();
        $this->count = (int)$accounts->children()->SearchDirectoryResponse['searchTotal'];
        foreach ($accounts->children()->SearchDirectoryResponse->children() as $accountXml) {
            $account = new AccountEntity;
            $account->populate($accountXml);
            $results[$account->getId()] = $account;
        }

        return $results;
    }

    /**
     * Détermine si un compte existe ou non
     *
     * By = {name|id}
     *
     * @param string $value
     * @param string $by
     * @return bool
     * @throws \UnicaenZimbra\Exception
     */
    public function exists(string $value, string $by = 'name'): bool
    {
        return $this->getZimbra()->exists($value, $by, 'accounts');
    }

    /**
     * Retourne un compte en fonction de la valeur donnée et de son type (son nom par défaut)
     *
     * @param string $value
     * @param string $by
     * @return AccountEntity
     * @throws \UnicaenZimbra\Exception
     */
    public function get(string $value, string $by = 'name'): AccountEntity
    {
        $params = array(
            'account' => array(
                '@content' => $value,
                '@attributes' => array('by' => $by)
            )
        );

        $response = $this->getZimbra()->request('GetAccountRequest', array(), $params);
        $accounts = $response->children()->GetAccountResponse->children();
        $account = new AccountEntity;
        $account->populate($accounts[0]);
        $this->count = 1;

        return $account;
    }
 
    /**
     * Recherche un ou plusieurs comptes à partir d'une requête LDAP
     *
     * @param string $query
     * @param integer $limit
     * @param integer $offset
     * @return AccountEntity[]
     * @throws \UnicaenZimbra\Exception
     */
    public function search($query = '', $limit = Zimbra::DEFAULT_LIMIT, $offset = Zimbra::DEFAULT_OFFSET): array
    {
        $params = array(
            'query' => $query,
            'limit' => $limit,
            'offset' => $offset
        );
        $accounts = $this->getZimbra()->request('SearchAccountsRequest', array(), $params);

        $results = array();
        $this->count = (int)$accounts->children()->SearchAccountsRequest['searchTotal'];
        foreach ($accounts->children()->SearchAccountsResponse->children() as $accountXml) {
            $account = new AccountEntity;
            $account->populate($accountXml);
            $results[$account->getId()] = $account;
        }

        return $results;
    }

    /**
     * Retourne les quotas d'utilisation par compte
     *
     * Return = array( account name => array( used, limit ) )
     *
     * @param array $attributes
     * @param array $sort
     * @param string $targetServer
     * @return array
     * @throws \UnicaenZimbra\Exception
     */
    public function getQuotas(array $attributes, $sort = null, $targetServer = null): array
    {
        $results = array();

        if (!empty($sort)) {
            list($attributes['sortBy'], $attributes['sortAscending']) = $sort;
        }

        if (!empty($targetServer)) {
            $this->getZimbra()->addContextChild('targetServer', $targetServer);
        }
        $response = $this->getZimbra()->request('GetQuotaUsageRequest', $attributes);
        $quotas = $response->children()->GetQuotaUsageResponse->children();

        foreach ($quotas as $quota) {
            $account = explode('@', $quota['name']);
            $b = explode('.', $account[0]);

            if (!in_array($b[0], $this->getZimbra()->getSystemUsers())) {
                $results[(string)$quota['name']] = array(
                    'used' => (integer)$quota['used'],
                    'limit' => (integer)$quota['limit'],
                );
            }
        }

        return $results;
    }

    /**
     * Renomme un compte
     * 
     * @param string|AccountEntity $id
     * @param string $newName
     * @return AccountEntity
     * @throws \UnicaenZimbra\Exception
     */
    public function rename($id, $newName): AccountEntity
    {
        if ($id instanceof AccountEntity) {
            $aid = $id->getId();
            $account = $id;
        } else {
            $aid = $id;
            $account = new AccountEntity;
        }

        $attributes = array(
            'id' => $aid,
            'newName' => $newName
        );

        $response = $this->getZimbra()->request('RenameAccountRequest', $attributes, array());
        $accounts = $response->children()->RenameAccountResponse->children();
        $account->populate($accounts[0]);
        $this->count = 1;

        return $account;
    }

    /**
     * Crée un nouveau compte
     *
     * @param string|AccountEntity $value nom du compte ou objet AccountEntity
     * @param array $params
     * @return AccountEntity
     * @throws \UnicaenZimbra\Exception
     */
    public function create($value, array $params = []): AccountEntity
    {
        if ($value instanceof AccountEntity) {
            $attrs['name'] = $value->getName();
            $params = $value->getConvertedChanges();
            if (isset($params['name'])) unset($params['name']);
            if (isset($params['password'])) {
                $attrs['password'] = $params['password'];
                unset($params['password']);
            }
            $params = $this->getZimbra()->makeZimbraAttributes($params);
            $account = $value;
        } elseif (is_string($value)) {
            $attrs['name'] = $value;
            if (isset($params['name'])) unset($params['name']);
            if (isset($params['password'])) {
                $attrs['password'] = $params['password'];
                unset($params['password']);
            }
            $params = $this->getZimbra()->makeZimbraAttributes($params);
            $account = new AccountEntity;
        }

        $response = $this->getZimbra()->request('CreateAccountRequest', $attrs, $params);
        $accounts = $response->children()->CreateAccountResponse->children();
        $account->populate($accounts[0]);
        $this->count = 1;

        return $account;
    }

    /**
     * Modifie un compte
     * 
     * @param AccountEntity $account
     * @return AccountEntity
     * @throws \UnicaenZimbra\Exception
     */
    public function modify(AccountEntity $account)
    {
        $attrs = array(
            'id' => $account->getId(),
        );
        $params = $this->getZimbra()->makeZimbraAttributes($account->getConvertedChanges());

        $response = $this->getZimbra()->request('ModifyAccountRequest', $attrs, $params);
        $accounts = $response->children()->ModifyAccountResponse->children();
        $account->populate($accounts[0]);
        $this->count = 1;

        return $account;
    }

    /**
     * Supprime un compte
     * 
     * @param AccountEntity|string $id
     * @return bool
     * @throws \UnicaenZimbra\Exception
     */
    public function delete($id): bool
    {
        if ($id instanceof AccountEntity) $id = $id->getId();
        $this->getZimbra()->request('DeleteAccountRequest', array('id' => $id), array());
        $this->count = 1;

        return true;
    }

    /**
     * Ajoute un alias de messagerie
     * 
     * @param string|AccountEntity $id
     * @param string $alias
     * @return self
     * @throws \UnicaenZimbra\Exception
     */
    public function addAlias($id, $alias): self
    {
        if ($id instanceof AccountEntity) {
            $account = $id;
            $id = $id->getId();
        } else {
            $account = null;
        }

        $attributes = array(
            'id' => $id,
            'alias' => $alias
        );

        $this->getZimbra()->request('AddAccountAliasRequest', $attributes);

        if (!empty($account)) {
            $lastAlias = $account->getAlias();
            $lastAlias[] = $alias;
            $account->zimbraMailAlias = $lastAlias;
        }

        return $this;
    }

    /**
     * Modifie la conservation des messages dans la boite aux lettres Zimbra
     *
     * @param string|AccountEntity $id
     * @param bool $localDelivery
     * @return self
     * @throws \UnicaenZimbra\Exception
     */
    public function modifyLocalDelivery($id, bool $localDelivery): self
    {
        $account = $id instanceof AccountEntity ? $id : $this->get($id);
        $account->zimbraPrefMailLocalDeliveryDisabled = $localDelivery;
        $this->modify($account);

        return $this;
    }

    /**
     * Modifie la synchronisation du mobile avec Zimbra
     *
     * @param string|AccountEntity $id
     * @param bool $synchroMobile
     * @return self
     * @throws \UnicaenZimbra\Exception
     */
    public function modifySynchroMobile($id, bool $synchroMobile): self
    {
        $account = $id instanceof AccountEntity ? $id : $this->get($id);
        $cos = $this->getZimbraCosService()->get($account->zimbraCOSId, 'id');

        switch ($cos->getName()) {
            case Cos::COS_PERSONNEL:
            case Cos::COS_PERSONNEL_SYNCHRO:
                $cosName = ($synchroMobile) ? Cos::COS_PERSONNEL_SYNCHRO : Cos::COS_PERSONNEL;
                break;
            case Cos::COS_PERSONNEL_VIP:
            case Cos::COS_PERSONNEL_VIP_SYNCHRO:
                $cosName = ($synchroMobile) ? Cos::COS_PERSONNEL_VIP_SYNCHRO : Cos::COS_PERSONNEL_VIP;
                break;
        }

        $account->zimbraCOSId =  $this->getZimbraCosService()->get($cosName ?: $cos->getName())->getId();
        $this->modify($account);

        return $this;
    }


    /**
     * Supprime un alias de messagerie
     * 
     * @param string|AccountEntity $id
     * @param string $alias
     * @return self
     * @throws \UnicaenZimbra\Exception
     */
    public function removeAlias($id, string $alias): self
    {
        if ($id instanceof AccountEntity){
            $account = $id;
            $id = $id->getId();
        }else{
            $account = null;
        }

        $attributes = array(
            'id'    => $id,
            'alias' => $alias
        );

        $this->getZimbra()->request('RemoveAccountAliasRequest', $attributes, array());

       if (! empty($account)){
            $lastAlias = $account->getAlias();
            foreach( $lastAlias as $index => $a ){
                if ($a == $alias) unset($lastAlias[$index]);
            }
            $account->zimbraMailAlias = $lastAlias;
        }

        return $this;
    }

    /**
     * Ajoute une adresse de redirection
     *
     * @param string|AccountEntity $id
     * @param string $forward
     * @return self
     * @throws \UnicaenZimbra\Exception
     */
    public function addForward($id, string $forward): self
    {
        $account = $id instanceof AccountEntity ? $id : $this->get($id);
        $zimbraForward = $account->getForward();
        $zimbraForward[] = $forward;

        $account->zimbraPrefMailForwardingAddress = implode(',', $zimbraForward);
        $this->modify($account);

        return $this;
    }

    /**
     * Supprime une adresse de redirection
     *
     * @param string|AccountEntity $id
     * @param string $forward
     * @return self
     * @throws \UnicaenZimbra\Exception
     */
    public function removeForward($id, string $forward): self
    {
        $account = $id instanceof AccountEntity ? $id : $this->get($id);
        $zimbraForward = $account->getForward();
        $zimbraForward = array_filter($zimbraForward, function ($v) use ($forward) {
            return $v != $forward;
        });

        $account->zimbraPrefMailForwardingAddress = implode(',', $zimbraForward);
        if(empty($zimbraForward)) {
            $account->zimbraPrefMailLocalDeliveryDisabled = false;
        }
        $this->modify($account);

        return $this;
    }

    /**
     * Change l'adresse mail principale du compte
     * NB : l'ancien nom est conservé comme alias
     *
     * @param string|AccountEntity $id
     * @param string $newName
     * @return AccountEntity
     * @throws \UnicaenZimbra\Exception
     */
    public function changePrimaryEmail($id, $newName): AccountEntity
    {
        if ($id instanceof AccountEntity) {
            $aid = $id->getId();
            $account = $id;
        } else {
            $aid = $id;
            $account = new AccountEntity;
        }

        $params = array(
            'account' => array(
                '@content' => $aid,
                '@attributes' => array('by' => 'id')
            ),
            'newName' => $newName
        );

        $response = $this->getZimbra()->request('ChangePrimaryEmailRequest', [], $params);
        $accounts = $response->children()->ChangePrimaryEmailResponse->children();
        $account->populate($accounts[0]);
        $this->count = 1;

        return $account;
    }
}