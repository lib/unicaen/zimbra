<?php

namespace UnicaenZimbra\Service;

use UnicaenZimbra\Entity\Server as ServerEntity;

class ZimbraServerService extends AbstractService
{

    /**
     * Retourne la liste des serveurs
     * 
     * Les clés sont les ID des serveurs
     * 
     * @return ServerEntity[]
     */
    public function getList()
    {
        $response = $this->getZimbra()->request('GetAllServersRequest');

        $results = array();

        foreach ($response->children()->GetAllServersResponse->children() as $serverXml) {
            $server = new ServerEntity;
            $server->populate($serverXml);
            $results[$server->getId()] = $server;
        }
        $this->count = count($results);
        return $results;
    }

    /**
     * Retourne un serveur en fonction de la valeur donnée et de son type (son nom par défaut)
     * @param string $value
     * @param string $by
     * @return ServerEntity
     */
    public function get( $value, $by='name' )
    {
        $params = array(
            'server' => array(
                '@content'  => $value,
                '@attributes' => array( 'by' => $by )
            )
        );

        $response = $this->getZimbra()->request('GetServerRequest', array(), $params);
        $servers = $response->children()->GetServerResponse->children();
        $server = new ServerEntity;
        $server->populate($servers[0]);
        $this->count = 1;
        return $server;       
    }

}