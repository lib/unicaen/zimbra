<?php

namespace UnicaenZimbra\Service;

use Interop\Container\ContainerInterface;
use UnicaenZimbra\Zimbra;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ZimbraIdentityServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|IdentityService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var Zimbra $zimbra
         */
        $zimbra = $container->get('zimbra');
        $accountService = $container->get(ZimbraAccountService::class);

        $service = new ZimbraIdentityService();
        $service->setZimbra($zimbra);
        $service->setZimbraAccountService($accountService);

        return $service;
    }
}