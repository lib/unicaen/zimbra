<?php

/**
 * classe qui permet de gérer une règle de partage de dossier
 * 
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
namespace UnicaenZimbra\Entity;

use UnicaenZimbra\Entity\Account as AccountEntity;
use SimpleXMLElement;
use UnicaenZimbra\Exception;

/**
 * @property string $perm
 * @property string $granteeType
 * @property string $granteeId
 * @property string $granteeName
 * @property string $guestPassword
 * @property string $accessKey
 */
class Grant
{
    /**
     * Permission de lecture seule
     */
    const PERM_READ = 'r';

    /**
     * Permission de gérer le dossier
     */
    const PERM_MANAGE = 'rwidx';

    /**
     * Permission d'administrer le dossier
     */
    const PERM_ADMIN = 'rwidxa';

    const TYPE_USR = 'usr';

    const TYPE_GRP = 'grp';

    const TYPE_COS = 'cos';

    const TYPE_DOM = 'dom';

    const TYPE_ALL = 'all';

    const TYPE_PUB = 'pub';

    const TYPE_GUEST = 'guest';

    const TYPE_KEY = 'key';

    private $granteeTypes = array(
        self::TYPE_USR   => 'User',
        self::TYPE_GRP   => 'Group',
        self::TYPE_COS   => 'Class of services',
        self::TYPE_DOM   => 'Domain',
        self::TYPE_ALL   => 'All',
        self::TYPE_PUB   => 'Public',
        self::TYPE_GUEST => 'Guests',
        self::TYPE_KEY   => 'Key'
    );

    /**
     * Champs publics
     * 
     * @var string[]
     */
    private $publicFields = array(
        'perm', 'granteeType', 'granteeId', 'granteeName', 'guestPassword', 'accessKey'
    );

    /**
     * Nature du droit
     * 
     * @var string
     */
    private $perm;

    /**
     * Définie la portée du droit
     * Valeurs possibles :  {usr|grp|cos|dom|all|pub|guest|key}
     * 
     * @var string
     */
    private $granteeType;

    /**
     * ID Zimbra du bénéficiaire
     * 
     * @var string
     */
    private $granteeId;

    /**
     * Nom ou adresse mail du bénéficiaire (doit être <code>null</code> si granteeType est sur "all" ou "pub"
     * 
     * @var string
     */
    private $granteeName;

    /**
     * Mot de passe invité (nécessaire que si granteeType est sur "guest")
     * 
     * @var string
     */
    private $guestPassword;

    /**
     * Clé de contrôle d'accès
     * 
     * Utilise si granteeType est sur "key"
     * 
     * @var string
     */
    private $accessKey;


    public function __get( $name )
    {
        if (! in_array($name, $this->publicFields))
            throw new Exception('La propriété '.$name.' est inconnue ou n\'est pas accessible');
        
        return $this->$name;
    }

    public function __set($name, $value)
    {
        if (! in_array($name, $this->publicFields) && 'changed' != $name)
            throw new Exception('La propriété '.$name.' est inconnue ou n\'est pas modifiable');

        $this->$name = $value;
        if ('changed' != $name) $this->changed = true;
    }

    /**
     * Le compte transmis devient la cible du présent droit.
     *
     * @param AccountEntity $account
     */
    public function setGrantee( AccountEntity $account ){
        $this->granteeId = $account->getId();
        $this->granteeName = $account->getName();
        $this->granteeType = 'usr';
    }

    /**
     * Détermine si une propriété de l'objet a changé ou non
     * 
     * @return boolean
     */
    public function getChanged()
    {
        return $this->changed;
    }
    
    /**
     * efface la trace des changements
     * 
     */
    public function resetChanged()
    {
        $this->changed = false;
    }

    /**
     * Retourne les propriétés de l'objet sous forme de tableau
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $result = array();
        foreach( $this->publicFields as $field ){
            $result[$field] = $this->$field;
        }
        return $result;
    }

    /**
     * Retourne la liste des attributs, avec leurs valeurs, formatées pour Zimbra
     */
    public function getConvertedToXml(){
        $result = array();
        if (! empty($this->perm))          $result['perm']  = $this->perm;
        if (! empty($this->accessKey))     $result['key']   = $this->accessKey;
        if (! empty($this->granteeId))     $result['zid']   = $this->granteeId;
        if (! empty($this->granteeName))   $result['d']     = $this->granteeName;
        if (! empty($this->granteeType))   $result['gt']    = $this->granteeType;
        if (! empty($this->guestPassword)) $result['pw']    = $this->guestPassword;
        return $result;
    }

    /**
     * Retourne la liste des types de partages
     * 
     * @return array
     */
    public function getGranteeTypes()
    {
        return $this->granteeTypes;
    }

    /**
     * Met à jour les données de l'objet à partir d'un élément Xml
     * 
     * @param SimpleXMLElement $object
     */
    public function populate(SimpleXMLElement $object)
    {
        $attributes = $object->attributes();
        if (isset($attributes->perm)) $this->perm          = (string)$attributes->perm; else $this->perm          = null;
        if (isset($attributes->key))  $this->accessKey     = (string)$attributes->key;  else $this->accessKey     = null;
        if (isset($attributes->zid))  $this->granteeId     = (string)$attributes->zid;  else $this->granteeId     = null;
        if (isset($attributes->d))    $this->granteeName   = (string)$attributes->d;    else $this->granteeName   = null;
        if (isset($attributes->gt))   $this->granteeType   = (string)$attributes->gt;   else $this->granteeType   = null;
        if (isset($attributes->pw))   $this->guestPassword = (string)$attributes->pw;   else $this->guestPassword = null;
    }

    /**
     *
     * @param string $name
     */
    public function setName( $name )
    {
        throw new Exception('Il est impossible de changer le nom d\'un droit de partage. Changez plutôt la propriété "granteeName" ou "granteeId"');
    }
}