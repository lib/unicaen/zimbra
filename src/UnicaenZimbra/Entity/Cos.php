<?php

/**
 * Classe de services
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

class Cos extends Entity
{
    const COS_ETUDIANTS = 'cos_etudiants';
    const COS_PERSONNEL = 'cos_personnel';
    const COS_PERSONNEL_SYNCHRO = 'cos_personnel_synchro';
    const COS_PERSONNEL_VIP = 'cos_personnel_6g';
    const COS_PERSONNEL_VIP_SYNCHRO = 'cos_personnel_synchro_6g';
}