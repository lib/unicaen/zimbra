<?php

/**
 * Lien Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;

class Link extends Entity
{

    public function populate( SimpleXMLElement $object )
    {
        $this->data = array();
        foreach ($object->attributes() as $attName => $attVal) {
            $this->data[(string)$attName] = (string)$attVal;
        }
        $this->id = (string) $object['id'];
        $this->name = (string) $object['name'];
    }

    /**
     * retourne l'identifiant du dossier cible du lien
     *
     * @return string
     */
    public function getRemoteFolderId()
    {
        return $this->zid.':'.$this->rid;
    }
}