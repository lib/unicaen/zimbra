<?php

/**
 * Objet Gal
 * 
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;

class Gal extends Entity
{
    public function populate(SimpleXMLElement $object)
    {
        parent::populate($object);
        $this->name = $this->data['fullName'];
    }

}