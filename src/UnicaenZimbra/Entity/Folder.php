<?php

/**
 * Dossier Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;
use UnicaenZimbra\Exception;

class Folder extends Entity
{
    /**
     *
     * @var Folder[]
     */
    protected $children = array();

    /**
     *
     * @var Folder
     */
    protected $parent;
    
    /**
     * Liste des ACL
     * 
     * @var Grant[]
     */
    protected $acl = array();

    /**
     * Liste des liens
     *
     * @var Link[]
     */
    protected $links = array();


    


    public function populate(SimpleXMLElement $object)
    {
        $this->data = array();
        $this->links = array();
        $this->children = array();
        $this->acl = array();

        $this->data['id'] = (string) $object['id'];
        $this->data['name'] = (string) $object['name'];
        $this->id = (string) $object['id'];
        $this->name = (string) $object['name'];
        
        foreach( $object->attributes() as $attKey => $attVal ){
            $this->data[(string)$attKey] = (string)$attVal;
        }
        if (0 < $object->acl->count()) foreach( $object->acl->children()->grant as $grantXml ){
            $grant = new Grant;
            $grant->populate( $grantXml );
            $this->acl[$grant->granteeId] = $grant;
        }

        foreach( $object->children()->link as $linkXml ){
            $link = new Link;
            $link->populate( $linkXml );
            $this->links[$link->getName()] = $link;
        }

        foreach ($object->children()->folder as $data) {
            $child = new self;
            $child->populate($data);
            $child->parent = $this;
            $this->children[$child->getName()] = $child;
        }
    }

    /**
     * Retourne la liste des sous-dossiers
     * 
     * @return Folder[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Retourne le dossier parent
     * 
     * @return Folder
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * 
     * @param Folder $parent
     * @return Folder
     */
    public function setParent( Folder $parent )
    {
        $this->parent = $parent;
        $this->l = $parent->getId();
        return $this;
    }

    /**
     * Retourne la liste des règles de partage (Grants) du dossier
     * 
     * @return Grant[]
     */
    public function getAcl()
    {
        return $this->acl;
    }

    /**
     * Retourne la liste des liens du dossier
     * 
     * @return Link[]
     */
    public function getLinks(){
        return $this->links;
    }

    /**
     * Getter (magic)
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        switch( $name ){
            case 'acl': return $this->acl;
            case 'links': return $this->links;
        }
        return parent::__get($name);
    }

    /**
     * Getter
     *
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->__get($name);
    }

    /**
     * Setter (magic)
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        switch( $name ){
            case 'acl': throw new Exception('Les ACL ne sont pas modifiables dans l\'objet. Utilisez pour cela le service dédié.');
            case 'links': throw new Exception('Les liens ne sont pas modifiables dans l\'objet. Utilisez pour cela le service dédié.');
        }
        return parent::__set($name, $value);
    }

    /**
     * Setter
     *
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $this->__set($name, $value);
    }

    /**
     * Mise à jour des diverses propriétés du dossier depuis le service Folder
     *
     * @param type $newName
     */
    public function __hasBeenRenamed( $newName )
    {
        $moved = 0 === strpos($newName, '/');
        if ($moved){
            $this->name = basename($newName);
            $this->absFolderPath = $newName;
            $this->data['name'] = $this->name;
            $this->parent = null;
        }else{
            $this->name = $newName;
            $this->data['name'] = $this->name;
        }
    }

    /**
     * Met à jour l'objet lorsque un partage a été ajouté
     *
     * @param Grant $grant
     */
    public function __aclGrantAdded( Grant $grant )
    {
        $this->acl[$grant->granteeId] = $grant;
    }

    /**
     * Met à jour l'objet lorsque un partage a été retiré
     *
     * @param string $accountId
     */
    public function __aclGrantRemoved( $accountId )
    {
        unset($this->acl[$accountId]);
    }

}