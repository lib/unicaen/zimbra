<?php

namespace UnicaenZimbra\Entity;

class Account extends Entity
{
    /**
     * Retourne la liste des alias
     * 
     * @return string[]
     */
    public function getAlias()
    {
        if (is_array($this->zimbraMailAlias))
            return $this->zimbraMailAlias;
        elseif(is_string($this->zimbraMailAlias))
            return [$this->zimbraMailAlias];
        else
            return [];
    }

    /**
     * Retourne la liste des forwards
     *
     * @return string[]
     */
    public function getForward()
    {
        if(is_string($this->zimbraPrefMailForwardingAddress)) {
            return preg_split('/,\s*/', $this->zimbraPrefMailForwardingAddress);
        }
        else {
            return [];
        }
    }

    /**
     * Conservation d'une copie des messages dans la boîte locale ?
     *
     * @return bool
     */
    public function getLocalDelivery()
    {
        return $this->zimbraPrefMailLocalDeliveryDisabled ? false : true;
    }

    /**
     * Synchronisation du mobile (ActiveSync) ?
     *
     * @return bool
     */
    public function getSynchroMobile()
    {
        return $this->zimbraFeatureMobileSyncEnabled;
    }

    /**
     * Retourne la liste des changements intervenus
     * Une fois récupérée, la liste des changements est remise à zéro
     *
     * @return array
     */
    public function getConvertedChanges(){
        if (isset($this->changed['zimbraMailAlias'])){
            unset($this->changed['zimbraMailAlias']);
        }
        return parent::getConvertedChanges();
    }
}