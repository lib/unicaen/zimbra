<?php

/**
 * Liste de distribution Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;

class DistributionList extends Entity
{
     public function populate( SimpleXMLElement $object )
    {
        $this->data = array();
        $this->data['id'] = (string) $object['id'];
        $this->data['name'] = (string) $object['name'];
        $this->id = (string) $object['id'];
        $this->name = (string) $object['name'];

        foreach ($object->children()->a as $data) {
            $key = (string) $data['n'];

            if (isset($this->data[$key])) {     // store multiple attributes in an array
                $this->data[$key] = (array) $this->data[$key];
                $this->data[$key][] = self::convertFromXml($data);
            } else {
                $this->data[$key] = self::convertFromXml($data);
            }
        }
	
	foreach($object->children()->dlm as $member) {
	    if (isset($this->data['members'])) {     // store multiple attributes in an array
                $this->data['members'] = (array) $this->data['members'];
                $this->data['members'][] = self::convertFromXml($member);
            } else {
                $this->data['members'] = (array) self::convertFromXml($member);
            }
	}
    }
}
