<?php

/**
 * 
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;
use UnicaenZimbra\Exception;

abstract class Entity
{
    /**
     * Identifiant d'objet
     * 
     * @var string
     */
    protected $id;

    /**
     * Nom de l'objet
     * 
     * @var string
     */
    protected $name;

    /**
     * Tableau des propriétés
     * 
     * @var array
     */
    protected $data = array();

    /**
     * Tableau des propriétés qui ont changé
     * 
     * @var array
     */
    protected $changed = array();

    public function __construct()
    {

    }

    public function populate( SimpleXMLElement $object )
    {
        $this->data = array();
        $this->data['id'] = (string) $object['id'];
        $this->data['name'] = (string) $object['name'];
        $this->id = (string) $object['id'];
        $this->name = (string) $object['name'];

        foreach ($object->children()->a as $data) {
            $key = (string) $data['n'];

            if (isset($this->data[$key])) {     // store multiple attributes in an array
                $this->data[$key] = (array) $this->data[$key];
                $this->data[$key][] = self::convertFromXml($data);
            } else {
                $this->data[$key] = self::convertFromXml($data);
            }
        }
    }

    /**
     * Retourne la liste des changements intervenus
     * Une fois récupérée, la liste des changements est remise à zéro
     * 
     * @return array
     */
    public function getConvertedChanges(){
        $changes = array();
        foreach( $this->changed as $key => $value ){
            $changes[$key] = self::convertToXml($value);
        }
        $this->changed = array();
        return $changes;
    }

    public static function convertFromXml($data)
    {
        switch ($data) {
            case 'FALSE':
                return false;
            case 'TRUE':
                return true;
            default:
                return (string) $data;
        }
    }
    
    public static function convertToXml($data)
    {
        if (is_bool($data)){
            if ($data) return 'TRUE'; else return 'FALSE';
        }else{
            return $data;
        }
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Getter (magic)
     * 
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
    }

    /**
     * Getter 
     * 
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->__get($name);
    }

    /**
     * Setter (magic)
     * 
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        if ('id' == $name){
            throw new Exception('Il est impossible de changer un identifiant d\'objet.');
        }
        $this->data[$name] = $value;
        if ('name' == $name){
            $this->name = (string)$value;
        }
        $this->changed[$name] = $value;
    }

    /**
     * Setter
     * 
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $this->__set($name, $value);
    }

    /**
     * Applique les paramètres du tableau à l'objet courant
     * 
     * @param array $data
     */
    public function exchangeArray( array $data )
    {
        foreach( $data as $key => $value ){
            $this->$key = $value;
        }
    }

    /**
     * Retourne les propriétés de l'objet sous forme de tableau
     * 
     * @return array
     */
    public function getArrayCopy()
    {
        return $this->data;
    }

    /**
     * Retourne la liste des propriétés qui ont changé
     * 
     * @return array
     */
    public function getArrayCopyChanged(){
        return $this->changed;
    }
    
    /**
     * Retourne un tableau listant toutes les propriétés de l'objet
     * 
     * @return array
     */
    public function getAttributes()
    {
        return array_keys($this->data);
    }

    /**
     * Retourne l'identifiant de l'objet
     * 
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Retourne le nom de l'objet
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 
     * @param string $name
     * @return self
     */
    public function setName( $name )
    {
        $this->name = (string)$name;
        $this->data['name'] = $name;
        return $this;
    }

}
