<?php

/**
 * Filtre Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity\Filter;

use UnicaenZimbra\Entity\Filter\Test\Test as FilterTest;
use UnicaenZimbra\Entity\Filter\Action\Action as FilterAction;
use UnicaenZimbra\Entity\Filter\Action\Stop as FilterActionStop;
use UnicaenZimbra\Entity\Entity;
use UnicaenZimbra\Stdlib\ArrayUtils;
use UnicaenZimbra\Exception;
use SimpleXMLElement;

class Filter extends Entity
{

    /**
     * Toutes les conditions doivent être remplies
     */
    const CONDITION_ALLOF = 'allof';

    /**
     * Une au moins des conditions doit être remplie
     */
    const CONDITION_ANYOF = 'anyof';

    /**
     * détermine si le filtre est actif ou non
     * 
     * @var boolean
     */
    protected $active = true;

    /**
     * Condition des tests (un des tests ou tous les tests)
     *
     * Tous les tests par défaut
     * 
     * @var string
     */
    protected $condition = self::CONDITION_ALLOF;

    /**
     * Filtres de test
     *
     * @var FilterTest[]
     */
    protected $tests = array();

    /**
     * Liste des actions à effectuer
     * 
     * @var FilterAction[]
     */
    protected $actions = array();





    /**
     * Retourne true si le filtre est actif, false sinon
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Définit si le filtre doit être actif ou non
     *
     * @param boolean $active
     * @return self
     */
    public function setActive( $active )
    {
        $this->active = (boolean)$active;
        return $this;
    }

    /**
     * Retourne la condition
     *
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Définie la condition des tests
     *
     * @param string $condition
     * @throws Exception
     * @return self
     */
    public function setCondition( $condition )
    {
        switch($condition){
            case self::CONDITION_ALLOF: $this->condition = self::CONDITION_ALLOF; break;
            case self::CONDITION_ANYOF: $this->condition = self::CONDITION_ANYOF; break;
            default:
                throw new Exception('Type de condition "'.$condition.'" erronée.');
        }
        return $this;
    }

    /**
     * retourne la liste des tests
     *
     * @return FilterTest[]
     */
    public function getTests()
    {
        return $this->tests;
    }

    /**
     * Assigne une nouvelle liste de tests au filtre
     *
     * @param FilterTest[] $tests
     * @throws Exception
     * @return self
     */
    public function setTests(array $tests)
    {
       if( !ArrayUtils::isList($tests)){
            throw new Exception('Les tests doivent être transmises sous forme de liste (les index doivent être 0, 1, 2, 3, etc.');
        }
        foreach( $tests as $test ){
            if (! $test instanceof FilterTest){
                throw new Exception('Seuls des objets hérités de UnicaenZimbra\Entity\Filter\Test\Test peuvent être transmis');
            }
        }
        $this->tests = $tests;
        return $this;
    }

    /**
     * Ajoute un test à la liste de tests
     * 
     * $index correspond à la place du test dans la liste.
     * $index ne doit pas dépasser le nombre de tests disponibles
     *
     * Si index < 0 alors le test est ajouté en dernier.
     * $index = 0 signifie que le test sera placé en premier.
     * 
     * @param FilterTest $test
     * @param integer $index
     * @return integer
     */
    public function addTest(FilterTest $test, $index=-1)
    {
        return ArrayUtils::add($this->tests, $test, $index);
    }

    /**
     * Supprime un test en fonction de son index
     *
     * @param integer $index
     */
    public function removeTest( $index )
    {
        ArrayUtils::remove($this->tests, $index);
    }

    /**
     * Remplace un test existant par un autre
     *
     * @param FilterTest $test
     * @param integer $index
     */
    public function modifyTest( FilterTest $test, $index )
    {
        if (! isset($this->tests[$index])){
            throw new Exception('L\'index "'.$index.'" n\'existe pas');
        }
        $this->tests[$index] = $test;
    }

    /**
     * Retourne la liste des actions
     *
     * @return FilterAction[]
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Assigne une nouvelle liste d'actions au filtre
     *
     * @param FilterAction[] $actions
     * @throws Exception
     * @return self
     */
    public function setActions(array $actions)
    {
        if( !ArrayUtils::isList($actions)){
            throw new Exception('Les actions doivent être transmises sous forme de liste (les index doivent être 0, 1, 2, 3, etc.');
        }
        foreach( $actions as $action ){
            if (! $action instanceof FilterAction){
                throw new Exception('Seuls des objets hérités de UnicaenZimbra\Entity\Filter\Action\Action peuvent être transmis');
            }
        }
        $this->actions = $actions;
        return $this;
    }

    /**
     * Ajoute une action à la liste des actions
     * 
     * $index correspond à la place de l'action dans la liste.
     * $index ne doit pas dépasser le nombre d'actions disponibles
     *
     * Si index < 0 alors l'action est ajoutée en dernier, JUSTE AVANT l'action de STOP si elle existe.
     * $index = 0 signifie que l'action sera placée en premier.
     * 
     * @param FilterAction $action
     * @param integer $index
     * @return integer
     */
    public function addAction(FilterAction $action, $index=-1)
    {
        if ($index < 0 && ! empty($this->actions)){
            $lastIndex = count($this->actions) - 1;
            $lastAction = $this->actions[$lastIndex];
            if ($lastAction instanceof FilterActionStop){
                $index = $lastIndex;
            }
        }
        return ArrayUtils::add($this->actions, $action, $index);
    }

    /**
     * Supprime une action en fonction de son index
     *
     * @param integer $index
     */
    public function removeAction( $index )
    {
        ArrayUtils::remove($this->actions, $index);
    }

    /**
     * Remplace une action existante par une autre
     *
     * @param FilterAction $action
     * @param integer $index
     */
    public function modifyAction( FilterAction $action, $index )
    {
        if (! isset($this->actions[$index])){
            throw new Exception('L\'index "'.$index.'" n\'existe pas');
        }
        $this->actions[$index] = $action;
    }

    /**
     * Peuple le filtre à partir d'un fichier XML issu d'une requête SOAP
     * 
     * @param SimpleXMLElement $object
     */
    public function populate( SimpleXMLElement $object )
    {
        $this->setName( (string)$object['name'] )
             ->setActive( (integer)$object['active'] == 1 )
             ->setCondition( (string)$object->filterTests['condition'] )
             ->tests = array();
        foreach( $object->filterTests->children() as $filterTestXml ){
            $this->tests[] = FilterTest::factory($filterTestXml);
        }
        $this->actions = array();
        foreach( $object->filterActions->children() as $filterActionXml ){
            $this->actions[] = FilterAction::factory($filterActionXml);
        }
    }

    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = $this->getArrayCopy();
        if (isset($result['active'])) $result['active'] = $result['active'] ? 1 : 0;
        return $result;
    }

    /**
     * Applique les paramètres du tableau à l'objet courant
     *
     * Ne fonctionne pas pour les actions et les tests, qui sont des listes d'objets
     *
     * @param array $data
     */
    public function exchangeArray( array $data )
    {
        if (isset($data['name'])) $this->setName($data['name']);
        if (isset($data['active'])) $this->setActive($data['active']);
        if (isset($data['condition'])) $this->setCondition($data['condition']);
    }

    /**
     * Retourne les propriétés de l'objet sous forme de tableau
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array(
            'name'      => $this->name,
            'active'    => $this->active,
            'condition' => $this->condition,
        );
    }

    /**
     * Retourne la liste des propriétés qui ont changé
     *
     * Comme les changements ne sont pas logués, toutes les propriétés sont systématiquement retournées
     *
     * @return array
     */
    public function getArrayCopyChanged(){
        return $this->getArrayCopy();
    }

    /**
     * Retourne un tableau listant toutes les propriétés de l'objet
     *
     * @return array
     */
    public function getAttributes()
    {
        return array('name', 'active', 'condition');
    }

    /**
     * Retourne l'identifiant du filtre qui est son nom
     *
     * @return string
     */
    public function getId()
    {
        return $this->getName();
    }
}