<?php
namespace UnicaenZimbra\Entity\Filter\Test;

use SimpleXMLElement;

/**
 * @version    $Id$
 * 
 * Test body pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Body extends Test
{
    /**
     * Valeur du test
     *
     * @var string
     */
    protected $value;

    /**
     * Si le test est sensible à la casse ou non
     *
     * @var boolean
     */
    protected $caseSensitive;

    /**
     * Si le test doit être négatif ou non
     *
     * @var boolean
     */
    protected $negative;


    /**
     * Retourne la valeur à tester
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Modifie la valeur à tester
     *
     * @param string $value
     * @return self
     */
    public function setValue( $value )
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Retourne le nom du champ à tester
     *
     * @return boolean
     */
    public function getCaseSensitive()
    {
        return $this->caseSensitive;
    }

    /**
     * Modifie la sensibilité à la casse du test
     *
     * @param string $caseSensitive
     * @return self
     */
    public function setCaseSensitive( $caseSensitive )
    {
        $this->caseSensitive = $caseSensitive;
        return $this;
    }

    /**
     * Retourne le sens du test
     *
     * @return boolean
     */
    public function getNegative()
    {
        return $this->negative;
    }

    /**
     * Modifie le sens du test
     *
     * @param string $negative
     * @return self
     */
    public function setNegative( $negative )
    {
        $this->negative = $negative;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        $this->setValue((string)$node['value']);
        if (isset($node['caseSensitive'])) $this->setCaseSensitive((integer)$node['caseSensitive'] == 1);
        if (isset($node['negative']))      $this->setNegative((integer)$node['negative'] == 1);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $result = array(
            'value'             => $this->value,
        );
        if (! empty($this->negative)) $result['negative'] = $this->negative;
        if (! empty($this->caseSensitive)) $result['caseSensitive'] = $this->caseSensitive;

        return $result;
    }


    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = parent::getConvertedChanges();
        if (isset($result['negative']))      $result['negative'] = $result['negative'] ? 1 : 0;
        if (isset($result['caseSensitive'])) $result['caseSensitive'] = $result['caseSensitive'] ? 1 : 0;
        return $result;
    }
}