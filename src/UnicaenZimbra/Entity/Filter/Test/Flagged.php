<?php
namespace UnicaenZimbra\Entity\Filter\Test;

use SimpleXMLElement;

/**
 * @version    $Id$
 * 
 * Test flag pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Flagged extends Test
{
    /**
     * Nom du flag
     *
     * @var string
     */
    protected $flagName;
    
    /**
     * Si le test doit être négatif ou non
     *
     * @var boolean
     */
    protected $negative;


    /**
     * Retourne le nom du flag
     *
     * @return string
     */
    public function getFlagName()
    {
        return $this->flagName;
    }

    /**
     * Définie le nom du flag
     *
     * @param string $flagName
     * @return self
     */
    public function setFlagName( $flagName )
    {
        $this->flagName = $flagName;
        return $this;
    }
    
    /**
     * Retourne le sens du test
     *
     * @return boolean
     */
    public function getNegative()
    {
        return $this->negative;
    }

    /**
     * Modifie le sens du test
     *
     * @param string $negative
     * @return self
     */
    public function setNegative( $negative )
    {
        $this->negative = $negative;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
	$this->setFlagName((string)$node['flagName']);
        if (isset($node['negative']))      $this->setNegative((integer)$node['negative'] == 1);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
	$result = array(
            'flagName' => $this->getFlagName(),
        );
        if (! empty($this->negative))      $result['negative'] = $this->negative;

        return $result;
    }

    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = parent::getConvertedChanges();
        if (isset($result['negative']))      $result['negative'] = $result['negative'] ? 1 : 0;
        return $result;
    }
}