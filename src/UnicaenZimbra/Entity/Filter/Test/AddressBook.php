<?php
namespace UnicaenZimbra\Entity\Filter\Test;

use SimpleXMLElement;

/**
 * @version    $Id$
 * 
 * Test addressbook pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class AddressBook extends Test
{
    /**
     * Nom du champ à tester
     *
     * @var string
     */
    protected $header;

    /**
     * Si le test doit être négatif ou non
     *
     * @var boolean
     */
    protected $negative;

    /**
     * Retourne le nom du champ à tester
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Modifie le nom du champ à tester
     *
     * @param string $value
     * @return self
     */
    public function setHeader( $header )
    {
        $this->header = $header;
        return $this;
    }

    /**
     * Retourne le sens du test
     *
     * @return boolean
     */
    public function getNegative()
    {
        return $this->negative;
    }

    /**
     * Modifie le sens du test
     *
     * @param string $negative
     * @return self
     */
    public function setNegative( $negative )
    {
        $this->negative = $negative;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        $this->setHeader((string)$node['header']);
        if (isset($node['negative']))      $this->setNegative((integer)$node['negative'] == 1);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $result = array(
            'header'            => $this->header,
        );
        if (! empty($this->negative))      $result['negative'] = $this->negative;

        return $result;
    }

    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = parent::getConvertedChanges();
        if (isset($result['negative']))      $result['negative'] = $result['negative'] ? 1 : 0;
        return $result;
    }
}