<?php

/**
 * Test pour filtre Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity\Filter\Test;

use SimpleXMLElement;
use UnicaenZimbra\Exception;

abstract class Test
{
    private static $classRoot = 'UnicaenZimbra\\Entity\\Filter\\Test\\';

    /**
     * Retourne le type de test
     */
    public function getType()
    {
        return lcfirst(substr(get_class($this), strlen(self::$classRoot))).'Test';
    }

    /**
     * Crée un test à partir d'un nœud XML
     *
     * @param SimpleXMLElement $node
     * @return Test
     * @throws Exception
     */
    public static function factory( SimpleXMLElement $node )
    {
        $nodeName = $node->getName();

        /*
         * On retire Test du nom et on ajoute une majuscule
         * Exemple : $nodeName = addressTest donnera $className = Address
         */
        $className = ucfirst( substr( $nodeName, 0, -4 ) );

        /*
         * On recherche si le fichier de classe existe ou non
         * 
         * Si non alors on lève une exception car le test demandé n'est pas implémenté ou il y a a erreur
         */
        $fileName = __DIR__.'/'.$className.'.php';
        if (! file_exists($fileName) ){
            throw new Exception( 'Type de test de filtre "'.$nodeName.'" non pris en charge.');
        }

        $className = self::$classRoot.$className;

        $test = new $className;
        $test->populate( $node );
        return $test;
    }

    abstract protected function populate( SimpleXMLElement $node );

    /**
     * Retourne les propriétés de l'objet courant sous forme de tableau
     *
     * @return array
     */
    abstract public function getArrayCopy();

    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = $this->getArrayCopy();
        return $result;
    }
}