<?php
namespace UnicaenZimbra\Entity\Filter\Test;

use SimpleXMLElement;

/**
 * @version    $Id$
 * 
 * Test pièce jointe pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Attachment extends Test
{
    /**
     * Si le test doit être négatif ou non
     *
     * @var boolean
     */
    protected $negative;


    /**
     * Retourne le sens du test
     *
     * @return boolean
     */
    public function getNegative()
    {
        return $this->negative;
    }

    /**
     * Modifie le sens du test
     *
     * @param string $negative
     * @return self
     */
    public function setNegative( $negative )
    {
        $this->negative = $negative;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        if (isset($node['negative']))      $this->setNegative((integer)$node['negative'] == 1);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $result = array();
        if (! empty($this->negative))      $result['negative'] = $this->negative;

        return $result;
    }

    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = parent::getConvertedChanges();
        if (isset($result['negative']))      $result['negative'] = $result['negative'] ? 1 : 0;
        return $result;
    }
}