<?php

/**
 * Test header pour filtre Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity\Filter\Test;

use SimpleXMLElement;

class Header extends Test
{

    /**
     * Nom du champ à tester
     *
     * @var string
     */
    protected $header;

    /**
     * Valeur du champ (adresse mail etc)
     *
     * @var string
     */
    protected $value;

    /**
     * Comparateur de chaîne (is , contains ou matches)
     *
     * @var string
     */
    protected $stringComparison;

    /**
     * Si l'adresse est sensible à la casse ou non
     *
     * @var boolean
     */
    protected $caseSensitive;

    /**
     * Si le test doit être négatif ou non
     *
     * @var boolean
     */
    protected $negative;


    


    /**
     * Retourne la valeur à tester
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Modifie la valeur à tester
     *
     * @param string $value
     * @return self
     */
    public function setValue( $value )
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Retourne le critère de comparaison à tester
     *
     * @return string
     */
    public function getStringComparison()
    {
        return $this->stringComparison;
    }

    /**
     * Modifie le critère de comparaison à tester
     *
     * @param string $stringComparison
     * @return self
     */
    public function setStringComparison( $stringComparison )
    {
        $this->stringComparison = $stringComparison;
        return $this;
    }

    /**
     * Retourne le nom du champ à tester
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Modifie le nom du champ à tester
     *
     * @param string $value
     * @return self
     */
    public function setHeader( $header )
    {
        $this->header = $header;
        return $this;
    }


    /**
     * Retourne le nom du champ à tester
     *
     * @return boolean
     */
    public function getCaseSensitive()
    {
        return $this->caseSensitive;
    }

    /**
     * Modifie la sensibilité à la casse du test
     *
     * @param string $caseSensitive
     * @return self
     */
    public function setCaseSensitive( $caseSensitive )
    {
        $this->caseSensitive = $caseSensitive;
        return $this;
    }

    /**
     * Retourne le sens du test
     *
     * @return boolean
     */
    public function getNegative()
    {
        return $this->negative;
    }

    /**
     * Modifie le sens du test
     *
     * @param string $negative
     * @return self
     */
    public function setNegative( $negative )
    {
        $this->negative = $negative;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        $this->setValue((string)$node['value'])
             ->setStringComparison((string)$node['stringComparison'])
             ->setHeader((string)$node['header']);
        if (isset($node['caseSensitive'])) $this->setCaseSensitive((integer)$node['caseSensitive'] == 1);
        if (isset($node['negative']))      $this->setNegative((integer)$node['negative'] == 1);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $result = array(
            'value'             => $this->value,
            'stringComparison'  => $this->stringComparison,
            'header'            => $this->header,
        );
        if (! empty($this->negative)) $result['negative'] = $this->negative;
        if (! empty($this->caseSensitive)) $result['caseSensitive'] = $this->caseSensitive;

        return $result;
    }


    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = parent::getConvertedChanges();
        if (isset($result['negative']))      $result['negative'] = $result['negative'] ? 1 : 0;
        if (isset($result['caseSensitive'])) $result['caseSensitive'] = $result['caseSensitive'] ? 1 : 0;
        return $result;
    }
}