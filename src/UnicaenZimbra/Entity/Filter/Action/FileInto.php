<?php

/**
 * Action de redirection vers un dossier pour filtre Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity\Filter\Action;

use UnicaenZimbra\Entity\Folder as FolderEntity;
use SimpleXMLElement;

class FileInto extends Action
{

    /**
     * Nom du dossier de destination du filtre
     *
     * @var string
     */
    protected $folderPath;





    /**
     * Retourne le chemin du répertoire de destination du filtre
     *
     * @return string
     */
    public function getFolderPath()
    {
        return $this->folderPath;
    }

    /**
     * Définie le dossier de destination du filtre
     *
     * @param string|FolderEntity $folderPath
     * @return self
     */
    public function setFolderPath( $folderPath )
    {
        if ($folderPath instanceof FolderEntity) $folderPath = $folderPath->absFolderPath;

        $this->folderPath = $folderPath;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        $this->setFolderPath((string)$node['folderPath']);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array(
            'folderPath' => $this->folderPath,
        );
    }
}