<?php
namespace UnicaenZimbra\Entity\Filter\Action;

use SimpleXMLElement;


/**
 * @version    $Id$
 * 
 * Action flag pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Flag extends Action
{

    /**
     * Nom du flag
     *
     * @var string
     */
    protected $flagName;


    /**
     * Retourne le nom du flag
     *
     * @return string
     */
    public function getFlagName()
    {
        return $this->flagName;
    }

    /**
     * Définie le nom du flag
     *
     * @param string $flagName
     * @return self
     */
    public function setFlagName( $flagName )
    {
        $this->flagName = $flagName;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        $this->setFlagName((string)$node['flagName']);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array(
            'flagName' => $this->getFlagName(),
        );
    }
}