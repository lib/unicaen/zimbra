<?php

/**
 * Action pour filtre Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity\Filter\Action;

use SimpleXMLElement;
use UnicaenZimbra\Exception;

abstract class Action
{
    
    private static $classRoot = 'UnicaenZimbra\\Entity\\Filter\\Action\\';

    /**
     * Retourne le type d'action
     */
    public function getType()
    {
        return 'action'.substr(get_class($this), strlen(self::$classRoot));
    }

    /**
     * Crée une action à partir d'un nœud XML
     *
     * @param SimpleXMLElement $node
     * @return self
     * @throws Exception
     */
    public static function factory( SimpleXMLElement $node )
    {
        $nodeName = $node->getName();

        /*
         * On retire "action" du nom
         * Exemple : $nodeName = actionFileInto donnera $className = FileInto
         */
        $className = substr( $nodeName, 6 );

        /*
         * On recherche si le fichier de classe existe ou non
         *
         * Si non alors on lève une exception car le test demandé n'est pas implémenté ou il y a a erreur
         */
        $fileName = __DIR__.'/'.$className.'.php';
        if (! file_exists($fileName) ){
            throw new Exception( 'Type d\'action de filtre "'.$nodeName.'" non pris en charge.');
        }

        $className = self::$classRoot.$className;

        $action = new $className;
        $action->populate( $node );
        return $action;
    }

    abstract protected function populate( SimpleXMLElement $node );

    /**
     * Retourne les propriétés de l'objet courant sous forme de tableau
     */
    abstract public function getArrayCopy();

    /**
     * Retourne la liste des changements intervenus
     *
     * Les changements n'étant pas logués, toutes les données sont retournées
     *
     * @return array
     */
    public function getConvertedChanges(){
        $result = $this->getArrayCopy();
        return $result;
    }
}