<?php
namespace UnicaenZimbra\Entity\Filter\Action;

use SimpleXMLElement;

/**
 * @version    $Id$
 * 
 * Action keep pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Keep extends Action
{

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        /* Aucune propriété particulière n'est requise */
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array();
    }
}
