<?php
namespace UnicaenZimbra\Entity\Filter\Action;

use SimpleXMLElement;


/**
 * @version    $Id$
 * 
 * Action redirect pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Redirect extends Action
{

    /**
     * Adresse mail du destinataire
     *
     * @var string
     */
    protected $a;


    /**
     * Retourne le nom du destinataire
     *
     * @return string
     */
    public function getA()
    {
        return $this->a;
    }

    /**
     * Définie le nom du destinataire
     *
     * @param string $a
     * @return self
     */
    public function setA( $a )
    {
        $this->a = $a;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        $this->setA((string)$node['a']);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array(
            'a' => $this->getA(),
        );
    }
}