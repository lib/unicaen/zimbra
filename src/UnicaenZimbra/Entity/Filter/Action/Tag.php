<?php
namespace UnicaenZimbra\Entity\Filter\Action;

use SimpleXMLElement;


/**
 * @version    $Id$
 * 
 * Action tag pour filtre Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Tag extends Action
{

    /**
     * Nom du tag
     *
     * @var string
     */
    protected $tagName;


    /**
     * Retourne le nom du tag
     *
     * @return string
     */
    public function getTagName()
    {
        return $this->tagName;
    }

    /**
     * Définie le nom du tag
     *
     * @param string $tagName
     * @return self
     */
    public function setTagName( $tagName )
    {
        $this->tagName = $tagName;
        return $this;
    }

    /**
     *
     * @param SimpleXMLElement $node
     * @return self
     */
    protected function populate( SimpleXMLElement $node )
    {
        $this->setTagName((string)$node['tagName']);
        return $this;
    }

    /**
     * Retourne la liste des propriétés de l'objet sous forme de tableau associatif
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return array(
            'tagName' => $this->getTagName(),
        );
    }
}