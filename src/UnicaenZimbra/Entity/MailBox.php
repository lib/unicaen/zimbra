<?php

/**
 * Objet Mailbox
 * 
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;

class MailBox extends Entity
{
    public function populate( SimpleXMLElement $object )
    {
        $this->id = (string) $object['accountId'];
        $this->name = (string) $object['accountId'];
        $this->data = array();
        if (isset($object['mbxid'])){
           $this->data['id']        = (string) $object['mbxid'];
           $this->data['accountId'] = (string) $object['accountId'];
           $this->data['size']      = (int) $object['s'];
        }else{
           $this->data['id']        = (string) $object['id'];
           $this->data['accountId'] = (string) $object['accountId'];
           $this->data['size']      = (int) $object['sizeCheckPoint'];
        }
    }

    public function getAccountId()
    {
        return $this->data['accountId'];
    }

    public function getSize()
    {
        return $this->data['size'];
    }
}
