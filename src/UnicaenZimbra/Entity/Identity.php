<?php

/**
 * Identité de Mail Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;

class Identity extends Entity
{
    public function populate( SimpleXMLElement $object )
    {
        $this->data = array();
        $this->data['id'] = (string) $object['id'];
        $this->data['name'] = (string) $object['name'];
        $this->id = (string) $object['id'];
        $this->name = (string) $object['name'];

        foreach ($object->children()->a as $data) {
            $key = (string) $data['name'];

            if (isset($this->data[$key])) {     // store multiple attributes in an array
                $this->data[$key] = (array) $this->data[$key];
                $this->data[$key][] = self::convertFromXml($data);
            } else {
                $this->data[$key] = self::convertFromXml($data);
            }
        }
    }
}