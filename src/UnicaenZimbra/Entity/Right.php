<?php

namespace UnicaenZimbra\Entity;

use SimpleXMLElement;
use UnicaenZimbra\Entity\Entity as ZimbraEntity;
use Application\Exception;

/**
 * @version    $Id$
 * 
 * Gestion des droits sur les différents éléments Zimbra
 * 
 * @author     David Surville <david.surville at unicaen.fr>
 */
class Right
{
    const TARGET_ACCOUNT	= 'account';
    const TARGET_CALRESOURCE	= 'calresource';
    const TARGET_COS		= 'cos';
    const TARGET_DL		= 'dl';		    // distribution list
    const TARGET_GROUP		= 'group';	    // dynamic group
    const TARGET_DOMAIN		= 'domain';
    const TARGET_SERVER		= 'server';
    const TARGET_UCSERVICE	= 'ucservice';	    // UC service
    const TARGET_XMPPCOMPONENT	= 'xmppcomponent';  // XMPP component
    const TARGET_ZIMLET		= 'zimlet';
    const TARGET_CONFIG		= 'config';
    const TARGET_GLOBAL		= 'global';	    // global grant
    
    const GRANTEE_USR		= 'usr';    // internal user
    const GRANTEE_GRP		= 'grp';    // internal group (distribution list)
    const GRANTEE_EGP		= 'egp';    // external AD group
    const GRANTEE_ALL		= 'all';    // all Zimbra authenticated users
    const GRANTEE_DOM		= 'dom';    // domain
    const GRANTEE_GST		= 'gst';    // non-Zimbra email address and password - might not yet supported
    const GRANTEE_KEY		= 'key';    // non-Zimbra email address/external user and access key
    const GRANTEE_PUB		= 'pub';    // public authenticated and unauthenticated access
    const GRANTEE_EMAIL		= 'email';  // email adress
    
        
    /**
     * id Zimbra ou nom de la cible
     *
     * @var string
     */
    private $target;
    
    /**
     * id Zimbra de la cible
     * utilisé lors de la récupération de la réponse du web services
     * 
     * @var string
     */
    private $targetId;
    
    /**
     * nom de la cible
     * utilisé lors de la récupération de la réponse du web services
     * 
     * @var string
     */
    private $targetName;
    
    /**
     * type de la cible
     * 
     * @var string
     * @see self::TARGET_*
     */
    private $targetType;
    
    /**
     * sélecteur de la cible
     *
     * @var string
     */
    private $targetBy = 'id';

    
    /**
     * id Zimbra ou nom du bénéficiaire
     * requis pour : usr grp egp dom gst key email
     * 
     * @var string
     */
    private $grantee;
    
    /**
     * id Zimbra du bénéficiaire
     * utilisé lors de la récupération de la réponse du web services
     * 
     * @var string
     */
    private $granteeId;
    
    /**
     * nom du bénéficiaire
     * utilisé lors de la récupération de la réponse du web services
     * 
     * @var string
     */
    private $granteeName;
    
    /**
     * type du bénéficiaire
     * 
     * @var string
     * @see self::GRANTEE_*
     */
    private $granteeType;
    
    /**
     * sélecteur du bénéficiaire
     *
     * @var string
     */
    private $granteeBy = 'id';      

    /**
     * mot de passe ou clé d'accès (requis pour : gst key)
     * 
     * @var string
     */
    private $granteeSecret = null;
    
    /**
     * droit
     * Value is of the form : {right-name} | {inline-right} where 
     * {right-name} = a system defined right name 
     * {inline-right} = {op}.{target-type}.{attr-name} 
     * {op} = set | get 
     * {attr-name} = a valid attribute name on the specified target type
     * 
     * @var string
     */
    private $right;
    
    /**
     * deny - negative right
     * 
     * @var boolean
     */
    private $rightDeny = false;
    
    /**
     * can delegate
     * 
     * @var boolean
     */
    private $rightCanDelegate = false;
    
    /**
     * 
     * @var boolean
     */
    private $rightDisinheritSubGroups = false;

    /**
     *
     * @var boolean
     */
    private $rightSubDomain = false;
    
    /**
     * méthodes de la classe
     * 
     * @var array
     */
    private $publicFields = array(
	'target',
	'targetId',
	'targetName',
	'targetType',
	'targetBy',
	'grantee',
	'granteeId',
	'granteeName',
	'granteeType', 
	'granteeBy',
	'granteeSecret',
	'right',
	'rightDeny',
	'rightCanDelegate',
	'rightDisinheritSubGroups',
	'rightSubDomain'
    );
    
    private $targetAuthorized = array(
	'UnicaenZimbra\Entity\Account'		=> self::TARGET_ACCOUNT,
	'UnicaenZimbra\Entity\Resource'		=> self::TARGET_CALRESOURCE,
	'UnicaenZimbra\Entity\Cos'		=> self::TARGET_COS,
	'UnicaenZimbra\Entity\DistributionList' => self::TARGET_DL,
	'UnicaenZimbra\Entity\Domain'		=> self::TARGET_DOMAIN,
	'UnicaenZimbra\Entity\Server'		=> self::TARGET_SERVER
    );
    
    private $granteeAuthorized = array(
	'UnicaenZimbra\Entity\Account'		=> self::GRANTEE_USR,
	'UnicaenZimbra\Entity\DistributionList'	=> self::GRANTEE_GRP,
	'UnicaenZimbra\Entity\Domain'		=> self::GRANTEE_DOM
    );
    
    
    /**
     * Getter (magic)
     * 
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
       if (!in_array($name, $this->publicFields))
             throw new Exception(sprintf("La propriété '%s' est inconnue ou n'est pas accessible", $name));
        
        return $this->$name;
    }

    /**
     * Getter 
     * 
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->__get($name);
    }

    /**
     * Setter (magic)
     * 
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
	if (! in_array($name, $this->publicFields))
            throw new Exception(sprintf("La propriété '%s' est inconnue ou n'est pas modifiable", $name));

        $this->$name = $value;
    }

    /**
     * Setter
     * 
     * @param string $name
     * @param mixed $value
     * @return \UnicaenZimbra\Entity\Right
     */
    public function set($name, $value)
    {
        $this->__set($name, $value);
	
	return $this;
    }
    
    /**
     * Retourne la liste des attributs avec leur valeur formatée pour Zimbra
     * 
     * @return array
     */
    public function getConvertedToXml()
    {
	foreach(array('target', 'grantee', 'right') as $tag) {
	    if(null != $this->$tag) {
		$params[$tag] = $this->_makeZimbraAttributes($tag);
	    }
	}
	
	return $params;
    }
    
    /**
     * Formate la liste des attributs avec leur valeur pour Zimbra
     * 
     * @param string $tag
     * @return array
     */
    private function _makeZimbraAttributes($tag)
    {
        $attributes = array();
	
	foreach($this->publicFields as $attr) {
	    if(preg_match(sprintf('/(%s)(.+)/', $tag), $attr, $matches)) {
		if(!is_null($this->$attr)) {
		    $attributes[strtolower($matches[2])] = Entity::convertToXml($this->$attr);
		}
	    }
	}
	
	return $result[] = array(
	    '@attributes' => $attributes,
	    '@content' => $this->$tag,
	);
    }
    
    public function populate(SimpleXMLElement $object)
    {
	if($object->target->count() > 0) {
	    $attributes = $object->target->attributes();
	    if(isset($attributes->type))    $this->targetType = $attributes->type;
	    if(isset($attributes->id))	    $this->targetId   = $attributes->id;
	    if(isset($attributes->name))    $this->targetName = $attributes->name;	
	}
	if($object->grantee->count() > 0) {
	    $attributes = $object->grantee->attributes();
	    if(isset($attributes->type))    $this->granteeType = $attributes->type;
	    if(isset($attributes->id))	    $this->granteeId   = $attributes->id;
	    if(isset($attributes->name))    $this->granteeName = $attributes->name;	
	}
	if($object->right->count() > 0) {
	    $this->right = $object->right;
	    $attributes = $object->right->attributes();
	    if(isset($attributes->deny))		$this->rightDeny = $attributes->deny;
	    if(isset($attributes->canDelegate))		$this->rightCanDelegate = $attributes->canDelegate;
	    if(isset($attributes->disinheritSubGroups)) $this->rightDisinheritSubGroups = $attributes->disinheritSubGroups;
	    if(isset($attributes->subDomain))		$this->rightSubDomain = $attributes->subDomain;
	}
    }
    
    /**
     * L'entité transmise devient la cible du droit
     *
     * @param \UnicaenZimbra\Entity\Entity $target
     * @return \UnicaenZimbra\Entity\Right
     * @throws Exception
     */
    public function setTarget(ZimbraEntity $target)
    {
	$this->targetType = $this->targetAuthorized[get_class($target)];

	if(null == $this->targetType)
	    throw new Exception(sprintf("Types d'entité autorisés pour la cible : %s", implode('|', array_values($this->targetAuthorized))));
	
        $this->target = $target->getId();
	
	return $this;
    }
    
    /**
     * L'entité transmise devient le bénéficiaire du droit
     * 
     * @param \UnicaenZimbra\Entity\Entity $grantee
     * @return \UnicaenZimbra\Entity\Right
     * @throws Exception
     */
    public function setGrantee(ZimbraEntity $grantee)
    {
	$this->granteeType = $this->granteeAuthorized[get_class($grantee)];

	if(null == $this->granteeType)
	    throw new Exception(sprintf("Types d'entité autorisés pour la bénéficiaire : %s", implode('|', array_values($this->granteeAuthorized))));
	
        $this->grantee = $grantee->getId();
	
	return $this;
    }
    
    /**
     * A system define right name
     * Ex. : invite, viewFreeBusy
     * 
     * @param string $rightName
     * @return \UnicaenZimbra\Entity\Right
     */
    public function setRightByRightName($rightName)
    {
	$this->right = $rightName;
	
	return $this;
    }
    
    /**
     * Inline right
     * {inline-right} = {op}.{target-type}.{attr-name} 
     * {op} = set | get 
     * {attr-name} = a valid attribute name on the specified target type
     * 
     * @param string $op
     * @param string $attrName
     * @return \UnicaenZimbra\Entity\Right
     * @throws Exception
     */
    public function setRightByInlineRight($op, $attrName)
    {
	if(null == $this->targetType)
	    throw new Exception("Vous devez spécifier le type de la cible 'targetType' pour utiliser cette méthode");
	
	if(!in_array($op, array('get', 'set')))
		throw new Exception("Opération incorrecte, valeurs autorisées : get|set");
	
	$this->right = $op . $this->targetType . $attrName;
	
	return $this;
    }
}

