<?php

/**
 * Resource Zimbra
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */

namespace UnicaenZimbra\Entity;

class Resource extends Entity
{
    /**
     * Used for zimbraCalResType attribute
     */
    const TYPE_LOCATION = 'Location';

    /**
     * Used for zimbraCalResType attribute
     */
    const TYPE_EQUIPMENT = 'Equipment';
    
}