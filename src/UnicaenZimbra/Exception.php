<?php

namespace UnicaenZimbra;

/**
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class Exception extends \Exception
{
}