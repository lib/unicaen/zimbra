<?php

namespace UnicaenZimbra\Stdlib;

use Laminas\Stdlib\ArrayUtils as ZendArrayUtils;
//use UnicaenZimbra\Exception;
use Exception; // Oboigatoire à cause d'un problème dans les tests unitaires

/**
 *
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
abstract class ArrayUtils extends ZendArrayUtils
{

    /**
     * Ajoute un élément dans un tableau à l'index souhaité si précisé, sinon à la fin
     *
     * @param array $array
     * @param mixed $value
     * @param integer $index
     * @return integer
     * @throws Exception
     */
    public static function add( &$array, $value, $index=-1 )
    {
        $count = count($array);

        if ($index > $count){
            throw new Exception('Impossible d\'ajouter un élément au tableau : l\'index "'.$index.'" est trop grand');
        }elseif ($index < 0){
            $index = $count;
        }elseif( $index < $count ){
            for( $i=$count-1;$i>=$index;$i--){
                $array[$i+1] = $array[$i];
            }
        }
        $array[$index] = $value;
        return $index;
    }

    /**
     * Retire un élément d'un tableau tout en assurant la continuité des index
     *
     * @param array $array
     * @param integer $index
     * @throws Exception
     */
    public static function remove( &$array, $index )
    {
        if (! isset($array[$index])){
            throw new Exception('L\'index "'.$index.'" n\'existe pas.');
        }
        unset($array[$index]);
        $array = array_values($array);
    }

}