<?php

namespace UnicaenZimbra;

use Interop\Container\ContainerInterface;
use UnicaenZimbra\Options\ModuleOptions;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ZimbraFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Zimbra|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var ModuleOptions $moduleOptions
         */
        $moduleOptions = $container->get('zimbraOptions');

        $service = new Zimbra();
        $service->setOptions($moduleOptions);

        return $service;
    }
}