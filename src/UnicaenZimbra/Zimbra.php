<?php

namespace UnicaenZimbra;

use SimpleXMLElement;
use UnicaenZimbra\Options\ModuleOptions;

class Zimbra
{
    /**
     * Zimbra ID pour le puvlic (utile pour gérer les droits de partage)
     */
    const ID_PUBLIC = '99999999-9999-9999-9999-999999999999';

    /**
     * Zimbra ID pour tout le monde (utile pour gérer les droits de partage)
     */
    const ID_ALL = '00000000-0000-0000-0000-000000000000';

    /**
     * Limite de recherche par défaut
     */
    const DEFAULT_LIMIT = 10;

    /**
     * Offset par défaut
     */
    const DEFAULT_OFFSET = 0;

    /**
     * The entire XML message
     *
     * @var SimpleXMLElement
     */
    private $message;

    /**
     * Pointing to the context element
     *
     * @var SimpleXMLElement
     */
    private $context;

    /**
     * Curl Handle
     *
     * @var resource
     */
    private $curlHandle;

    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * Détermine si la connexion a été faite ou non
     *
     * @var boolean
     */
    protected $connected = false;

    /**
     * @var array
     */
    private $systemUsers = array(
        "admin",
        "postmaster",
        "ham",
        "spam",
        "virus-quarantine",
        "galsync"
    );

    /**
     *
     * @var string
     */
    private $authToken;


    /**
     * @param ModuleOptions $moduleOptions
     */
    public function setOptions(ModuleOptions $moduleOptions)
    {
        $this->options = $moduleOptions;
    }

    /**
     * @return ModuleOptions
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Retourne la liste des utilisateurs système de Zimbra
     *
     * @return array|string[]
     */
    public function getSystemUsers()
    {
        return $this->systemUsers;
    }

    /**
     * Connexion au serveur Zimbra
     *
     * @return self
     * @throws Exception
     */
    public function connect()
    {
        $options = $this->getOptions();

        $this->curlHandle = curl_init();
        curl_setopt($this->curlHandle, CURLOPT_URL, "https://$options->server:$options->port/service/admin/soap");
        curl_setopt($this->curlHandle, CURLOPT_POST, TRUE);
        curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->curlHandle, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($this->curlHandle, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($this->curlHandle, CURLOPT_CONNECTTIMEOUT, 30);

        $this->message = new SimpleXMLElement('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"></soap:Envelope>');
        $this->context = $this->message->addChild('Header')->addChild('context', null, 'urn:zimbra');
        $this->message->addChild('Body');

        $xml = $this->request('AuthRequest', array(), array('name' => $options->email, 'password' => $options->password));

        $this->authToken = $xml->children()->AuthResponse->authToken;
        $this->addContextChild('authToken', $this->authToken);

        $this->connected = true;
        return $this;
    }

    /**
     * Recherche dans l'annuaire LDAP de Zimbra
     *
     * @param string $domain
     * @param integer $limit
     * @param integer $offset
     * @param string $types
     * @param string $sort
     * @param string $query
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function searchDirectory($domain, $limit = self::DEFAULT_LIMIT, $offset = self::DEFAULT_OFFSET, $types = 'accounts', $sort = null, $query = null)
    {
        if ($types == 'accounts') {
            $ldapQuery = "(&amp;";
            if ($query !== null) {
                $ldapQuery .= "($query)";
            }
            $ldapQuery .= '(!(zimbraIsSystemAccount=TRUE))';
            $ldapQuery .= ')';
        } else {
            $ldapQuery = $query;
        }

        $attributes = array(
            'limit' => $limit,
            'offset' => $offset,
            'types' => $types,
        );
        if ($types != 'coses') { // pas de domaine pris en compte quand on recherche des COS
            $attributes['domain'] = $domain;
        }

        if (!is_null($sort)) {
            $attributes['sortBy'] = $sort[0];
            $attributes['sortAscending'] = $sort[1];
        }

        return $this->request('SearchDirectoryRequest', $attributes, array('query' => $ldapQuery));
    }

    /**
     * Vérifie l'existance d'un nom (by=name) ou d'un ID (by=id), en fonction de son type
     *
     * Types = {accounts|distributionlists|aliases|resources|domains|coses}
     *
     * @param string $value
     * @param string $by
     * @param string $types
     * @return bool
     * @throws Exception
     */
    public function exists($value, $by = 'name', $types = 'accounts')
    {
        $domain = null;
        $queryField = null;
        switch ($by) {
            case 'id':
                $queryField = 'zimbraId';
                break;
            case 'name':
                $queryField = 'mail';
                break;
            default:
                throw new Exception('La valeur de $by n\'est pas correcte.');
        }

        if ($types == 'accounts') {
            $ldapQuery = "(&amp;";
            $ldapQuery .= "($queryField=$value)";
            $ldapQuery .= '(!(zimbraIsSystemAccount=TRUE))';
            $ldapQuery .= ')';
        } elseif ($types == 'aliases' && $by == 'name') {
            list($uid, $domain) = explode('@', $value);

            $ldapQuery = "uid=$uid";
        } elseif ($types == 'domains' && $by == 'name') {
            $ldapQuery = "zimbraDomainName=$value";
        } elseif ($types == 'coses' && $by == 'name') {
            $ldapQuery = "cn=$value";
        } else {
            $ldapQuery = "$queryField=$value";
        }

        $attributes = array(
            'types' => $types,
            'countOnly' => 1
        );
        if (!empty($domain) && $types != 'coses') { // pas de domaine pris en compte quand on recherche des COS
            $attributes['domain'] = $domain;
        }

        $params = array('query' => $ldapQuery);

        $response = $this->request('SearchDirectoryRequest', $attributes, $params);

        return 1 == (int)$response->children()->SearchDirectoryResponse->attributes()->num;
    }

    /**
     * Change le mot de passe d'une entité
     *
     * $id = Zimbra ID
     * $password = nouveau mot de passe (en clair)
     *
     * @param string $id
     * @param string $password
     * @return self
     * @throws Exception
     */
    public function setPassword($id, $password)
    {
        $attributes = array(
            'id' => $id,
            'newPassword' => $password,
        );
        $this->request('SetPasswordRequest', $attributes);
        return $this;
    }

    /**
     * Retourne les quotas d'utilisation du domaine
     *
     * @param string $domain
     * @return int[]
     * @throws Exception
     */
    public function getTotalQuota($domain)
    {
        $result = array(
            'diskUsage' => 0,
            'diskProvisioned' => 0,
            'diskLimit' => 0,
            'mailTotal' => 0,
            'mailLimit' => 0,
        );

        $allServers = $this->request('GetAllServersRequest', array());

        foreach ($allServers->children()->GetAllServersResponse->children() as $server) {
            $this->addContextChild('targetServer', (string)$server['id']);
            $response = $this->request('GetQuotaUsageRequest', array('domain' => $domain));
            $result['mailTotal'] += (string)$response->children()->GetQuotaUsageResponse['searchTotal'];

            foreach ($response->children()->GetQuotaUsageResponse->children() as $quota) {
                $b = explode('@', $quota['name']);
                $c = explode('.', $b[0]);
                $account = $c[0];

                // Remove the system users (antispam, etc.) from the count
                if (!in_array($account, $this->systemUsers)) {
                    $result['diskUsage'] += $quota['used'];
                    $result['diskProvisioned'] += $quota['limit'];
                } elseif ($b[0] = 'postmaster') {
                    $result['diskLimit'] = (int)$quota['limit'];
                    $result['mailTotal'] -= 1;
                } else {
                    $result['mailTotal'] -= 1;
                }
            }
        }

        $params = array(
            'domain' => array(
                '@content' => $domain,
                '@attributes' => array('by' => 'name'),
            )
        );
        $response = $this->request('GetDomainRequest', array('attrs' => 'zimbraDomainMaxAccounts'), $params);
        $domains = $response->children()->GetDomainResponse->children();

        $result['mailLimit'] = (int)$domains[0]->zimbraDomainMaxAccounts;

        return $result;
    }

    /**
     * Envoie une requête qui ne fait rien
     *
     * @throws Exception
     */
    public function noOp()
    {
        $this->request('NoOpRequest');
    }

    /**
     * @param string $name
     * @param string $value
     * @param array $attributes
     */
    public function addContextChild($name, $value, $attributes = [])
    {
        if (isset($this->context->$name)) {
            $this->context->$name = $value;
            if (array() != $attributes) {
                foreach ($attributes as $key => $value) {
                    if ((string)$this->context->$name->attributes()->$key != $value)
                        $this->context->$name->addAttribute($key, $value);
                }
            }
        } else {
            $child = $this->context->addChild($name, $value);
            if (array() != $attributes) {
                foreach ($attributes as $key => $value) {
                    $child->addAttribute($key, $value);
                }
            }
        }
    }

    /**
     * Effectue une requête sur le serveur SOAP
     *
     * @param string $name
     * @param array $attributes
     * @param array $params
     * @param string $urn
     * @return SimpleXMLElement
     * @throws Exception
     */
    public function request($name, array $attributes = array(), array $params = array(), $urn = 'urn:zimbraAdmin')
    {
        if (!$this->connected && 'AuthRequest' != $name) $this->connect();

        unset($this->message->children('soap', true)->Body);
        $body = $this->message->addChild('Body');
        $this->arrayToNode($body, $name, $attributes, $params, $urn);
        if ($this->getOptions()->getDebugMode()) $this->debugXml($this->message->asXml());
        curl_setopt($this->curlHandle, CURLOPT_POSTFIELDS, $this->message->asXml());
        $soapMessage = curl_exec($this->curlHandle);

        if ($this->getOptions()->getDebugMode()) $this->debugXml($soapMessage);
        if (!$soapMessage) {
            throw new Exception(curl_error($this->curlHandle), curl_errno($this->curlHandle));
        }

        $xml = new SimpleXMLElement($soapMessage);

        $fault = $xml->children('soap', true)->Body->Fault;
        if ($fault) {
            throw new Exception($fault->Reason->Text);
        }

        return $xml->children('soap', true)->Body;
    }

    /**
     * Transforme un tableau en ensemble de noeuds XML
     *
     * @param SimpleXMLElement $node
     * @param string $name
     * @param array $attributes
     * @param array $data
     * @param string $namespace
     * @return self
     */
    private function arrayToNode(SimpleXMLElement $node, $name, array $attributes = array(), $data = null, $namespace = null)
    {
        $nodeContent = null;
        $hasChildren = false;
        if (!is_array($data)) {
            $nodeContent = $data;
        } elseif (isset($data['@content'])) {
            $nodeContent = $data['@content'];
            unset($data['@content']);
            $hasChildren = true;
        } else {
            $hasChildren = true;
        }

        if (isset($data['@attributes'])) {
            $attributes = array_merge($attributes, $data['@attributes']);
            unset($data['@attributes']);
        }

        $child = $node->addChild($name, $nodeContent, $namespace);
        foreach ($attributes as $attrKey => $attrValue) {
            $child->addAttribute($attrKey, $attrValue);
        }

        if ($hasChildren) {
            foreach ($data as $childName => $childData) {
                if (0 === strpos($childName, '*')) {
                    $childName = substr($childName, 1);
                    foreach ($childData as $childDataItem) {
                        $this->arrayToNode($child, $childName, array(), $childDataItem, $namespace);
                    }
                } else {
                    $this->arrayToNode($child, $childName, array(), $childData, $namespace);
                }
            }
        }
        return $this;
    }

    /**
     * Retourne un tableau de paramètres pour requête à partir d'un tableau d'attributs d'object Zimbra
     *
     * @param array $data
     * @param string $nameField
     * @return array[]
     */
    public function makeZimbraAttributes(array $data, $nameField = 'n')
    {
        $result = array();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $val) {
                    $result[] = array(
                        '@attributes' => array($nameField => $key),
                        '@content' => $val,
                    );
                }
            } else {
                $result[] = array(
                    '@attributes' => array($nameField => $key),
                    '@content' => $value,
                );
            }
        }
        return array('*a' => $result);
    }

    /**
     * Debug
     *
     * @param string $xml
     */
    public function debugXml($xml)
    {
        echo '<div style="border:1px red solid;margin:1px;padding:1px">';
        echo htmlentities($xml);
        echo '</div>';
    }
}