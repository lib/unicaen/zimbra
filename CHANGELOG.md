CHANGELOG
=========

4.0.3 (09/06/2023)
------------------

- [Fix] L'appel de la fonction de création d'un compte Zimbra dans le processus de création d'une boîte générique ne correspondait pas à la signature de cette fonction

4.0.2 (15/03/2023)
------------------

- merge tag 3.0.5 et 3.0.6
- ajout fichier .gitlab-ci.yml

4.0.1 (19/04/2022)
------------------

- merge tag 3.0.4

4.0.0 (15/12/2021)
------------------

- Migration Laminas

3.0.5 (02/02/2023)
------------------

- [Fix] Erreur sur le namespace du trait ZimbraBoiteGeneriqueProcessusAwareTrait
- [Fix] Erreur sur l'objet de retour de la fonction d'invocation de la factory ZimbraBoiteGeneriqueProcessusFactory

3.0.4 (19/04/2022)
------------------

- Utilisation de la commande "changePrimaryEmail" pour modifier l'adresse mail principale d'un compte Zimbra

3.0.3 (08/03/2021)
------------------

- Possibilité de récupérer une boîte générique qui n'est pas au format bg-* lors de l'ajout de droits à un utilisateur sur une adresse fonctionnelle

3.0.2 (15/10/2021)
------------------

- [Fix] Les paramètres passés dans la fonction create() n'étaient pas pris en compte lors de la création d'un compte Zimbra

3.0.1 (03/09/2020)
------------------

- Modification des paramètres de la fonction de création d'un compte Zimbra

3.0.0 (03/09/2020)
------------------

- Adaptation du code pour le passage à ZF3.
- Ajout de fonctions au Service "ZimbraAccount" : ajout/suppression alias, ajout/suppression forward et modifications de quelques options du compte Zimbra.