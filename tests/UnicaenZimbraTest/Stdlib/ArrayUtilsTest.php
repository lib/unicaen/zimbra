<?php

namespace UnicaenZimbraTest\Stdlib;

use UnicaenZimbra\Stdlib\ArrayUtils;
use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Exception;

/**
 * @group Stdlib
 */
class ArrayUtilsTest extends PHPUnit_Framework_TestCase
{

    public function testAdd()
    {
        $array = array(
            'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq'
        );

        $expected = array(
            'zéro', 'un', 'deux', 'trois', 'trois-et-demi', 'quatre', 'cinq'
        );
        $result = $array;
        $index = ArrayUtils::add($result, 'trois-et-demi', 4);
        $this->assertEquals($expected, $result);
        $this->assertEquals(4, $index);

        $expected = array(
            'prems', 'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq'
        );
        $result = $array;
        $index = ArrayUtils::add($result, 'prems', 0);
        $this->assertEquals($expected, $result);
        $this->assertEquals(0, $index);

        $expected = array(
            'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six'
        );
        $result = $array;
        $index = ArrayUtils::add($result, 'six');
        $this->assertEquals($expected, $result);
        $this->assertEquals(6, $index);

        $expected = array(
            'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six'
        );
        $result = $array;
        $index = ArrayUtils::add($result, 'six', -2);
        $this->assertEquals($expected, $result);
        $this->assertEquals(6, $index);
    }

    /**
     *
     * @expectedException Exception
     */
    public function testAddException($param)
    {
        $array = array(
            'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq'
        );

        $result = $array;
        $index = ArrayUtils::add($result, 'sept', 7);
    }

    public function testRemove()
    {
        $array = array(
            'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq'
        );

        $expected = array(
            'zéro', 'un', 'deux', 'quatre', 'cinq'
        );
        $result = $array;
        ArrayUtils::remove($result, 3);
        $this->assertEquals($expected, $result);        

        $expected = array(
            'un', 'deux', 'trois', 'quatre', 'cinq'
        );
        $result = $array;
        ArrayUtils::remove($result, 0);
        $this->assertEquals($expected, $result);

        $expected = array(
            'zéro', 'un', 'deux', 'trois', 'quatre'
        );
        $result = $array;
        ArrayUtils::remove($result, 5);
        $this->assertEquals($expected, $result);
    }

    /**
     * @expectedException Exception
     */
    public function testRemoveException()
    {
        $array = array(
            'zéro', 'un', 'deux', 'trois', 'quatre', 'cinq'
        );

        $result = $array;
        $index = ArrayUtils::remove($result, 6);
    }
}