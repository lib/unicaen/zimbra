<?php

namespace UnicaenZimbraTest;

use PHPUnit_Framework_TestCase;
use Laminas\ServiceManager\ServiceManager;

/**
 * @group Zimbra
 */
class ZimbraTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    protected $data = array(
        'id' => '01d13c21-2d2c-45f9-9df0-9bee295b3542' // laurent.lecluse@pp.unicaen.fr
    );

    protected $services = array(
            'account',
            'alias',
            'cos',
            'distributionList',
            'domain',
            'filter',
            'folder',
            'gal',
            'identity',
            'mailBox',
            'resource',
            'server',
    );

    public function servicesProvider()
    {
        $result = array();
        foreach( $this->services as $service ){
            $result[] = array($service);
        }
        return $result;
    }

    protected function setUp()
    {
        $this->serviceManager = Bootstrap::getServiceManager();
        $this->serviceManager->get('zimbra')->connect();
    }

    public function testIsConnected()
    {
        $res = $this->serviceManager->get('zimbra')->request('NoOpRequest');
        $this->assertEquals('NoOpResponse', $res->children()->NoOpResponse->getName());
    }

    /**
     * @dataProvider servicesProvider
     * @param string $name
     */
    public function testGetServices($name)
    {
        $propertyName = $name.'Service';
        $methodName = 'get'.ucfirst($name).'Service';
        $className = 'UnicaenZimbra\\Service\\'.ucfirst($name);

        $result = $this->serviceManager->get('zimbraService'.ucfirst($name));
        $this->assertEquals($className, get_class($result));
    }

    public function existsProvider()
    {
         return array(
             array( 'laurent.lecluse@pp.unicaen.fr', 'name', 'accounts', true ),
             array( 'laurent.lec999@pp.unicaen.fr', 'name', 'accounts', false ),
             array( '01d13c21-2d2c-45f9-9df0-9bee295b3542', 'id', 'accounts', true ),
             array( '01d13c21-2d2c-45f9-9df0-999999999999', 'id', 'accounts', false ),

             array( 'zimbra.admin@pp.unicaen.fr', 'name', 'distributionlists', true ),
             array( 'zimbra.admin999@pp.unicaen.fr', 'name', 'distributionlists', false ),
             array( '01f818b4-2c5a-475c-9037-9045b041b391', 'id', 'distributionlists', true ),
             array( '01f818b4-2c5a-475c-9037-999999999999', 'id', 'distributionlists', false ),

             array( 'surville-david@pp.unicaen.fr', 'name', 'aliases', true ),
             array( 'surville-david999@pp.unicaen.fr', 'name', 'aliases', false ),
             array( '5cab946e-5b0d-484d-b8ed-00877f952dec', 'id', 'aliases', true ),
             array( '5cab946e-5b0d-484d-b8ed-999999999999', 'id', 'aliases', false ),

             array( 'sallerunion@pp.unicaen.fr', 'name', 'resources', true ),
             array( 'sallerunion999@pp.unicaen.fr', 'name', 'resources', false ),
             array( 'cb6c5ef0-b6ed-46e4-a025-0d53472f4cc4', 'id', 'resources', true ),
             array( 'cb6c5ef0-b6ed-46e4-a025-999999999999', 'id', 'resources', false ),

             array( 'pp.unicaen.fr', 'name', 'domains', true ),
             array( 'pp999.unicaen.fr', 'name', 'domains', false ),
             array( 'b2113b08-1dcd-4143-ac67-c9c9045e3245', 'id', 'domains', true ),
             array( 'b2113b08-1dcd-4143-ac67-999999999999', 'id', 'domains', false ),

             array( 'default', 'name', 'coses', true ),
             array( 'default999', 'name', 'coses', false ),
             array( 'e00428a1-0c00-11d9-836a-000d93afea2a', 'id', 'coses', true ),
             array( 'e00428a1-0c00-11d9-836a-999999999999', 'id', 'coses', false ),
         );
     }

     public function searchDirectoryProvider()
     {
         return array(
             array( 'accounts', 'mail=laurent.lecluse@pp.unicaen.fr', 1, array(
                    '01d13c21-2d2c-45f9-9df0-9bee295b3542' => array(
                        'type' => 'account',
                        'name' => 'laurent.lecluse@pp.unicaen.fr'
                     )
                 )
             ),
             array( 'accounts', 'mail=laurent.lecluse@unicaen.fr', 0, array()
             ),
             array( 'coses', 'cn=default', 1, array(
                    'e00428a1-0c00-11d9-836a-000d93afea2a' => array(
                        'type' => 'cos',
                        'name' => 'default'
                     )
                 )
             ),
             array( 'domains', 'zimbraDomainName=pp.unicaen.fr', 1, array(
                    'b2113b08-1dcd-4143-ac67-c9c9045e3245' => array(
                        'type' => 'domain',
                        'name' => 'pp.unicaen.fr'
                     )
                 )
             ),
             array( 'aliases', 'uid=surville-david', 1, array(
                    '5cab946e-5b0d-484d-b8ed-00877f952dec' => array(
                        'type' => 'alias',
                        'name' => 'surville-david@pp.unicaen.fr'
                     )
                 )
             ),
             array( 'resources', 'mail=sallerunion@pp.unicaen.fr', 1, array(
                    'cb6c5ef0-b6ed-46e4-a025-0d53472f4cc4' => array(
                        'type' => 'calresource',
                        'name' => 'sallerunion@pp.unicaen.fr'
                     )
                 )
             ),
         );
     }


     /**
      * @dataProvider existsProvider
      */
     public function testExists( $value, $by, $types, $result )
     {
         $this->assertEquals($result, $this->serviceManager->get('zimbra')->exists($value, $by, $types));
     }

     /**
      * @dataProvider searchDirectoryProvider
      * @param type $types
      * @param type $query
      * @param type $count
      */
     public function testSearchDirectory( $types, $query, $targetCount, array $targetResult )
     {
         $res = $this->serviceManager->get('zimbra')->searchDirectory('pp.unicaen.fr', 10, 0, $types, null, $query);
         $count = (int)$res->children()->SearchDirectoryResponse['searchTotal'];
         $result = array();
         foreach ($res->children()->SearchDirectoryResponse->children() as $responseXml) {
             $result[(string)$responseXml['id']] = array(
                 'type' => $responseXml->getName(),
                 'name' => (string)$responseXml['name'],
             );
         }
         $this->assertEquals($targetCount, $count);
         $this->assertEquals($targetResult, $result);
     }

     public function testGetTotalQuota()
     {
         $expected = array(
            'diskUsage',
            'diskProvisioned',
            'diskLimit',
            'mailTotal',
            'mailLimit',
         );
         $result = $this->serviceManager->get('zimbra')->getTotalQuota('pp.unicaen.fr');
         $this->assertEquals( $expected, array_keys($result));
     }
}