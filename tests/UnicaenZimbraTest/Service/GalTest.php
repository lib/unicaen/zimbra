<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Service\ZimbraGalService;

/**
 * @group Gal
 */
class GalTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraGalService
     */
    protected $service;

    protected $data = array(
        /* Attention : seuls les 10 premiers résultats sont retournés!! Veillez à ne pas prendre un critère de recherche amenant trop de résultats! */
        'contactLdapId' => 'uid=laurent.lecluse,ou=people,dc=pp,dc=unicaen,dc=fr',
        'contactId'     => '01d13c21-2d2c-45f9-9df0-9bee295b3542',
        'contactMail'   => 'laurent.lecluse@pp.unicaen.fr',
        'contactSearch' => 'Lecluse',
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraGalService::class);
    }

    public function testAutocomplete()
    {
        $domain = 'pp.unicaen.fr';
        $name = $this->data['contactSearch'];
        $type = 'account';

        $res = $this->service->autoComplete($domain, $name, $type);
        $this->assertArrayHasKey($this->data['contactLdapId'], $res);

        $galEntry = $res[$this->data['contactLdapId']];

        $this->assertEquals($this->data['contactId'], $galEntry->zimbraId);
    }

}