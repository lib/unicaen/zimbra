<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Account;
use UnicaenZimbra\Service\ZimbraAccountService;

/**
 * @group Account
 */
class AccountTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraAccountService
     */
    protected $service;

    protected $data = array(
        'name' => 'test.unitaire@pp.unicaen.fr',
        'givenName' => 'Test',
        'sn' => 'Unitaire',
        'zimbraHideInGal' => true,
        'zimbraNotes' => 'Essai de comentaire'
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraAccountService::class);
    }

    public function testCreate()
    {
        $account = new Account;
        $account->setName($this->data['name']);
        $properties = $this->data;
        unset($properties['name']);
        $account->exchangeArray($properties);

        $res = $this->service->create($account, 'TRt56wxc6w');

        foreach( $properties as $key => $value ){
            $this->assertEquals($value, $res->$key);
        }

        return $res->getId();
    }

    /**
     * @depends testCreate
     * @param type $id
     */
    public function testExists( $id )
    {
        $this->assertTrue( $this->service->exists($id, 'id') );
    }

    /**
     * @depends testCreate
     */
    public function testGetList( $id )
    {
        $res = $this->service->getList('pp.unicaen.fr', 100);

        $this->assertArrayHasKey($id, $res);
        $this->assertEquals( $this->service->getLastCount(), count($res) );
        $this->assertInstanceOf(Account::class, $res[$id]);
        $this->assertEquals($id, $res[$id]->getId());
    }

    /**
     * @depends testCreate
     * @return Account
     */
    public function testGet( $id )
    {
        $res = $this->service->get($id, 'id');

        $resByName = $this->service->get( $res->getName() );

        $this->assertInstanceOf(Account::class, $res);
        $this->assertEquals($id, $resByName->getId());
        $this->assertEquals($res, $resByName);

        foreach( $this->data as $key => $value ){
            if ('name' != $key && 'id' != $key ){
                $this->assertEquals($value, $res->$key);
            }
        }
        return $res;
    }

    /**
     * 
     * @depends testGet
     */
    public function testGetEmptyAlias( Account $account )
    {
        $alias = $account->getAlias();
        $alias2 = $this->service->getAlias($account);

        $this->assertEquals( $alias, $alias2 );
        $this->assertEquals(array(), $alias);
    }

    /**
     * @depends testGet
     */
    public function testAddAlias( Account $account )
    {
        $this->assertEquals( $this->service, $this->service->addAlias($account, 'alias.testunitaire@pp.unicaen.fr') );
        $alias = $this->service->getAlias($account->getId());
        $aalias = $account->getAlias();
        $this->assertEquals($alias, $aalias);
        $this->assertCount(1, $alias);
        return $account;
    }

    /**
     * @depends testAddAlias
     */
    public function testGetOneAlias( Account $account )
    {
        $alias = $account->getAlias();
        $this->assertEquals(array('alias.testunitaire@pp.unicaen.fr'), $alias);
    }

    /**
     *
     * @depends testAddAlias
     * @param \UnicaenZimbra\Entity\Account $account
     */
    public function testRemoveAlias(Account $account )
    {
        $this->service->removeAlias($account, 'alias.testunitaire@pp.unicaen.fr');
        $alias = $this->service->getAlias($account->getId());
        $aalias = $account->getAlias();
        $this->assertEquals($alias, $aalias);
        $this->assertCount(0, $alias);
    }

    /**
     * @depends testGet
     */
    public function testModify( Account $account )
    {
        $account->givenName = 'Test 3';
        $this->service->modify($account);
        $res = $this->service->get($this->data['name']);
        $this->assertEquals('Test 3', $res->givenName);
    }

    /**
     * 
     * @depends testCreate
     */
    public function testSearch( $id ){
        $res = $this->service->search('zimbraId='.$id);
        $this->assertArrayHasKey($id, $res);

        $account = $res[$id];
        $this->assertInstanceOf(Account::class, $account);
    }

    /**
     * @depends testCreate
     */
    public function testGetQuota( $id )
    {
        $res = $this->service->getQuotas( array() );

        $this->assertArrayHasKey($this->data['name'], $res);

        $accountQuota = $res[$this->data['name']];

        $this->assertArrayHasKey('used', $accountQuota);
        $this->assertInternalType('integer', $accountQuota['used']);
        $this->assertArrayHasKey('limit', $accountQuota);
        $this->assertInternalType('integer', $accountQuota['limit']);
    }

    /**
     * 
     * @depends testCreate
     */
    public function testRename( $id )
    {
        $newName = 'test.unitaire2@pp.unicaen.fr';

        $account = $this->service->rename($id, $newName);
        $this->assertEquals($newName, $account->getName());
        return $newName;
    }

    /**
     * @depends testRename
     */
    public function testDelete( $name )
    {
        $account = $this->service->get( $name );
        $this->service->delete( $account );
        $this->service->search( 'zimbraId='.$account->getId());
        $this->assertEquals(0, $this->service->getLastCount());
    }
}