<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Server;
use UnicaenZimbra\Service\ZimbraServerService;

/**
 * @group Server
 */
class ServerTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraServerService
     */
    protected $service;

    protected $data = array( /* Données du serveur de test */
        'id' => '1947d148-5372-436c-8b72-a1763dac8947',
        'cn' => 'zimbra-pp.unicaen.fr',
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraServerService::class);
    }

    public function testGetList()
    {
        $id = $this->data['id'];
        $cn = $this->data['cn'];


        $res = $this->service->getList();

        $this->assertArrayHasKey($id, $res);
        $this->assertEquals( $this->service->getLastCount(), count($res) );
        $this->assertInstanceOf(ZimbraServerService::class, $res[$id]);
        $this->assertEquals($id, $res[$id]->getId());
    }

    public function testGet()
    {
        $id = $this->data['id'];
        $cn = $this->data['cn'];

        $res = $this->service->get($id, 'id');

        $this->assertEquals($cn, $res->getName());

        $resByName = $this->service->get( $res->getName() );

        $this->assertInstanceOf(ZimbraServerService::class, $res);
        $this->assertEquals($id, $resByName->getId());
        $this->assertEquals($res, $resByName);
    }
}