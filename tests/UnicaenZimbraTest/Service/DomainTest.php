<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Service\ZimbraDomainService;

/**
 * @group Domain
 */
class DomainTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraDomainService
     */
    protected $service;

    protected $data = array(
        'id' => 'b2113b08-1dcd-4143-ac67-c9c9045e3245',
        'name' => 'pp.unicaen.fr'
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraDomainService::class);
    }

    public function testExists()
    {
        $name = $this->data['name'];
        $this->assertTrue( $this->service->exists($name) );
    }

    public function testGetList()
    {
        $id = $this->data['id'];
        $name = $this->data['name'];

        $res = $this->service->getList();

        $this->assertArrayHasKey($id, $res);
        $this->assertEquals( $this->service->getLastCount(), count($res) );
        $this->assertInstanceOf(Domain::class, $res[$id]);
        $this->assertEquals($name, $res[$id]->getName());
    }

    /**
     * 
     * @return Domain
     */
    public function testGet()
    {
        $id = $this->data['id'];

        $res = $this->service->get($id, 'id');

        $resByName = $this->service->get( $res->getName() );

        $this->assertInstanceOf(Domain::class, $res);
        $this->assertEquals($id, $resByName->getId());
        $this->assertEquals($res, $resByName);
        return $res;
    }

    /**
     * @depends testGet
     */
    public function testModify( Domain $domain )
    {
        $notes = 'Test de modification de domaine (depuis les tests unitaires de la librairie unicaen-zimbra)';

        $domain->zimbraNotes = $notes;
        $this->service->modify($domain);
        $res = $this->service->get($this->data['name']);
        $this->assertEquals($notes, $res->zimbraNotes);
    }
}