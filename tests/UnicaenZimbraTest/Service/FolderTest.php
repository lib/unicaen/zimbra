<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Folder;
use UnicaenZimbra\Entity\Grant;
use UnicaenZimbra\Service\ZimbraAccountService;
use UnicaenZimbra\Service\ZimbraFolderService;

/**
 * @group Folder
 */
class FolderTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraFolderService
     */
    protected $service;

    protected $data = array(
        'accountName' => 'laurent.lecluse@pp.unicaen.fr',
        'absFolderPath' => '/Inbox/Tests Unitaires/Essai1.1/Essai2.1',
        'absFolderPath2' => '/Inbox/Tests Unitaires/Essai1.1/Essai2.2',
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraFolderService::class);

        $this->service->setAccount( $this->data['accountName'] );
    }

    public function testSetAccount()
    {
        $name = $this->data['accountName'];

        $this->service->setAccount( $name );
        $this->assertEquals($name, $this->service->getAccountName() );
    }

    /**
     * @depends testSetAccount
     */
    public function testCreate()
    {
        $folder1 = $this->service->create( $this->data['absFolderPath']);

        $folder2 = new Folder();
        $folder2->setName('Essai2.2');
        $folder2->setParent($this->service->get('/Inbox/Tests Unitaires/Essai1.1'));
        $folder2 = $this->service->create( $folder2 );

        $this->assertEquals( $this->data['absFolderPath'], $folder1->absFolderPath );
        $this->assertEquals( 'Essai2.1', $folder1->getName() );

        $this->assertEquals( $this->data['absFolderPath2'], $folder2->absFolderPath );
        $this->assertEquals( 'Essai2.2', $folder2->getName() );

        $pf = $folder2->getParent();
        $this->assertEquals('Essai1.1', $pf->getName());
    }

    /**
     * @depends testCreate
     */
    public function testGetList()
    {
        $res = $this->service->getList('absFolderPath');

        $this->assertArrayHasKey($this->data['absFolderPath'], $res);
        $this->assertArrayHasKey($this->data['absFolderPath2'], $res);
        $this->assertInstanceOf(Folder::class, $res[$this->data['absFolderPath']]);
        $this->assertInstanceOf(Folder::class, $res[$this->data['absFolderPath2']]);
        $this->assertEquals($this->data['absFolderPath'], $res[$this->data['absFolderPath']]->absFolderPath);
        $this->assertEquals($this->data['absFolderPath2'], $res[$this->data['absFolderPath2']]->absFolderPath);
    }

    /**
     * @depends testCreate
     * @return Folder
     */
    public function testGet()
    {
       $folder = $this->service->get('/Inbox/Tests Unitaires');
       $children = $folder->getChildren();

       $this->assertEquals(array('Essai1.1'), array_keys($children));

       $folderChild = $children['Essai1.1'];
       $children2 = $folderChild->getChildren();

       $this->assertEquals(array('Essai2.1','Essai2.2'), array_keys($children2));
       return $folder;
    }

    /**
     * 
     * @depends testGet
     * @param \UnicaenZimbra\Entity\Folder $folder
     */
    public function testAddGrant( Folder $folder)
    {
        $granteeName = 'laurent.lecluse2@pp.unicaen.fr';
        $granteeFolderName = 'Tests Unitaires de Laurent LECLUSE';

        $account = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraAccountService::class)->get($granteeName);

        $grant = new Grant();
        $grant->setGrantee($account);
        $grant->perm = Grant::PERM_MANAGE;
        $this->service->addGrant($folder, $grant);

        $acl = $folder->getAcl();
        $this->assertArrayHasKey($grant->granteeId, $acl); // Vérifie que le dossier a bien son ACL mise à jour ...
        $grant2 = $acl[$grant->granteeId];
        $this->assertEquals($grant, $grant2); // ... avec la bonne règle de partage

        /* Dans la boite mail du bénéficiaire... */
        $oldAccount = $this->service->getAccount();
        $this->service->setAccount($account);

        $granteeFolder = $this->service->get('/'.$granteeFolderName);
        $this->assertEquals($granteeFolderName, $granteeFolder->getName()); // Prouve que le dossier existe

        $this->service->setAccount($oldAccount);
    }

    /**
     *
     * @depends testGet
     * @param \UnicaenZimbra\Entity\Folder $folder
     */
    public function testRevokeGrant( Folder $folder)
    {
        $granteeName = 'laurent.lecluse2@pp.unicaen.fr';
        $account = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraAccountService::class)->get($granteeName);

        $this->service->revokeGrant($folder, $account);

        $acl = $folder->getAcl();
        $this->assertArrayNotHasKey($account->getId(), $acl); // Vérifie que le dossier a bien son ACL mise à jour ...

        /* Dans la boite mail du bénéficiaire... */
        $oldAccount = $this->service->getAccount();
        $this->service->setAccount($account);

        $granteeLinks = $this->service->getLinkList();
        $linksIds = array();
        foreach( $granteeLinks as $link ){
            $fid = $link->getRemoteFolderId();
            $linksIds[$fid] = true;
        }
        $this->assertArrayNotHasKey($folder->getId(), $linksIds); // Le clé ne doit pas être trouvée si la suppression a bien eu lieu

        $this->service->setAccount($oldAccount);
    }

    /**
     * @depends testGet
     */
    public function testModify( Folder $folder )
    {
        $properties = array(
            'color' => 5,
        );

        foreach( $properties as $key => $value ){
            $folder->$key = $value;
        }
        $this->service->modify($folder);
        $folderRes = $this->service->get($folder->absFolderPath);
        foreach( $properties as $key => $value ){
            $this->assertEquals($value, $folderRes->$key);
        }
    }

    /**
     * @depends testGet
     */
    public function testRename( Folder $folder )
    {
         $this->service->rename($folder, '/Trash/'.$folder->getName());
         $this->assertEquals('Tests Unitaires', $folder->getName());
         $parent = $this->service->getParent($folder);
         $this->assertEquals('Trash', $parent->getName());
    }

    /**
     * @depends testGet
     */
    public function testDelete( Folder $folder )
    {
        $this->service->delete($folder);
        $folders = $this->service->getList();
        $this->assertArrayNotHasKey($folder->getId(), $folders); // Le dossier supprimé ne doit pas être dans la liste!!
    }
}