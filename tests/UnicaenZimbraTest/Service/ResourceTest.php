<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Resource;
use UnicaenZimbra\Service\ZimbraResourceService;

/**
 * @group Resource
 */
class ResourceTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraResourceService
     */
    protected $service;

    protected $data = array(
        'name'              => 'r-test-integration@pp.unicaen.fr',
        'displayName'       => 'Ressource de test d\'intégration',
        'zimbraCalResType'  => Resource::TYPE_EQUIPMENT,
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraResourceService::class);
    }

    public function testCreate()
    {
        $ressource = new Resource;
        $ressource->setName($this->data['name']);
        $properties = $this->data;
        unset($properties['name']);
        $ressource->exchangeArray($properties);

        $res = $this->service->create($ressource);

        foreach( $properties as $key => $value ){
            $this->assertEquals($value, $res->$key);
        }

        return $res->getId();
    }

    /**
     * @depends testCreate
     * @param type $id
     */
    public function testExists( $id )
    {
        $this->assertTrue( $this->service->exists($id, 'id') );
    }

    /**
     * @depends testCreate
     */
    public function testGetList( $id )
    {
        $res = $this->service->getList('pp.unicaen.fr', 100);

        $this->assertArrayHasKey($id, $res);
        $this->assertEquals( $this->service->getLastCount(), count($res) );
        $this->assertInstanceOf(ZimbraResourceService::class, $res[$id]);
        $this->assertEquals($id, $res[$id]->getId());
    }

    /**
     * @depends testCreate
     * @return Resource
     */
    public function testGet( $id )
    {
        $res = $this->service->get($id, 'id');

        $resByName = $this->service->get( $res->getName() );

        $this->assertInstanceOf(ZimbraResourceService::class, $res);
        $this->assertEquals($id, $resByName->getId());
        $this->assertEquals($res, $resByName);

        foreach( $this->data as $key => $value ){
            if ('name' != $key ){
                $this->assertEquals($value, $res->$key);
            }
        }
        return $res;
    }

    /**
     * @depends testGet
     */
    public function testModify(Resource $resource )
    {
        $newDN = 'Ressource de test d\'intégration renommée';

        $resource->displayName = $newDN;
        $this->service->modify($resource);
        $res = $this->service->get($this->data['name']);
        $this->assertEquals($newDN, $res->displayName);
    }

    /**
     * 
     * @depends testCreate
     */
    public function testRename( $id )
    {
        $newName = 'r-test-integration2@pp.unicaen.fr';

        $resource = $this->service->rename($id, $newName);
        $this->assertEquals($newName, $resource->getName());
        return $newName;
    }

    /**
     * @depends testRename
     */
    public function testDelete( $name )
    {
        $resource = $this->service->get( $name );
        $this->service->delete( $resource );
    }
}