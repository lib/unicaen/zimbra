<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Filter\Action\FileInto as FilterActionFileInto;
use UnicaenZimbra\Entity\Filter\Action\Stop as FilterActionStop;
use UnicaenZimbra\Entity\Filter\Filter;
use UnicaenZimbra\Entity\Filter\Test\Header as FilterTestHeader;
use UnicaenZimbra\Service\ZimbraFilterService;

/**
 * @group Filter
 */
class FilterTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraFilterService
     */
    protected $service;

    protected $data = array(
        'accountName' => 'laurent.lecluse@pp.unicaen.fr',
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraFilterService::class);
        $this->service->setAccount($this->data['accountName']);

        $filter = new Filter;
        $filter->setName('test-unitaire');

        $test = new FilterTestHeader;
        $test->setHeader('to,cc');
        $test->setStringComparison('is');
        $test->setValue('laurent.lecluse@pp.unicaen.fr');
        $filter->addTest($test);

        $action = new FilterActionFileInto;
        $action->setFolderPath('/Inbox/Essai Dest 1');
        $filter->addAction($action);
        $filter->addAction(new FilterActionStop);

        $this->data['filters'] = array(
            'test-unitaire' => $filter
        );

    }

    public function testInput()
    {
        $oriInput = $this->service->getInput();

        $this->service->setInput($this->data['filters']);
        $filters = $this->service->getInput();

        $this->assertArrayHasKey('test-unitaire', $filters);

        $filter = $this->data['filters']['test-unitaire'];

        $this->assertEqualFilters($filter, $filters['test-unitaire']);


        $filter->setCondition(Filter::CONDITION_ANYOF);
        $action = new FilterActionFileInto;
        $action->setFolderPath('/Inbox/Essai Dest 1/fgfg');
        $filter->addAction($action);
        $filter->removeAction(0);

        $test = new FilterTestHeader;
        $test->setHeader('subject');
        $test->setStringComparison('contains');
        $test->setValue('Test');
        $testIndex = $filter->addTest($test);

        $actual = $this->saveAndAssertEqualInputFilterTest($this->data['filters'])['test-unitaire'];

        $this->assertEquals('/Inbox/Essai Dest 1/fgfg', $actual->getActions()[0]->getFolderPath());
        $this->assertEquals('Test', $actual->getTests()[$testIndex]->getValue());

        // Restauration de l'état antérieur
        $this->service->setInput($oriInput);
    }

    public function testCreateDeleteInput()
    {
        $filter = $this->data['filters']['test-unitaire'];

        $oriInput = $this->service->getInput();

        $this->service->createInput($filter);
        $filters = $this->service->getInput();

        $this->assertArrayHasKey('test-unitaire', $filters);


        $this->service->deleteInput($filter->getName());
        $filters = $this->service->getInput();

        $this->assertArrayNotHasKey('test-unitaire', $filters);

        // Restauration de l'état antérieur
        $this->service->setInput($oriInput);
    }

    public function testOutput()
    {
        $oriOutput = $this->service->getOutput();

        $this->service->setOutput($this->data['filters']);
        $filters = $this->service->getOutput();

        $this->assertArrayHasKey('test-unitaire', $filters);
        $this->assertEqualFilters($this->data['filters']['test-unitaire'], $filters['test-unitaire']);

        // Restauration de l'état antérieur
        $this->service->setOutput($oriOutput);
    }

    public function testCreateDeleteOutput()
    {
        $filter = $this->data['filters']['test-unitaire'];

        $oriOutput = $this->service->getOutput();

        $this->service->createOutput($filter);
        $filters = $this->service->getOutput();

        $this->assertArrayHasKey('test-unitaire', $filters);

        $this->service->deleteOutput($filter->getName());
        $filters = $this->service->getOutput();

        $this->assertArrayNotHasKey('test-unitaire', $filters);

        // Restauration de l'état antérieur
        $this->service->setOutput($oriOutput);
    }

    public function saveAndAssertEqualInputFilterTest(array $filters)
    {
        $this->service->setInput($filters);
        $actual = $this->service->getInput();

        $this->assertEquals(array_keys($filters), array_keys($actual));
        foreach ($filters as $index => $filter) {
            $this->assertEqualFilters($filter, $actual[$index]);
        }
        return $actual;
    }

    public function assertEqualFilters(Filter $expected, Filter $actual)
    {
        $this->assertEquals($expected, $actual);

        $expectedActions = $expected->getActions();
        $actualActions = $actual->getActions();

        $expectedTests = $expected->getTests();
        $actualTests = $actual->getTests();

        foreach ($expectedActions as $index => $action) {
            $this->assertEquals($action->getArrayCopy(), $actualActions[$index]->getArrayCopy());
        }

        foreach ($expectedTests as $index => $test) {
            $this->assertEquals($test->getArrayCopy(), $actualTests[$index]->getArrayCopy());
        }
    }
}