<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Account;
use UnicaenZimbra\Entity\DistributionList;
use UnicaenZimbra\Service\ZimbraDistributionListService;

/**
 * @group DistributionList
 */
class DistributionListTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraDistributionListService
     */
    protected $service;

    protected $data = array(
        'name' => 'dl-tests-unitaires@pp.unicaen.fr',
        'description' => 'Description',
    );

    protected $listMembers = array(
        'laurent.lecluse@pp.unicaen.fr',
        'laurent.lecluse2@pp.unicaen.fr',
        'david.surville@pp.unicaen.fr',
        'olivier.lemonnier@pp.unicaen.fr',
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraDistributionListService::class);
    }

    public function testCreate()
    {
        $dl = new DistributionList;
        $dl->setName($this->data['name']);
        $properties = $this->data;
        unset($properties['name']);
        $dl->exchangeArray($properties);

        $res = $this->service->create($dl);

        foreach( $properties as $key => $value ){
            $this->assertEquals($value, $res->$key);
        }

        return $res->getId();
    }

    /**
     * @depends testCreate
     * @param type $id
     */
    public function testExists( $id )
    {
        $this->assertTrue( $this->service->exists($id, 'id') );
    }

    /**
     *
     * @depends testCreate
     */
    public function testGetList( $id )
    {
        $res = $this->service->getList('pp.unicaen.fr', 100);

        $this->assertArrayHasKey($id, $res);
        $this->assertEquals( $this->service->getLastCount(), count($res) );
        $this->assertInstanceOf(DistributionList::class, $res[$id]);
        $this->assertEquals($id, $res[$id]->getId());
    }

    /**
     * @depends testCreate
     * @return DistributionList
     */
    public function testGet( $id )
    {
        $res = $this->service->get($id, 'id');

        $resByName = $this->service->get( $res->getName() );

        $this->assertInstanceOf(DistributionList::class, $res);
        $this->assertEquals($id, $resByName->getId());
        $this->assertEquals($res, $resByName);

        foreach( $this->data as $key => $value ){
            if ('name' != $key && 'id' != $key ){
                $this->assertEquals($value, $res->$key);
            }
        }
        return $res;
    }

    /**
     * @depends testGet
     * @param \UnicaenZimbra\Entity\DistributionList $dl
     */
    public function testAddMembers( DistributionList $dl )
    {
        $this->service->addMembers($dl, $this->listMembers[0]);
        $this->service->addMembers($dl, array($this->listMembers[1]) );
        $account1 = new Account;
        $account1->setName($this->listMembers[2]);
        $account2 = new Account;
        $account2->setName($this->listMembers[3]);

        $this->service->addMembers($dl, $account1);
        $this->service->addMembers($dl, $account2 );
        return $dl;
    }

    /**
     * @depends testAddMembers
     * @param \UnicaenZimbra\Entity\DistributionList $dl
     */
    public function testGetMembers( DistributionList $dl )
    {
        $members = $this->service->getMembers($dl);
        /* Tri des deux tableaux pour les comparer */
        sort($this->listMembers);
        sort($members);
        $this->assertEquals($this->listMembers, $members);
        return $dl;
    }

    /**
     * @depends testAddMembers
     * @param \UnicaenZimbra\Entity\DistributionList $dl
     */
    public function testRemoveMembers( DistributionList $dl )
    {
        $members = $this->listMembers;
        $account1 = new Account;
        $account1->setName($this->listMembers[2]);
        $members[2] = $account1;

        $this->service->removeMembers($dl, $this->listMembers);
        return $dl;
    }

    /**
     * @depends testRemoveMembers
     * @param \UnicaenZimbra\Entity\DistributionList $dl
     */
    public function testGetNoMembers( DistributionList $dl )
    {
        $members = $this->service->getMembers($dl);
        $this->assertEmpty($members);
        return $dl;
    }

    /**
     * @depends testGet
     */
    public function testModify(DistributionList $dl )
    {
        $dl->displayName = 'Test DL 3';
        $this->service->modify($dl);
        $res = $this->service->get($this->data['name']);
        $this->assertEquals('Test DL 3', $res->displayName);
    }

    /**
     * 
     * @depends testCreate
     */
    public function testRename( $id )
    {
        $newName = 'dl-tests-unitaires2@pp.unicaen.fr';

        $dl = $this->service->rename($id, $newName);
        $this->assertEquals($newName, $dl->getName());
        return $newName;
    }

    /**
     * @depends testRename
     */
    public function testDelete( $name )
    {
        $dl = $this->service->get( $name );
        $this->assertInstanceOf(DistributionList::class, $this->service->delete( $dl ));
    }
}