<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Service\ZimbraMailBoxService;

/**
 * @group MailBox
 */
class MailBoxTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraMailBoxService
     */
    protected $service;

    protected $data = array(
        'accountId' => '01d13c21-2d2c-45f9-9df0-9bee295b3542' // laurent.lecluse@pp.unicaen.fr
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraMailBoxService::class);
    }

    public function testGetList()
    {
        $id = $this->data['accountId'];

        $res = $this->service->getList(100);
        $this->assertArrayHasKey($id, $res);
        $this->assertInstanceOf(ZimbraMailBoxService::class, $res[$id]);
        $this->assertEquals($id, $res[$id]->getId());
    }

    public function testGet()
    {
        $id = $this->data['accountId'];

        $res = $this->service->get($id);
        $this->assertInstanceOf(ZimbraMailBoxService::class, $res);
        $this->assertEquals($id, $res->getId());
    }
}