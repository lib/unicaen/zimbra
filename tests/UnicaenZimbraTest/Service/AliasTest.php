<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Alias;
use UnicaenZimbra\Service\ZimbraAliasService;

/**
 * @group Alias
 */
class AliasTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraAliasService
     */
    protected $service;

    protected $data = array(
        'id' => 'dfb32883-35d2-4870-9dfd-ad0c04f1fab6',
        'name' => 'dieu@pp.unicaen.fr',
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraAliasService::class);
    }

    public function testExists()
    {
        $name = $this->data['name'];
        $this->assertTrue( $this->service->exists($name) );
    }

    public function testGetList()
    {
        $id = $this->data['id'];
        $name = $this->data['name'];

        $res = $this->service->getList('pp.unicaen.fr', 100);

        $this->assertArrayHasKey($id, $res);
        $this->assertInternalType( 'integer', $this->service->getLastCount() );
        $this->assertInstanceOf(Alias::class, $res[$id]);
        $this->assertEquals($id, $res[$id]->getId());
        $this->assertEquals($name, $res[$id]->getName());
    }

    public function testGet()
    {
        $id = $this->data['id'];
        $name = $this->data['name'];
        
        $alias = $this->service->get( $name );
        $this->assertInstanceOf(Alias::class, $alias);
        $this->assertEquals($id, $alias->getId());
        $this->assertEquals($name, $alias->getName());
    }
}