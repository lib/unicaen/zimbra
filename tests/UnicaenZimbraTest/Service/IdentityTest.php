<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Identity;
use UnicaenZimbra\Service\ZimbraIdentityService;

/**
 * @group Identity
 */
class IdentityTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraIdentityService
     */
    protected $service;

    protected $data = array(
        'accountName' => 'laurent.lecluse@pp.unicaen.fr',
        'name' => 'TEST UNITAIRE',
        'zimbraPrefFromDisplay' => 'Test unitaire d\'avatar',
        'zimbraPrefFromAddress' => 'toto@pp.unicaen.fr'
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraIdentityService::class);
        $this->service->setAccount( $this->data['accountName'] );
    }

    public function testSetAccount()
    {
        $name = $this->data['accountName'];

        $this->service->setAccount( $name );
        $this->assertEquals($name, $this->service->getAccountName() );
    }

    /**
     * @depends testSetAccount
     */
    public function testCreate()
    {
        $identity = new Identity;
        $identity->setName($this->data['name']);
        $properties = $this->data;
        unset($properties['accountName']);
        unset($properties['name']);
        $identity->exchangeArray($properties);

        $res = $this->service->create($identity);

        foreach( $properties as $key => $value ){
            $this->assertEquals($value, $res->$key);
        }

        return $res->getId();
    }

    /**
     * @depends testCreate
     */
    public function testGetList()
    {
        $res = $this->service->getList();

        $this->assertArrayHasKey($this->data['name'], $res);
        $this->assertInstanceOf(ZimbraIdentityService::class, $res[$this->data['name']]);
    }

    /**
     * @depends testCreate
     * @return FolderEntity
     */
    public function testGet()
    {
       $identity = $this->service->get($this->data['name']);

       $this->assertEquals($this->data['name'], $identity->getName());

       return $identity;
    }

    /**
     * @depends testGet
     */
    public function testModify( Identity $identity )
    {
        $identity->zimbraPrefSentMailFolder = 'sent/Tests unitaires';
        $this->service->modify($identity);
        $res = $this->service->get($this->data['name']);
        $this->assertEquals('sent/Tests unitaires', $res->zimbraPrefSentMailFolder);
    }

    /**
     * @depends testGet
     */
    public function testDelete( Identity $identity )
    {
        $this->service->delete($identity);
        $identities = $this->service->getList('id');
        $this->assertArrayNotHasKey($identity->getId(), $identities); // L'identité supprimée ne doit pas être dans la liste!!
    }
}