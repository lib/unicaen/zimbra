<?php

namespace UnicaenZimbraTest\Service;

use PHPUnit_Framework_TestCase;
use UnicaenZimbra\Entity\Cos;
use UnicaenZimbra\Service\ZimbraCosService;

/**
 * @group Cos
 */
class CosTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ZimbraCosService
     */
    protected $service;

    protected $data = array(
        'id' => 'e00428a1-0c00-11d9-836a-000d93afea2a',
        'cn' => 'default',
    );

    protected function setUp()
    {
        \UnicaenZimbraTest\Bootstrap::getServiceManager()->get('zimbra')->connect();
        $this->service = \UnicaenZimbraTest\Bootstrap::getServiceManager()->get(ZimbraCosService::class);
    }

    public function testExists()
    {
        $name = $this->data['cn'];
        $this->assertTrue( $this->service->exists($name) );
    }

    public function testGetList()
    {
        $res = $this->service->getList();

        $this->assertArrayHasKey($this->data['id'], $res);
        $cos = $res[$this->data['id']];
        $this->assertEquals($this->data['cn'], $cos->cn);
        $this->assertInstanceOf(Cos::class, $cos);
    }

    public function testGet()
    {
        $cos = $this->service->get($this->data['cn']);
        $this->assertEquals($this->data['id'], $cos->getId());
        $this->assertInstanceOf(Cos::class, $cos);
    }
}