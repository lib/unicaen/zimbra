<?php

// Application config
$appConfig = array();//include __DIR__ . '/../../../../../config/application.config.php';

// Test config
$testConfig = array(
    'modules' => array(
        'UnicaenZimbra'
    ),
    'module_listener_options' => array(
        'config_glob_paths'    => array(
            __DIR__ . '/autoload/{,*.}{global,local}.php',
        ),
    ),
);

return \Laminas\Stdlib\ArrayUtils::merge($appConfig, $testConfig);