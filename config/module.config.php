<?php

namespace UnicaenZimbra;

use UnicaenZimbra\Options\ModuleOptionsFactory;
use UnicaenZimbra\Processus\ZimbraBoiteGeneriqueProcessus;
use UnicaenZimbra\Processus\ZimbraBoiteGeneriqueProcessusFactory;
use UnicaenZimbra\Service\ZimbraAccountService;
use UnicaenZimbra\Service\ZimbraAccountServiceFactory;
use UnicaenZimbra\Service\ZimbraAliasService;
use UnicaenZimbra\Service\ZimbraAliasServiceFactory;
use UnicaenZimbra\Service\ZimbraCosService;
use UnicaenZimbra\Service\ZimbraCosServiceFactory;
use UnicaenZimbra\Service\ZimbraDistributionListService;
use UnicaenZimbra\Service\ZimbraDistributionListServiceFactory;
use UnicaenZimbra\Service\ZimbraDomainService;
use UnicaenZimbra\Service\ZimbraDomainServiceFactory;
use UnicaenZimbra\Service\ZimbraFilterService;
use UnicaenZimbra\Service\ZimbraFilterServiceFactory;
use UnicaenZimbra\Service\ZimbraFolderService;
use UnicaenZimbra\Service\ZimbraFolderServiceFactory;
use UnicaenZimbra\Service\ZimbraGalService;
use UnicaenZimbra\Service\ZimbraGalServiceFactory;
use UnicaenZimbra\Service\ZimbraIdentityService;
use UnicaenZimbra\Service\ZimbraIdentityServiceFactory;
use UnicaenZimbra\Service\ZimbraMailBoxService;
use UnicaenZimbra\Service\ZimbraMailBoxServiceFactory;
use UnicaenZimbra\Service\ZimbraResourceService;
use UnicaenZimbra\Service\ZimbraResourceServiceFactory;
use UnicaenZimbra\Service\ZimbraRightService;
use UnicaenZimbra\Service\ZimbraRightServiceFactory;
use UnicaenZimbra\Service\ZimbraServerService;
use UnicaenZimbra\Service\ZimbraServerServiceFactory;

return [
    'unicaen-zimbra' => [],

    'service_manager' => [
        'aliases' => [
            // services
            'zimbraServiceAccount' => ZimbraAccountService::class,
            'zimbraServiceAlias' => ZimbraAliasService::class,
            'zimbraServiceCos' => ZimbraCosService::class,
            'zimbraServiceDistributionList' => ZimbraDistributionListService::class,
            'zimbraServiceDomain' => ZimbraDomainService::class,
            'zimbraServiceFilter' => ZimbraFilterService::class,
            'zimbraServiceFolder' => ZimbraFolderService::class,
            'zimbraServiceGal' => ZimbraGalService::class,
            'zimbraServiceIdentity' => ZimbraIdentityService::class,
            'zimbraServiceMailBox' => ZimbraMailBoxService::class,
            'zimbraServiceResource' => ZimbraResourceService::class,
            'zimbraServiceRight' => ZimbraRightService::class,
            'zimbraServiceServer' => ZimbraServerService::class,

            // processus
            'zimbraProcessusBoiteGenerique' => BoiteGeneriqueProcessus::class,
        ],
        'factories' => [
            'zimbra' => ZimbraFactory::class,
            'zimbraOptions' => ModuleOptionsFactory::class,

            // services
            ZimbraAccountService::class => ZimbraAccountServiceFactory::class,
            ZimbraAliasService::class => ZimbraAliasServiceFactory::class,
            ZimbraCosService::class => ZimbraCosServiceFactory::class,
            ZimbraDistributionListService::class => ZimbraDistributionListServiceFactory::class,
            ZimbraDomainService::class => ZimbraDomainServiceFactory::class,
            ZimbraFilterService::class => ZimbraFilterServiceFactory::class,
            ZimbraFolderService::class => ZimbraFolderServiceFactory::class,
            ZimbraGalService::class => ZimbraGalServiceFactory::class,
            ZimbraIdentityService::class => ZimbraIdentityServiceFactory::class,
            ZimbraMailBoxService::class => ZimbraMailBoxServiceFactory::class,
            ZimbraResourceService::class => ZimbraResourceServiceFactory::class,
            ZimbraRightService::class => ZimbraRightServiceFactory::class,
            ZimbraServerService::class => ZimbraServerServiceFactory::class,

            // Zimbra processus
            ZimbraBoiteGeneriqueProcessus::class => ZimbraBoiteGeneriqueProcessusFactory::class,
        ],
    ],
];